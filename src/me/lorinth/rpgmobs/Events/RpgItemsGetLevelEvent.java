package me.lorinth.rpgmobs.Events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class RpgItemsGetLevelEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private int level;

    public RpgItemsGetLevelEvent(Player player, int level){
        this.player = player;
        this.level = level;
    }

    public Player getPlayer(){
        return player;
    }

    public int getLevel(){
        return level;
    }

    public void setLevel(int level){
        this.level = level;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    /**
     * Gets a list of handlers handling this event.
     *
     * @return A list of handlers handling this event.
     */
    public final static HandlerList getHandlerList(){
        return handlers;
    }
}
