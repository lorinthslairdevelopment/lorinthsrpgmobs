package me.lorinth.rpgmobs.Enums;

public enum LevelHook {
    Vanilla, BattleStats, Heroes, MMOCore, RpgItems, SkillAPI, SkillsPro
}