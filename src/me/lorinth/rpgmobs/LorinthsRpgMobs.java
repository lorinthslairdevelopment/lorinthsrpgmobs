package me.lorinth.rpgmobs;

import com.google.common.io.Files;
import me.lorinth.rpgmobs.Command.ButcherExecutor;
import me.lorinth.rpgmobs.Command.CommandConstants;
import me.lorinth.rpgmobs.Command.MainExecutor;
import me.lorinth.rpgmobs.Data.*;
import me.lorinth.rpgmobs.Enums.LevelHook;
import me.lorinth.rpgmobs.Listener.CreatureEventListener;
import me.lorinth.rpgmobs.Listener.PluginListener;
import me.lorinth.rpgmobs.Listener.TaskListener;
import me.lorinth.rpgmobs.Objects.MobInstance;
import me.lorinth.rpgmobs.Objects.Placeholder.RpgMobsPlaceholder;
import me.lorinth.rpgmobs.Objects.Properties;
import me.lorinth.rpgmobs.Util.MetaDataConstants;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Variants.MobVariant;
import me.lorinth.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.HandlerList;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Main class of LorinthsRpgMobs contains main API Methods
 */
public class LorinthsRpgMobs extends JavaPlugin {

    public static LorinthsRpgMobs instance;
    public static Properties properties;
    private static DataLoader dataLoader;
    private int loads = 0;
    private TaskListener taskListener;
    private static LevelHook levelHook = LevelHook.Vanilla;

    @Override
    public void onEnable(){
        instance = this;
        new RpgMobsOutputHandler();
        RpgMobsOutputHandler.PrintInfo("Enabling v" + getDescription().getVersion() + "...");
        firstLoad();
        registerCommands();

        loadLoot();
        hookPapi();
        if(createDataLoader()){
            loadMinecraftVersion();
            loadDamageIndicators();
            loadFormulas();
            loadMessages();
            loadTasks();
            Bukkit.getPluginManager().registerEvents(new CreatureEventListener(dataLoader), this);
            Bukkit.getPluginManager().registerEvents(new PluginListener(), this);
            RpgMobsOutputHandler.PrintInfo("Finished!");
        }
    }

    @Override
    public void onDisable(){
        taskListener.stop();
        RpgMobsOutputHandler.PrintInfo("Disabling...");

        if(dataLoader != null){
            //Load possible changes in the file from user
            File file = new File(getDataFolder(), "config.yml");
            File backup = new File(getDataFolder(), "backup.yml");
            try {
                Files.copy(file, backup);
                YamlConfiguration config = new YamlConfiguration();
                config.load(file);

                //Apply the changes we gained during the session
                dataLoader.saveData(config);

                config.save(file);
            } catch(Exception e){
                RpgMobsOutputHandler.PrintException("Error on Disable", e);
            }
        }
        HandlerList.unregisterAll(this);
    }

    public static void Reload(){
        instance.onDisable();
        instance.onEnable();
    }

    public static DataLoader getDataLoader(){
        return dataLoader;
    }

    private boolean createDataLoader(){
        try {
            FileConfiguration config;
            if(this.loads == 0)
                config = this.getConfig();
            else{
                config = new YamlConfiguration();
                config.load(new File(getDataFolder(), "config.yml"));
            }

            loadFormulaMethod(config);
            loadLevelHook(config);
            autoButcher(config);

            dataLoader = new DataLoader();
            dataLoader.loadData(config, this);
            this.loads++;
            return true;
        }
        catch(Exception e) {
            RpgMobsOutputHandler.PrintException("Error loading config.yml", e);
            RpgMobsOutputHandler.PrintError("Rpg Mobs Disabled");
            return false;
        }
    }

    private void loadFormulaMethod(FileConfiguration config){
        String formulaMethod = config.getString("FormulaMethod");
        if(!TryParse.parseEnum(FormulaMethod.class, formulaMethod)){
            String formulaMethodsString = "";
            for(FormulaMethod method : FormulaMethod.values()){
                formulaMethodsString += method.name() + ",";
            }
            formulaMethodsString = formulaMethodsString.substring(0, formulaMethodsString.length()-1);

            RpgMobsOutputHandler.PrintError("Failed to load formula method, " + RpgMobsOutputHandler.HIGHLIGHT + formulaMethod);
            RpgMobsOutputHandler.PrintError("Please set FormulaMethod in config.yml to one of the following, " + formulaMethodsString);
            Properties.FormulaMethod = FormulaMethod.Java;
            return;
        }
        Properties.FormulaMethod = FormulaMethod.valueOf(formulaMethod);
    }

    private void loadLevelHook(FileConfiguration config){
        String levelHookValue = config.getString("LevelHook");
        if(!TryParse.parseEnum(LevelHook.class, levelHookValue)){
            RpgMobsOutputHandler.PrintError("Failed to load level hook, " + RpgMobsOutputHandler.ERROR + levelHookValue);
            RpgMobsOutputHandler.PrintError("Please set LevelHook in config.yml to one of the following, " + LevelHook.values().toString());
            levelHook = LevelHook.Vanilla;
            return;
        }
        levelHook = LevelHook.valueOf(levelHookValue);
    }

    private void autoButcher(FileConfiguration config){
        if(!ConfigHelper.ConfigContainsPath(config, "AutoButcher.ExecuteOnLoad") &&
            !ConfigHelper.ConfigContainsPath(config, "AutoButcher.Args") &&
            !ConfigHelper.ConfigContainsPath(config, "AutoButcher.Worlds")){

            config.set("AutoButcher.ExecuteOnLoad", false);
            config.set("AutoButcher.Args", "-b -f");
            config.set("AutoButcher.Worlds", new ArrayList<String>(){{
                add("world");
                add("world_nether");
                add("world_the_end");
            }});

            saveConfig();
        }

        if(!config.getBoolean("AutoButcher.ExecuteOnLoad")) {
            return;
        }

        ButcherExecutor executor = new ButcherExecutor();
        String[] args;

        //Use butcher args
        String butcherArgs = config.getString("AutoButcher.Args");
        if(butcherArgs != null) {
            args = butcherArgs.split(" ");
        }
        else{
            args = new String[0];
        }

        List<String> butcherWorlds = config.getStringList("AutoButcher.Worlds");
        //Butcher each world
        for(String butcherWorld : butcherWorlds){
            World world = Bukkit.getWorld(butcherWorld);
            if(world != null){
                executor.codeExecute(world, args);
            }
        }
    }

    private void hookPapi(){
        if(Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null){
            new RpgMobsPlaceholder().register();
        }
    }

    private void loadFormulas(){
        try{
            File file = new File(getDataFolder(), "formulas.yml");
            YamlConfiguration config = new YamlConfiguration();
            config.load(file);

            new FormulaManager().loadData(config, this);
        } catch(Exception e){
            RpgMobsOutputHandler.PrintException("Error loading formulas.yml", e);
        }
    }

    private void loadLoot(){
        try {
            File file = new File(getDataFolder(), "loot.yml");
            YamlConfiguration config = new YamlConfiguration();
            config.load(file);

            new LootManager().loadData(config, this);
        }
        catch(Exception e){
            RpgMobsOutputHandler.PrintException("Error loading loot.yml", e);
        }
    }

    private void loadDamageIndicators(){
        try {
            File file = new File(getDataFolder(), "indicators.yml");
            YamlConfiguration config = new YamlConfiguration();
            config.load(file);

            new DamageIndicatorDataManager().loadData(config, this);
        }
        catch(Exception e){
            RpgMobsOutputHandler.PrintException("Error loading indicators.yml", e);
        }
    }

    private void loadMessages(){
        try {
            File file = new File(getDataFolder(), "messages.yml");
            YamlConfiguration config = new YamlConfiguration();
            config.load(file);

            new MessageManager().loadData(config, this);
        }
        catch(Exception e){
            RpgMobsOutputHandler.PrintException("Error loading messages.yml", e);
        }
    }

    private void loadTasks(){
        try {
            File file = new File(getDataFolder(), "tasks.yml");
            YamlConfiguration config = new YamlConfiguration();
            config.load(file);

            new TaskManager().loadData(config, this);

            //Load listener
            taskListener = new TaskListener();
            Bukkit.getPluginManager().registerEvents(taskListener, this);

        }
        catch(Exception e){
            RpgMobsOutputHandler.PrintException("Error loading tasks.yml", e);
        }
    }

    private void firstLoad(){
        try{
            ResourceHelper.copy(getResource("resources/config.yml"), new File(getDataFolder(), "config.yml"));
            ResourceHelper.copy(getResource("resources/formulas.yml"), new File(getDataFolder(), "formulas.yml"));
            ResourceHelper.copy(getResource("resources/indicators.yml"), new File(getDataFolder(), "indicators.yml"));
            ResourceHelper.copy(getResource("resources/loot.yml"), new File(getDataFolder(), "loot.yml"));
            ResourceHelper.copy(getResource("resources/messages.yml"), new File(getDataFolder(), "messages.yml"));
            ResourceHelper.copy(getResource("resources/tasks.yml"), new File(getDataFolder(), "tasks.yml"));
        }
        catch(Exception exc){
            exc.printStackTrace();
        }
    }

    private void loadMinecraftVersion(){
        int subVersion = NmsHelper.getSimpleVersion();
        Properties.SubVersion = subVersion;
        Properties.IsAttributeVersion = subVersion >= 9;
        Properties.IsMessageTypeVersion = subVersion >= 10;
        Properties.IsAbstractHorseVersion = subVersion >= 11;
        Properties.IsAddPassengerVersion = subVersion >= 12;
        Properties.IsLootableVersion = subVersion >= 13;
        Properties.IsPersistentDataVersion = subVersion >= 14;
    }

    private void registerCommands() {
        getCommand(CommandConstants.LorinthsRpgMobsCommand).setExecutor(new MainExecutor());
    }

    public static Updater ForceUpdate(Updater.UpdateCallback callback){
        return new Updater(instance, instance.getFile(), Updater.UpdateType.DEFAULT, callback);
    }

    //API Methods
    /**
     * Get level of a given creature
     * @param creature creature you want to check
     * @return level of creature, null if no level
     */
    public static Integer GetLevelOfCreature(Creature creature){
        return GetLevelOfEntity(creature);
    }

    /**
     * Get level of entity
     * @param entity the entity you want to check
     * @return null if no level, otherwise returns level of entity
     */
    public static Integer GetLevelOfEntity(Entity entity){
        if(entity instanceof LivingEntity){
            if(NmsHelper.getSimpleVersion() >= 14){
                PersistentDataContainer dataContainer = entity.getPersistentDataContainer();
                Integer value = dataContainer.get(new NamespacedKey(LorinthsRpgMobs.instance, "level"), PersistentDataType.INTEGER);
                return value;
            }
            else{
                if(entity.hasMetadata(MetaDataConstants.Level)) {
                    if (entity.getMetadata(MetaDataConstants.Level).size() > 0) {
                        return entity.getMetadata(MetaDataConstants.Level).get(0).asInt();
                    }
                }
            }
        }
        return null;
    }

    /**
     * Returns the variant of the entity. Such as Burning, Poisonous, Fast, Slow, etc
     * @param entity
     * @return - The variant of the entity, null if no variant applied
     */
    public static MobVariant GetMobVariantOfEntity(Entity entity){
        if(entity instanceof LivingEntity){
            String variantName = null;
            if(Properties.IsPersistentDataVersion) {
                PersistentDataContainer dataContainer = entity.getPersistentDataContainer();
                variantName = dataContainer.get(new NamespacedKey(LorinthsRpgMobs.instance, "variant"), PersistentDataType.STRING);
            }
            else{
                if(entity.hasMetadata(MetaDataConstants.Variant)) {
                    variantName = entity.getMetadata(MetaDataConstants.Variant).get(0).asString();
                }
            }

            if(variantName != null){
                return MobVariantDataManager.GetVariant(variantName);
            }
        }

        return null;
    }

    /**
     * Get the level of a specific location
     * @param location location to check
     * @return level at location
     */
    public static int GetLevelAtLocation(Location location){
        return dataLoader.calculateLevel(location, null);
    }

    /**
     * Get the level of a specific location
     * @param location location to check
     * @param useRangeOrVariance If set to false will get the minimum level of the location.
     * @return level at location
     */
    public static int GetLevelAtLocation(Location location, boolean useRangeOrVariance){
        return dataLoader.calculateLevel(location, null, useRangeOrVariance);
    }

    /**
     * Get the spawn point manager which contains data for all spawn points which you can read/write to
     * @return SpawnPointManager
     */
    public static SpawnPointManager GetSpawnPointManager(){
        return dataLoader.getSpawnPointManager();
    }

    public static MobVariantDataManager GetMobVariantManager(){ return dataLoader.getMobVariantManager(); }

    public static MythicMobsDataManager GetMythicMobsDataManager(){
        return dataLoader.getMythicMobsDataManager();
    }

    /**
     * Get the level region manager which contains data for all regions with level settings
     * @return LevelRegionManager
     */
    public static LevelRegionManager GetLevelRegionManager(){
        return dataLoader.getLevelRegionManager();
    }

    /**
     * Get the creature data manager which contains data for all entities which you can read/write to
     * @return CreatureDataManager
     */
    public static CreatureDataManager GetCreatureDataManager(){
        return dataLoader.getCreatureDataManager();
    }

    public static HealthBarManager GetHealthBarManager(){
        return dataLoader.getHealthBarManager();
    }

    public static boolean IsMythicMob(Entity entity){
        return dataLoader.getMythicMobsDataManager().isMythicMob(entity);
    }

    public static LevelHook getLevelHook(){
        return levelHook;
    }

    public static MobInstance GetMob(Entity entity){
        return new MobInstance(entity);
    }
}
