package me.lorinth.rpgmobs.Listener;

import me.lorinth.rpgmobs.Data.DamageIndicatorDataManager;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.DamageIndicatorConfig;
import me.lorinth.utils.holograms.HologramManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;

public class DamageIndicatorListener implements Listener {

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event){
        DamageIndicatorConfig config = DamageIndicatorDataManager.getDamageIndicatorConfig(event.getCause().toString());
        if (!config.isUseHologram()){
            return;
        }

        Player except = null;
        if (event.getEntity() instanceof Player){
            except = (Player) event.getEntity();
        }

        HologramManager.setGlobalHologram(LorinthsRpgMobs.instance, config.getLines(event.getDamage()),
                event.getEntity().getLocation(), 20, config.getHeight(), config.getHologramRadius(), except);
    }

    @EventHandler
    public void onRegainHealth(EntityRegainHealthEvent event){
        DamageIndicatorConfig config = DamageIndicatorDataManager.getDamageIndicatorConfig("HEAL_" + event.getRegainReason().toString());
        if (!config.isUseHologram()){
            return;
        }

        Player except = null;
        if (event.getEntity() instanceof Player){
            except = (Player) event.getEntity();
        }

        HologramManager.setGlobalHologram(LorinthsRpgMobs.instance, config.getLines(event.getAmount()),
                event.getEntity().getLocation(), 20, config.getHeight(), config.getHologramRadius(), except);
    }

}
