package me.lorinth.rpgmobs.Listener;

import me.lorinth.rpgmobs.Data.JobsRebornDataManager;
import me.lorinth.rpgmobs.Events.RpgMobDeathEvent;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class JobsRebornListener implements Listener {

    private JobsRebornDataManager dataManager;

    public JobsRebornListener(JobsRebornDataManager dataManager){
        this.dataManager = dataManager;
    }

    @EventHandler
    public void onJobsExperienceGain(RpgMobDeathEvent event){
        dataManager.handleRpgDeathEvent(event);
    }

}
