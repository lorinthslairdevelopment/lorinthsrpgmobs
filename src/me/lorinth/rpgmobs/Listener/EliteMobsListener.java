package me.lorinth.rpgmobs.Listener;

import com.magmaguy.elitemobs.api.EliteMobSpawnEvent;
import com.magmaguy.elitemobs.config.custombosses.CustomBossConfigFields;
import com.magmaguy.elitemobs.custombosses.CustomBossEntity;
import com.magmaguy.elitemobs.mobconstructor.EliteMobEntity;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.Properties;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.Calculator;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

public class EliteMobsListener implements Listener {

    private HashMap<String, String> worldLevelFormulas;
    private ArrayList<String> myEliteMobs = new ArrayList<>();
    private ArrayList<Location> myCustomBossMobs = new ArrayList<>();

    public EliteMobsListener(HashMap<String, String> worldLevelFormulas){
        this.worldLevelFormulas = worldLevelFormulas;
    }

    @EventHandler
    public void OnEliteMobsSpawn(EliteMobSpawnEvent spawnEvent){
        if(myEliteMobs.contains(spawnEvent.getEntity().getUniqueId().toString())){
            myEliteMobs.remove(spawnEvent.getEntity().getUniqueId().toString());
            return;
        }
        if(spawnEvent.getEliteMobEntity() instanceof CustomBossEntity) {
                return;
//            Location target = null;
//            for (Location myCustomBossMobLocation : myCustomBossMobs) {
//                if (locationsEqual(loc, myCustomBossMobLocation)) {
//                    System.out.println("Location matched");
//                    target = myCustomBossMobLocation;
//                    break;
//                }
//            }
//
//            if (target != null) {
//                myCustomBossMobs.remove(target);
//                System.out.println("Location removed and leaving spawn");
//                return;
//            }
        }

        Entity entity = spawnEvent.getEntity();
        myEliteMobs.add(entity.getUniqueId().toString());
        Integer level = getLevelAtLocation(entity.getLocation());

        if(level == null){
            return;
        }

//        if(spawnEvent.getEliteMobEntity() instanceof CustomBossEntity){
//            Bukkit.getScheduler().runTaskLater(LorinthsRpgMobs.instance, () -> {
//                handleCustomBoss(level, (CustomBossEntity) spawnEvent.getEliteMobEntity());
//            }, 1);
//        }
//        else{
            handleEliteMob(level, spawnEvent.getEliteMobEntity());
        //}
    }

    private Integer getLevelAtLocation(Location location){
        if(location == null){
            return null;
        }
        if(location.getWorld() == null){
            return null;
        }

        String worldname = location.getWorld().getName();
        String formula = worldLevelFormulas.get(worldname);

        if(formula == null){
            return null;
        }

        Integer originalLevel = LorinthsRpgMobs.GetLevelAtLocation(location);
        formula = formula.replace("{level}", originalLevel.toString());

        return (int) Calculator.eval(Properties.FormulaMethod, formula);
    }

    private void handleCustomBoss(int level, CustomBossEntity customBossEntity){
        customBossEntity.remove();
        LivingEntity entity = customBossEntity.getLivingEntity();
        try{
            Field f = CustomBossEntity.class.getDeclaredField("customBossConfigFields");
            f.setAccessible(true);

            CustomBossConfigFields fields = (CustomBossConfigFields) f.get(customBossEntity);
            myCustomBossMobs.add(entity.getLocation());
            CustomBossEntity.constructCustomBoss(fields.getFileName(), entity.getLocation(), level);
        } catch(Exception ex){
            RpgMobsOutputHandler.PrintException("Error replacing the Custom Boss Entity", ex);
        }
    }

    private void handleEliteMob(int level, EliteMobEntity eliteMob){
        eliteMob.remove();
        Entity entity = eliteMob.getLivingEntity().getWorld().spawn(eliteMob.getLivingEntity().getLocation(), eliteMob.getLivingEntity().getClass());
        myEliteMobs.add(entity.getUniqueId().toString());
        new EliteMobEntity((LivingEntity) entity, level, CreatureSpawnEvent.SpawnReason.CUSTOM);
    }

    private boolean locationsEqual(Location a, Location b) {
        return a.getWorld().getName().equalsIgnoreCase(b.getWorld().getName()) &&
                a.getBlockX() == b.getBlockX() && a.getBlockY() == b.getBlockY() &&
                a.getBlockZ() == b.getBlockZ();
    }
}
