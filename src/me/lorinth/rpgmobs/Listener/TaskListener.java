package me.lorinth.rpgmobs.Listener;

import me.lorinth.rpgmobs.Data.TaskManager;
import me.lorinth.rpgmobs.Tasks.PlayerMobLevelNotifyTask;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class TaskListener implements Listener {

    private ArrayList<BukkitRunnable> tasks = new ArrayList<>();

    public TaskListener(){
        for(Player player : Bukkit.getServer().getOnlinePlayers()){
            playerLoggedOn(player);
        }
    }

    public void stop(){
        for(BukkitRunnable runnable : tasks){
            runnable.cancel();
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        playerLoggedOn(event.getPlayer());
    }

    private void playerLoggedOn(Player player){
        if(TaskManager.playerMobLevelNotifyOptions.Enabled){
            BukkitRunnable runnable = new PlayerMobLevelNotifyTask(player);
            tasks.add(runnable);
        }
    }

}
