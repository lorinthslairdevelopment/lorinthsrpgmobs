package me.lorinth.rpgmobs.Listener;

import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.api.bukkit.events.MythicMobSpawnEvent;
import me.lorinth.rpgmobs.Data.MythicMobsDataManager;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.objects.Version;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.lang.reflect.Method;

public class MythicMobsListener implements Listener {

    private MythicMobsDataManager dataManager;
    private Version mythicMobsVersion;
    private Method setLevelMethod;

    public MythicMobsListener(MythicMobsDataManager dataManager, Version version){
        this.dataManager = dataManager;
        this.mythicMobsVersion = version;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onMythicMobSpawn(MythicMobSpawnEvent event){
        if(dataManager.isWorldIgnored(event.getLocation().getWorld())) {
            return;
        }

        Entity entity = event.getEntity();

        Integer level = LorinthsRpgMobs.GetLevelOfEntity(event.getEntity());
        if(level != null) {
            setMythicMobLevel(event, level);
        }

        if(LorinthsRpgMobs.GetMobVariantOfEntity(entity) != null) {
            LorinthsRpgMobs.GetMobVariantOfEntity(entity).remove(entity);
        }
    }

    public boolean isMythicMob(Entity entity){
        return MythicMobs.inst().getAPIHelper().isMythicMob(entity);
    }

    private void setMythicMobLevel(MythicMobSpawnEvent event, int level) {
        if(this.mythicMobsVersion.getMajorVersion() > 4 ||
                (this.mythicMobsVersion.getMajorVersion() == 4 && this.mythicMobsVersion.getMinorVersion() >= 9)) {
            try {
                if(setLevelMethod == null)
                    setLevelMethod = event.getClass().getMethod("setMobLevel", double.class);

                setLevelMethod.invoke(event, (double) level);
            } catch(Exception ex) {
                RpgMobsOutputHandler.PrintException("Unable to find method, 'setMobLevel' on 'MythicMobSpawnEvent' with parameter {level: double}", ex);
            }
        } else {
            try {
                if(setLevelMethod == null)
                    setLevelMethod = event.getClass().getMethod("setMobLevel", int.class);

                setLevelMethod.invoke(event, level);
            } catch(Exception ex) {
                RpgMobsOutputHandler.PrintException("Unable to find method, 'setMobLevel' on 'MythicMobSpawnEvent' with parameter {level: int}", ex);
            }
        }
    }
}
