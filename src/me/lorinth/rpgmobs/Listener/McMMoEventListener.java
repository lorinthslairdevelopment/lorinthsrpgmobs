package me.lorinth.rpgmobs.Listener;

import com.gmail.nossr50.api.ExperienceAPI;
import com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent;
import me.lorinth.rpgmobs.Events.RpgMobDeathEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.HashMap;

public class McMMoEventListener implements Listener {

    private HashMap<String, Integer> playerLastKillExp = new HashMap<>();

    @EventHandler(ignoreCancelled = true)
    public void onMobDeathEvent(RpgMobDeathEvent event){
        Player player = event.getKiller();
        playerLastKillExp.put(player.getUniqueId().toString(), event.getExp());
    }

    @EventHandler(ignoreCancelled = true)
    public void onMcMMOPveExpEvent(McMMOPlayerXpGainEvent event){
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();
        if(!playerLastKillExp.containsKey(uuid)){
            return;
        }

        ExperienceAPI.addModifiedXP(player, event.getSkill().getName(), playerLastKillExp.remove(uuid), "PVE", true);
    }
}
