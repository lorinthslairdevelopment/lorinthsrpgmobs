package me.lorinth.rpgmobs.Listener;

import me.lorinth.rpgmobs.Events.RpgMobDeathEvent;
import me.lorinth.rpgmobs.Objects.Properties;
import me.lorinth.rpgmobs.Util.DistanceHelper;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Objects.DistanceAlgorithm;
import me.lorinth.utils.Calculator;
import me.lorinth.utils.TryParse;
import net.Indyuce.mmocore.api.experience.EXPSource;
import net.Indyuce.mmocore.api.player.PlayerData;
import net.Indyuce.mmocore.api.player.social.Party;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.HashMap;

public class MmoCoreListener implements Listener {

    private boolean useDistance = true;
    private int distance = 50;
    private DistanceAlgorithm distanceAlgorithm = DistanceAlgorithm.Diamond;
    private HashMap<Integer, String> memberFormula = new HashMap<>();

    public MmoCoreListener(FileConfiguration config, String path){
        distance = config.getInt(path + "ValidDistance");
        useDistance = distance > 0;

        String distanceAlgorithmValue = config.getString(path + "DistanceAlgorithm");
        if(useDistance){
            if(TryParse.parseEnum(DistanceAlgorithm.class, distanceAlgorithmValue))
                distanceAlgorithm = DistanceAlgorithm.valueOf(distanceAlgorithmValue);
            else
                RpgMobsOutputHandler.PrintError("Cannot determine DistanceAlgorithm type, " + RpgMobsOutputHandler.HIGHLIGHT + distanceAlgorithmValue + RpgMobsOutputHandler.ERROR + ", using default Diamond");
        }

        for(String key : config.getConfigurationSection(path + "PartyExperienceFormulas").getKeys(false)){
            if(!TryParse.parseInt(key)){
                RpgMobsOutputHandler.PrintError("Not a valid number, " + RpgMobsOutputHandler.HIGHLIGHT + key + RpgMobsOutputHandler.ERROR + ", in config.yml under " + path + "PartyExperienceFormulas");
                continue;
            }

            memberFormula.put(Integer.parseInt(key), config.getString(path + "PartyExperienceFormulas." + key));
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onRpgDeath(RpgMobDeathEvent event){
        Player player = event.getKiller();
        PlayerData data = PlayerData.get(player.getUniqueId());
        int exp = event.getExp();
        ArrayList<PlayerData> validMembers = new ArrayList<>();

        Party party = data.getParty();
        if(party == null || party.getMembers().count() == 1){
            data.giveExperience(exp, EXPSource.OTHER);
            return;
        }

        //Get valid members
        data.getParty().getMembers().forEach((member) -> {
            //Not the same player
            Player other = member.getPlayer();
            if(!other.isOnline())
                return;

            if(!other.getUniqueId().toString().equalsIgnoreCase(player.getUniqueId().toString())){
                if(useDistance){
                    if(DistanceHelper.calculateDistance(other.getLocation(), player.getLocation(), distanceAlgorithm) < distance)
                        validMembers.add(member);
                }
                else{
                    if(other.getWorld() == player.getWorld())
                        validMembers.add(member);
                }
            }
            else{
                validMembers.add(member);
            }
        });

        if(validMembers.size() == 1){
            data.giveExperience(exp, EXPSource.OTHER);
            return;
        }

        String formula = memberFormula.get(validMembers.size());
        formula = formula.replace("{exp}", Integer.toString(exp));
        final int Exp = (int) Calculator.eval(Properties.FormulaMethod, formula);
        validMembers.forEach((member) -> {
            member.giveExperience(Exp, EXPSource.OTHER);
        });
    }

}
