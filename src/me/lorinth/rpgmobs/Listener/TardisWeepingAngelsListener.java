package me.lorinth.rpgmobs.Listener;

import me.eccentric_nz.tardisweepingangels.TARDISWeepingAngelSpawnEvent;
import me.eccentric_nz.tardisweepingangels.utils.Monster;
import me.lorinth.rpgmobs.Data.CreatureModifierManager;
import me.lorinth.rpgmobs.Data.HealthBarManager;
import me.lorinth.rpgmobs.Enums.HealthBarType;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.CreatureData;
import me.lorinth.rpgmobs.Util.NameHelper;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class TardisWeepingAngelsListener implements Listener {

    @EventHandler
    public void onTardisSpawn(TARDISWeepingAngelSpawnEvent event){
        Monster type = event.getMonsterType();
        Entity entity = event.getEntity();

        if(!(entity instanceof LivingEntity)){
            return;
        }

        String entityType = type.name();
        CreatureData creatureData = LorinthsRpgMobs.GetCreatureDataManager().getCustomData((LivingEntity) entity, entityType);
        Integer level = LorinthsRpgMobs.GetLevelOfEntity(entity);
        if(level == null){
            return;
        }

        CreatureModifierManager.updateEntityWithData((LivingEntity) entity, level, creatureData);

        Bukkit.getScheduler().runTaskLater(LorinthsRpgMobs.instance, () -> {
            String customName = entity.getCustomName();
            NameHelper.setName(entity, customName);

            HealthBarManager healthBarManager = LorinthsRpgMobs.GetHealthBarManager();
            if(healthBarManager.getHealthBarType() == HealthBarType.Placeholder){
                customName = NameHelper.fillNameDetails((LivingEntity) entity, customName, healthBarManager.getBaseFormat());
                entity.setCustomName(customName);
            }
        }, 1);
    }

}
