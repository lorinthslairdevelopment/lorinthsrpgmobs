package me.lorinth.rpgmobs.Listener;

import me.lorinth.rpgmobs.Data.DataLoader;
import me.lorinth.rpgmobs.Data.FormulaManager;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.CreatureData;
import me.lorinth.rpgmobs.Variants.MobVariant;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.skills.api.events.SkillXPGainEvent;

public class SkillsProListener implements Listener {

    private DataLoader dataLoader;

    public SkillsProListener(DataLoader dataLoader){
        this.dataLoader = dataLoader;
    }

    @EventHandler
    public void onSkillsProExperience(SkillXPGainEvent event){
        Player player = event.getPlayer();
        Entity killed = event.getKilled();
        LivingEntity living = (LivingEntity) killed;

        if (dataLoader.getCreatureDataManager().isDisabled(living)){
            return;
        }

        CreatureData data = dataLoader.getCreatureDataManager().getData(living);
        MobVariant variant = LorinthsRpgMobs.GetMobVariantOfEntity(living);
        if(variant != null) {
            variant.onDeath(living);
        }

        Integer level = LorinthsRpgMobs.GetLevelOfEntity(living);
        if(level == null) {
            return;
        }

        //Calculate EXP / Money
        int exp = data.getExperienceAtLevel(level);
        float multiplier = dataLoader.getExperiencePermissionManager().getExperienceMultiplier(player);
        double afterPermissionExp =  (int) (exp * multiplier);
        exp = (int) FormulaManager.getExperienceResult(player, living, afterPermissionExp);

        event.setGained((double) exp);
    }

}
