package me.lorinth.rpgmobs.Listener;

import de.erethon.dungeonsxl.DungeonsXL;
import de.erethon.dungeonsxl.api.dungeon.Dungeon;
import de.erethon.dungeonsxl.api.event.mob.DungeonMobSpawnEvent;
import me.lorinth.rpgmobs.Data.CreatureModifierManager;
import me.lorinth.rpgmobs.Data.DungeonsDataManager;
import me.lorinth.rpgmobs.Objects.ValueRange;
import org.bukkit.ChatColor;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

public class DungeonsMobListener implements Listener {

    private final DungeonsXL dungeonsXL;

    public DungeonsMobListener(DungeonsXL dungeonsXL){
        this.dungeonsXL = dungeonsXL;
    }

    @EventHandler
    public void onDungeonMobSpawn(DungeonMobSpawnEvent event){
        LivingEntity entity = event.getBukkitEntity();
        Dungeon dungeon = dungeonsXL.getGame(entity.getWorld()).getDungeon();
        String dungeonId = ChatColor.stripColor(dungeon.getName()).replace(' ', '_');

        ValueRange range = DungeonsDataManager.getDungeonLevelRange(dungeonId);
        if(range != null){
            CreatureModifierManager.updateEntityWithLevel(entity, CreatureSpawnEvent.SpawnReason.CUSTOM, (int) range.getValue());
        }
    }

}
