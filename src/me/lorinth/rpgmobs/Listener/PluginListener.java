package me.lorinth.rpgmobs.Listener;

import me.lorinth.rpgmobs.LorinthsRpgMobs;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginEnableEvent;

public class PluginListener implements Listener {

    @EventHandler
    public void onPluginEnable(PluginEnableEvent e) {
        if (e.getPlugin().getName().equalsIgnoreCase("Heroes")) {
            LorinthsRpgMobs.getDataLoader().getHeroesDataManager().reload();
        }
        if (e.getPlugin().getName().equalsIgnoreCase("MythicMobs")){
            LorinthsRpgMobs.getDataLoader().getMythicMobsDataManager().reload();
        }
    }

}
