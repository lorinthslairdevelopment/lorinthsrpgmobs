package me.lorinth.rpgmobs.Listener;

import me.lorinth.rpgmobs.Enums.HealthBarType;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Tasks.ExecutedRunnable;
import me.lorinth.rpgmobs.Util.NameHelper;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntitySpawnEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

public class HealthBarListener implements Listener {

    private HealthBarType healthBarType;
    private String baseFormat;
    private TreeMap<Integer, String> percentFormat;

    private HashMap<UUID, String> entityNameStorage;
    private HashMap<UUID, ExecutedRunnable> entityHealthbarTask;

    public HealthBarListener(HealthBarType healthBarType, String baseFormat, TreeMap<Integer, String> percentFormat){
        this.healthBarType = healthBarType;
        this.baseFormat = baseFormat;
        this.percentFormat = percentFormat;

        entityNameStorage = new HashMap<>();
        entityHealthbarTask = new HashMap<>();
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntitySpawn(EntitySpawnEvent event){
        if(!(event.getEntity() instanceof LivingEntity)){
            return;
        }

        Bukkit.getScheduler().runTaskLater(LorinthsRpgMobs.instance, () -> {
            LivingEntity entity = (LivingEntity) event.getEntity();
            if(entity.getCustomName() == null){
                return;
            }

            String customName = entity.getCustomName();
            NameHelper.setName(entity, customName);

            if(healthBarType == HealthBarType.Placeholder){
                customName = NameHelper.fillNameDetails(entity, customName, baseFormat);
                entity.setCustomName(customName);
            }
        }, 1);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityHit(EntityDamageEvent event){
        Entity entity = event.getEntity();
        if(entity instanceof Player){
            return;
        }

        if(!(entity instanceof LivingEntity)){
            return;
        }

        Bukkit.getScheduler().runTaskLater(LorinthsRpgMobs.instance, () -> updateVisibleHealthBar((LivingEntity) entity), 1);
    }

    private void updateVisibleHealthBar(LivingEntity livingEntity){
        switch(healthBarType){
            case HealthBar:
                updateHealthBar(livingEntity);
                break;
            case Placeholder:
                updatePlaceholder(livingEntity);
                break;
            default:
                break;
        }
    }

    private void updateHealthBar(LivingEntity entity){
        UUID uuid = entity.getUniqueId();
        if(!entityNameStorage.containsKey(uuid)){
            entityNameStorage.put(uuid, entity.getCustomName());
        }
        ExecutedRunnable runnable = entityHealthbarTask.get(uuid);
        if(runnable != null && !runnable.hasExecuted()){
            runnable.cancel();
        }

        runnable = new ExecutedRunnable(() -> {
            entity.setCustomName(entityNameStorage.get(uuid));
        });
        Bukkit.getScheduler().runTaskLater(LorinthsRpgMobs.instance, runnable, 3 * 20);
        entityHealthbarTask.put(uuid, runnable);

        Map.Entry<Integer, String> entry = percentFormat.ceilingEntry(calculatePercentage(entity));
        String format = baseFormat;
        if(entry != null){
            format = entry.getValue();
        }

        entity.setCustomName(NameHelper.fillNameDetails(entity, NameHelper.getName(entity), format));
    }

    private void updatePlaceholder(LivingEntity entity){
        String baseName = NameHelper.getName(entity);
        Map.Entry<Integer, String> entry = percentFormat.ceilingEntry(calculatePercentage(entity));
        String format = baseFormat;
        if(entry != null){
            format = entry.getValue();
        }

        entity.setCustomName(NameHelper.fillNameDetails(entity, baseName, format));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityHitByPlayer(EntityDamageByEntityEvent event){
        Entity entity = event.getEntity();
        if(entity instanceof Player){
            return;
        }

        if(!(entity instanceof LivingEntity)){
            return;
        }

        Entity attacker = event.getDamager();
        if(!(attacker instanceof Player)){
            return;
        }

        Bukkit.getScheduler().runTaskLater(LorinthsRpgMobs.instance, () -> updateAttackerVisibleHealthBar((LivingEntity) entity, (Player) attacker), 1);
    }

    private void updateAttackerVisibleHealthBar(LivingEntity entity, Player attacker){
        switch(healthBarType){
            case ActionBar:
                updateActionBar(entity, attacker);
                return;
            /*case BossBar:
                updateBossbar(entity, attacker);
                return;
            */
            case Chat:
                updateChat(entity, attacker);
                return;
            default:
                break;
        }
    }

    private void updateActionBar(LivingEntity entity, Player attacker){
        String baseName = NameHelper.getName(entity);
        Map.Entry<Integer, String> entry = percentFormat.ceilingEntry(calculatePercentage(entity));
        String format = baseFormat;
        if(entry != null){
            format = entry.getValue();
        }

        attacker.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(NameHelper.fillNameDetails(entity, baseName, format)));
    }

    private void updateBossbar(LivingEntity entity, Player attacker){
        //TODO: Another time
    }

    private void updateChat(LivingEntity entity, Player attacker){
        String baseName = NameHelper.getName(entity);
        Map.Entry<Integer, String> entry = percentFormat.ceilingEntry(calculatePercentage(entity));
        String format = baseFormat;
        if(entry != null){
            format = entry.getValue();
        }

        attacker.sendMessage(NameHelper.fillNameDetails(entity, baseName, format));
    }

    private int calculatePercentage(LivingEntity entity){
        return (int) (entity.getHealth() * 100 / entity.getMaxHealth());
    }

}
