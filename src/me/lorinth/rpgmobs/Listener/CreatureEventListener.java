package me.lorinth.rpgmobs.Listener;

import io.lumine.xikage.mythicmobs.MythicMobs;
import me.lorinth.rpgmobs.Data.*;
import me.lorinth.rpgmobs.Events.RpgMobDeathEvent;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.CreatureData;
import me.lorinth.rpgmobs.Objects.Properties;
import me.lorinth.rpgmobs.Util.MetaDataConstants;
import me.lorinth.rpgmobs.Variants.BossVariant;
import me.lorinth.rpgmobs.Variants.MobVariant;
import me.lorinth.rpgmobs.Variants.WealthyVariant;
import me.lorinth.rpgmobs.handlers.bossSpawn.IBossHandler;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.loot.LootContext;
import org.bukkit.loot.LootTable;
import org.bukkit.loot.Lootable;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * Controls creature data and defaults
 */
public class CreatureEventListener implements Listener {

    private DataLoader dataLoader;
    private Random random;

    public CreatureEventListener(DataLoader dataLoader){
        this.dataLoader = dataLoader;
        random = new Random();
    }

    @EventHandler(ignoreCancelled = true)
    public void onEntitySpawn(EntitySpawnEvent event){
        Entity entity = event.getEntity();
        if(!(entity instanceof LivingEntity)) {
            return;
        }

        if(entity instanceof Creature) {
            return;
        }

        if(!dataLoader.getGriefPreventionDataManager().canSpawn(event.getLocation(), false)) {
            event.setCancelled(true);
            return;
        }

        if(CreatureModifierManager.replaceWithBossLevelRegion((LivingEntity) entity)){
            event.setCancelled(true);
            return;
        }
        CreatureModifierManager.updateEntity((LivingEntity) event.getEntity(), null);
    }

    @EventHandler(ignoreCancelled = true)
    public void onCreatureSpawn(CreatureSpawnEvent event){
        Entity entity = event.getEntity();
        if (entity instanceof WaterMob){
            return;
        }

        if(!dataLoader.getGriefPreventionDataManager().canSpawn(event.getLocation(), entity instanceof Monster)){
            event.setCancelled(true);
            return;
        }

        IBossHandler bossHandler = dataLoader.getBossHandler();
        if(bossHandler != null && dataLoader.getBossHandler().isBoss(entity)){
            return;
        }

        if(CreatureModifierManager.replaceWithBossLevelRegion(event.getEntity())){
            event.setCancelled(true);
            return;
        }

        CreatureModifierManager.updateEntity(event.getEntity(), event.getSpawnReason());
    }

    @EventHandler(ignoreCancelled = true)
    public void onPotionSplashEvent(PotionSplashEvent event){
        if(event.getPotion().getShooter() instanceof Witch){
            Witch witch = (Witch) event.getPotion().getShooter();
            Double damage = null;
            if(Properties.IsPersistentDataVersion){
                PersistentDataContainer dataContainer = witch.getPersistentDataContainer();
                damage = dataContainer.get(new NamespacedKey(LorinthsRpgMobs.instance, "damage"), PersistentDataType.DOUBLE);
            }
            else{
                if(!witch.hasMetadata(MetaDataConstants.Damage)){
                    return;
                }
                damage = witch.getMetadata(MetaDataConstants.Damage).get(0).asDouble();
            }

            if(damage != null){
                int damageFloored = (int) Math.floor(damage);
                ThrownPotion potion = event.getPotion();
                Collection<PotionEffect> effects = potion.getEffects();
                List<PotionEffect> removeEffects = new ArrayList<>();

                for(PotionEffect effect : effects){
                    if(effect.getType() == PotionEffectType.HARM){
                        removeEffects.add(effect);
                    }
                }

                for(PotionEffect effect : removeEffects){
                    potion.getEffects().add(new PotionEffect(effect.getType(), effect.getDuration(), damageFloored-1));
                    potion.getEffects().remove(effect);
                }
            }
        }

    }

    @EventHandler(ignoreCancelled = true)
    public void onTame(EntityTameEvent event){
        LivingEntity entity = event.getEntity();
        entity.setRemoveWhenFarAway(false);
    }

    @EventHandler(ignoreCancelled = true)
    public void onEntityHit(EntityDamageByEntityEvent event){
        if(event.getDamager() instanceof Player)
            return;

        Entity entity = event.getEntity();
        Entity damager = event.getDamager();
        if(entity instanceof LivingEntity){
            LivingEntity target = (LivingEntity) entity;

            MobVariant variant = LorinthsRpgMobs.GetMobVariantOfEntity(damager);
            if(variant != null)
                variant.onHit(target, event);
        }
        if(damager instanceof Projectile){
            ProjectileSource projectileSource = ((Projectile) damager).getShooter();
            if(projectileSource instanceof Entity){
                damager = (Entity) projectileSource;
            }
            else{
                return;
            }
        }
        if(!(damager instanceof LivingEntity)) {
            return;
        }
        LivingEntity living = (LivingEntity) damager;

        Double damage = null;
        if(Properties.IsPersistentDataVersion){
            PersistentDataContainer dataContainer = damager.getPersistentDataContainer();
            damage = dataContainer.get(new NamespacedKey(LorinthsRpgMobs.instance, "damage"), PersistentDataType.DOUBLE);
        }
        else{
            if(damager.hasMetadata(MetaDataConstants.Damage)) {
                damage = damager.getMetadata(MetaDataConstants.Damage).get(0).asDouble();
            }
        }

        if (Bukkit.getPluginManager().getPlugin("MythicMobs") != null){
            if (MythicMobs.inst().getAPIHelper().isMythicMob(damager)){
                return;
            }
        }

        if(damage != null){
            dataLoader.getHeroesDataManager().handleEntityDamageEvent(living, damage);
            event.setDamage(damage);
        }

        MobVariant variant = LorinthsRpgMobs.GetMobVariantOfEntity(entity);
        if(variant != null)
            variant.whenHit(living, event);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void changeCreatureLoot(EntityDeathEvent event){
        List<ItemStack> loot = LootManager.getDropsForCreature(event.getEntity());
        if(LootManager.shouldReplaceDrops() && LootManager.hasLootDefined(event.getEntityType())){
            event.getDrops().clear();
        }
        event.getDrops().addAll(loot);

        if(event.getEntity().getKiller() == null){
            return;
        }

        Player killer = event.getEntity().getKiller();
        LivingEntity entity = event.getEntity();
        if(Properties.IsLootableVersion && entity instanceof Lootable){
            Lootable lootable = (Lootable) entity;
            LootTable lootTable = lootable.getLootTable();
            if(lootTable == null){
                return;
            }

            LootContext context = new LootContext.Builder(entity.getLocation())
                                                .lootedEntity(entity)
                                                .killer(killer).build();

            int rolls = LootManager.getVanillaMultiplierForCreature(entity);
            for(int i=1; i<rolls; i++){
                event.getDrops().addAll(lootTable.populateLoot(random, context));
            }
        }

        for(String command : LootManager.getCommandRewardsForCreature(entity)){
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), preparseCommand(command, killer.getName()));
        }
    }

    private String preparseCommand(String command, String playername){
        return command.replace("{playername}", playername);
    }

    @EventHandler(ignoreCancelled = true)
    public void onCreatureDeath(EntityDeathEvent event){
        LivingEntity entity = event.getEntity();
        if(event.getDroppedExp() == 0 || entity.getKiller() == null) {
            return;
        }

        if (dataLoader.getCreatureDataManager().isDisabled(entity)){
            return;
        }

        CreatureData data = dataLoader.getCreatureDataManager().getData(entity);
        MobVariant variant = LorinthsRpgMobs.GetMobVariantOfEntity(entity);
        if(variant != null) {
            variant.onDeath(entity);
        }

        Integer level = LorinthsRpgMobs.GetLevelOfEntity(entity);
        if(level == null) {
            return;
        }

        //Calculate EXP / Money
        int exp = data.getExperienceAtLevel(level);
        double currencyValue = 0;
        double currencyChance = data.getCurrencyChanceAtLevel(level);
        double roll = random.nextDouble() * 100.0;

        if(currencyChance > roll || variant instanceof WealthyVariant) {
            currencyValue = data.getCurrencyValueAtLevel(level);
            currencyValue = FormulaManager.getMoneyResult(entity.getKiller(), entity, currencyValue);
        }

        //Modify result based on permission
        Player player = getKillerFromEntity(event);
        float multiplier = dataLoader.getExperiencePermissionManager().getExperienceMultiplier(player);
        exp =  (int) (exp * multiplier);
        exp = (int) FormulaManager.getExperienceResult(player, entity, exp);

        RpgMobDeathEvent rpgMobDeathEvent = new RpgMobDeathEvent(player, entity, level, variant, exp, currencyValue);
        if (variant instanceof BossVariant){
            variant.onDeathEvent(rpgMobDeathEvent);
        }

        Bukkit.getPluginManager().callEvent(rpgMobDeathEvent);
        if(rpgMobDeathEvent.isCancelled()) {
            return;
        }

        if(variant != null && !(variant instanceof BossVariant)) {
            variant.onDeathEvent(rpgMobDeathEvent);
        }

        int currency = (int)Math.floor(rpgMobDeathEvent.getCurrencyValue());
        if(currency > 0){
            VaultManager.addMoney(player, entity, currency);
        }

        if(rpgMobDeathEvent.getExp() <= 0) {
            if(dataLoader.getBattleLevelsManager().isEnabled()){
                return;
            }
            else if (dataLoader.getHeroesDataManager().isEnabled()){
                return;
            }
            else if(dataLoader.getSkillAPIDataManager().isEnabled()) {
                return;
            }
            else if(dataLoader.getMcMMoManager().isEnabled()){
                return;
            }
            else{
                event.setDroppedExp(rpgMobDeathEvent.getExp());
            }
            return;
        }

        if(dataLoader.getBattleLevelsManager().handleEntityDeathEvent(event, player, rpgMobDeathEvent.getExp(), rpgMobDeathEvent.getCurrencyValue())){
            return;
        }
        else if (dataLoader.getHeroesDataManager().handleEntityDeathEvent(event, player, rpgMobDeathEvent.getExp(), rpgMobDeathEvent.getCurrencyValue())){
            return;
        }
        else if(dataLoader.getSkillAPIDataManager().handleEntityDeathEvent(event, player, rpgMobDeathEvent.getExp(), rpgMobDeathEvent.getCurrencyValue())) {
            return;
        }
        else if(dataLoader.getSkillsProDataManager().isEnabled()){
            //Listener is handling experience drop
            return;
        }
        else if(dataLoader.getMcMMoManager().isDisabled()){
            event.setDroppedExp(rpgMobDeathEvent.getExp());
        }

        ExperienceDisplayManager.ShowExperienceToPlayer(player, rpgMobDeathEvent.getExp());
    }

    private Player getKillerFromEntity(EntityDeathEvent event){
        EntityDamageEvent lastDamageEvent = event.getEntity().getLastDamageCause();

        Player killer = event.getEntity().getKiller();
        killer = killer != null ? killer : getKillerFromTamedEntity(lastDamageEvent);

        return killer;
    }

    private Player getKillerFromTamedEntity(EntityDamageEvent lastDamageEvent){
        if(lastDamageEvent instanceof EntityDamageByEntityEvent){
            Entity damager = ((EntityDamageByEntityEvent) lastDamageEvent).getDamager();
            if(damager instanceof Player){
                return (Player) damager;
            }
            if(damager instanceof Tameable){
                AnimalTamer tamer = ((Tameable) damager).getOwner();
                if(tamer instanceof Player)
                    return (Player) tamer;
            }
        }

        return null;
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerDeath(PlayerDeathEvent event)
    {
        if (event.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent damageEvent = (EntityDamageByEntityEvent) event
                    .getEntity().getLastDamageCause();

            Entity damager = damageEvent.getDamager();

            if (damager == null) {
                return;
            }

            MobVariant variant = LorinthsRpgMobs.GetMobVariantOfEntity(damager);
            if(variant != null)
                variant.onPlayerDeath(event);
        }
    }
}
