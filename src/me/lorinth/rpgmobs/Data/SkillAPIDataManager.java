package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.Listener.SkillAPIListener;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Objects.CreatureDeathData;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Disableable;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.plugin.Plugin;

public class SkillAPIDataManager extends Disableable implements DataManager {

    private SkillAPIListener skillAPIEventListener;

    public void loadData(FileConfiguration config, Plugin plugin){
        if(!ConfigHelper.ConfigContainsPath(config, "SkillAPI.Enabled")){
            RpgMobsOutputHandler.PrintInfo("SkillAPI options not found, Generating...");
            setDefaults(config, plugin);
        }

        if(Bukkit.getServer().getPluginManager().getPlugin("SkillAPI") == null)
            this.setDisabled(true);
        else{
            this.setDisabled(!config.getBoolean("SkillAPI.Enabled"));
        }

        if(this.isDisabled())
            RpgMobsOutputHandler.PrintInfo("SkillAPI Integration is Disabled");
        else {
            RpgMobsOutputHandler.PrintInfo("SkillAPI Integration is Enabled!");
            skillAPIEventListener = new SkillAPIListener();
            Bukkit.getPluginManager().registerEvents(skillAPIEventListener, plugin);
        }
    }

    //Unused method
    public boolean saveData(FileConfiguration config){
        return false;
    }

    public boolean handleEntityDeathEvent(EntityDeathEvent deathEvent, Player player, int exp, double currency){
        if(this.isDisabled())
            return false;

        skillAPIEventListener.bindDeathEvent(player, new CreatureDeathData(exp, currency, deathEvent.getEntity()));
        return true;
    }

    private void setDefaults(FileConfiguration config, Plugin plugin){
        config.set("SkillAPI.Enabled", false);
        plugin.saveConfig();
    }
}
