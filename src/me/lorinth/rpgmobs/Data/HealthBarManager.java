package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.Enums.HealthBarType;
import me.lorinth.rpgmobs.Listener.HealthBarListener;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Disableable;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import java.util.TreeMap;

public class HealthBarManager extends Disableable implements DataManager {

    private HealthBarType healthBarType;
    private String baseFormat;

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        if(!ConfigHelper.ConfigContainsPath(config, "HealthBar.Enabled")){
            RpgMobsOutputHandler.PrintInfo("HealthBar options not found, Generating...");
            setDefaults(config, plugin);
        }

        setEnabled(config.getBoolean("HealthBar.Enabled"));

        if(isDisabled()){
            return;
        }

        String typeValue = config.getString("HealthBar.Type");
        if(!TryParse.parseEnum(HealthBarType.class, typeValue)){
            setDisabled(true);
            String types = "";
            for(HealthBarType type : HealthBarType.values()){
                types += type.name() + ", ";
            }
            types = types.substring(0, types.length()-2);

            RpgMobsOutputHandler.PrintError("Invalid HealthBar.Type " +
                    RpgMobsOutputHandler.HIGHLIGHT + typeValue +
                    RpgMobsOutputHandler.ERROR + " please use one of the valid types" +
                    RpgMobsOutputHandler.HIGHLIGHT + types);
            return;
        }
        healthBarType = HealthBarType.valueOf(typeValue);

        baseFormat = ChatColor.translateAlternateColorCodes('&', config.getString("HealthBar.Format"));
        TreeMap<Integer, String> percentBasedFormat = new TreeMap<>();
        for(String percentStringValue : config.getConfigurationSection("HealthBar.PercentBasedFormat").getKeys(false)){
            if(!TryParse.parseInt(percentStringValue)){
                //throw error
                continue;
            }

            int percentValue = Integer.parseInt(percentStringValue);
            String percentFormat = config.getString("HealthBar.PercentBasedFormat." + percentStringValue);

            percentBasedFormat.put(percentValue, ChatColor.translateAlternateColorCodes('&', percentFormat));
        }

        Listener listener = new HealthBarListener(healthBarType, baseFormat, percentBasedFormat);
        Bukkit.getPluginManager().registerEvents(listener, plugin);
        RpgMobsOutputHandler.PrintInfo("HealthBar system enabled! {Type: " + typeValue + "}");
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    private void setDefaults(FileConfiguration config, Plugin plugin){
        config.set("HealthBar.Enabled", false);
        config.set("HealthBar.Type", "Placeholder");
        config.set("HealthBar.Format", "{name} &7- &a{current} &7/ &a{max} &e{percent}%");
        config.set("HealthBar.PercentBasedFormat.50", "{name} &7- &e{current} &7/ &a{max} &e{percent}%");
        config.set("HealthBar.PercentBasedFormat.20", "{name} &7- &c{current} &7/ &a{max} &e{percent}%");
        plugin.saveConfig();
    }

    public HealthBarType getHealthBarType(){
        if(isDisabled()){
            return null;
        }

        return healthBarType;
    }

    public String getBaseFormat(){
        if(isDisabled()){
            return null;
        }

        return baseFormat;
    }

}
