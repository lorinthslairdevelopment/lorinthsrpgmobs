package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.Objects.*;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.handlers.bossSpawn.BossSpawnHandler;
import me.lorinth.rpgmobs.handlers.bossSpawn.IBossHandler;
import me.lorinth.rpgmobs.handlers.bossSpawn.MythicMobsSpawnHandler;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.UUID;

/**
 * This class shouldn't be used outside of the plugin
 */
public class DataLoader implements DataManager {

    private static DistanceAlgorithm distanceAlgorithm = DistanceAlgorithm.Diamond;
    private static boolean ignoreSpawnerMobs = true;
    private static ArrayList<UUID> ignoredEntities = new ArrayList<>();
    private static DataLoader instance;

    private BattleLevelsManager battleLevelsManager = new BattleLevelsManager();
    private CreatureDataManager creatureDataManager = new CreatureDataManager();
    private CreatureModifierManager creatureModifierManager = new CreatureModifierManager(this);
    private DungeonsDataManager dungeonsDataManager = new DungeonsDataManager();
    private EliteMobsDataManager eliteMobsDataManager = new EliteMobsDataManager();
    private ExperienceDisplayManager experienceDisplayManager = new ExperienceDisplayManager();
    private ExperiencePermissionManager experiencePermissionManager = new ExperiencePermissionManager();
    private GriefPreventionDataManager griefPreventionDataManager = new GriefPreventionDataManager();
    private HealthBarManager healthBarManager = new HealthBarManager();
    private HeroesDataManager heroesDataManager = new HeroesDataManager();
    private JobsRebornDataManager jobsRebornDataManager = new JobsRebornDataManager();
    private LevelRegionManager levelRegionManager = new LevelRegionManager();
    private McMMoManager mcMMoManager = new McMMoManager();
    private MmoCoreDataManager mmoCoreDataManager = new MmoCoreDataManager();
    private MobVariantDataManager mobVariantDataManager = new MobVariantDataManager();
    private MythicDropsDataManager mythicDropsDataManager = new MythicDropsDataManager(this);
    private MythicMobsDataManager mythicMobsDataManager = new MythicMobsDataManager();
    private SkillAPIDataManager skillAPIDataManager = new SkillAPIDataManager();
    private SkillsProDataManager skillsProDataManager = new SkillsProDataManager(this);
    private SpawnPointManager spawnPointManager = new SpawnPointManager();
    private TardisDataManager tardisDataManager = new TardisDataManager();
    private VaultManager vaultManager = new VaultManager();

    private IBossHandler bossHandler;

    public DataLoader(){
        instance = this;
    }

    public int calculateLevel(Location location, Entity entity){
        LevelRegion region = levelRegionManager.getHighestPriorityLeveledRegionAtLocation(location);
        if(region != null && entity != null) {
            if (region.entityIsDisabled(entity)) {
                return -1;
            }
        }

        if(region != null && region.getLevel() != -1) {
            return region.getLevel();
        }
        else{
            SpawnPoint closestSpawnPoint = spawnPointManager.getSpawnPointForLocation(location);
            if(closestSpawnPoint != null) {
                return closestSpawnPoint.calculateLevel(location, distanceAlgorithm);
            }
        }

        return -1; //this world is disabled if -1
    }

    public int calculateLevel(Location location, Entity entity, boolean useRangeOrVariance){
        LevelRegion region = levelRegionManager.getHighestPriorityLeveledRegionAtLocation(location);
        if(region != null && entity != null) {
            if (region.entityIsDisabled(entity)) {
                return -1;
            }
        }

        if(region != null && region.getLevel() != -1) {
            if (!useRangeOrVariance){
                return region.getMinLevel();
            }
            return region.getLevel();
        }
        else{
            SpawnPoint closestSpawnPoint = spawnPointManager.getSpawnPointForLocation(location);
            if(closestSpawnPoint != null) {
                if (!useRangeOrVariance){
                    return closestSpawnPoint.calculateRawLevel(location, distanceAlgorithm);
                }
                return closestSpawnPoint.calculateLevel(location, distanceAlgorithm);
            }
        }

        return -1; //this world is disabled if -1
    }

    public static LevelRegion getRegion(Location location){
        LevelRegion region = instance.levelRegionManager.getHighestPriorityLeveledRegionAtLocation(location);
        return region;
    }

    public static SpawnPoint getSpawnPoint(Location location){
        return instance.spawnPointManager.getSpawnPointForLocation(location);
    }

    public BattleLevelsManager getBattleLevelsManager() {
        return battleLevelsManager;
    }

    public IBossHandler getBossHandler(){
        return bossHandler;
    }

    public CreatureDataManager getCreatureDataManager(){
        return creatureDataManager;
    }

    public CreatureModifierManager getCreatureModifierManager(){
        return creatureModifierManager;
    }

    public ExperiencePermissionManager getExperiencePermissionManager(){ return experiencePermissionManager; }

    public GriefPreventionDataManager getGriefPreventionDataManager() { return griefPreventionDataManager; }

    public HealthBarManager getHealthBarManager() {
        return healthBarManager;
    }

    public HeroesDataManager getHeroesDataManager(){
        return heroesDataManager;
    }

    public JobsRebornDataManager getJobsRebornDataManager(){
        return jobsRebornDataManager;
    }

    public LevelRegionManager getLevelRegionManager(){
        return levelRegionManager;
    }

    public McMMoManager getMcMMoManager(){
        return mcMMoManager;
    }

    public MobVariantDataManager getMobVariantManager(){ return mobVariantDataManager; }

    public MythicDropsDataManager getMythicDropsDataManager(){ return mythicDropsDataManager; }

    public MythicMobsDataManager getMythicMobsDataManager() { return mythicMobsDataManager; }

    public SkillAPIDataManager getSkillAPIDataManager(){ return skillAPIDataManager; }

    public SkillsProDataManager getSkillsProDataManager() {
        return skillsProDataManager;
    }

    public SpawnPointManager getSpawnPointManager(){
        return spawnPointManager;
    }

    public TardisDataManager getTardisDataManager() {
        return tardisDataManager;
    }

    public static DistanceAlgorithm getDistanceAlgorithm(){
        return distanceAlgorithm;
    }

    public boolean saveData(FileConfiguration config){
        return spawnPointManager.saveData(config) || levelRegionManager.saveData(config) || creatureDataManager.saveData(config) ||
                heroesDataManager.saveData(config) || griefPreventionDataManager.saveData(config) || skillAPIDataManager.saveData(config) ||
                skillsProDataManager.saveData(config) ||
                mythicDropsDataManager.saveData(config) || mythicMobsDataManager.saveData(config) || mobVariantDataManager.saveData(config) ||
                experiencePermissionManager.saveData(config);
    }

    public void loadData(FileConfiguration config, Plugin plugin){
        checkForBossHandlers();
        loadGlobalOptions(config, plugin);
        battleLevelsManager.loadData(config, plugin);
        creatureDataManager.loadData(config, plugin);
        dungeonsDataManager.loadData(config, plugin);
        eliteMobsDataManager.loadData(config, plugin);
        experienceDisplayManager.loadData(config, plugin);
        experiencePermissionManager.loadData(config, plugin);
        griefPreventionDataManager.loadData(config, plugin);
        healthBarManager.loadData(config, plugin);
        heroesDataManager.loadData(config, plugin);
        jobsRebornDataManager.loadData(config, plugin);
        levelRegionManager.loadData(config, plugin);
        mcMMoManager.loadData(config, plugin);
        mmoCoreDataManager.loadData(config, plugin);
        mobVariantDataManager.loadData(config, plugin);
        mythicDropsDataManager.loadData(config, plugin);
        mythicMobsDataManager.loadData(config, plugin);
        skillAPIDataManager.loadData(config, plugin);
        skillsProDataManager.loadData(config, plugin);
        spawnPointManager.loadData(config, plugin);
        tardisDataManager.loadData(config, plugin);
        vaultManager.loadData(config, plugin);
    }

    private void loadGlobalOptions(FileConfiguration config, Plugin plugin){
        Properties.NameTagsAlwaysOn = config.getBoolean("Names.TagsAlwaysOn");
        Properties.NameFormat = ChatColor.translateAlternateColorCodes('&', config.getString("Names.Format"));
        loadDistanceAlgorithm(config, plugin);
        loadEquipmentOverrides(config, plugin);
        loadIgnoreSpawnerMobs(config, plugin);
    }

    private void loadDistanceAlgorithm(FileConfiguration config, Plugin plugin){
        String algo = config.getString("DistanceAlgorithm");
        try{
            distanceAlgorithm = DistanceAlgorithm.valueOf(algo);
        }
        catch(Exception error){
            RpgMobsOutputHandler.PrintError("Distance Algorithm : " + RpgMobsOutputHandler.HIGHLIGHT + algo + RpgMobsOutputHandler.ERROR + " is not a valid Algorithm " + RpgMobsOutputHandler.HIGHLIGHT + "(Circle/Diamond/Square)");
            RpgMobsOutputHandler.PrintError("Using " + RpgMobsOutputHandler.HIGHLIGHT + "Diamond" + RpgMobsOutputHandler.ERROR + " Distance Algorithm");
            distanceAlgorithm = DistanceAlgorithm.Diamond;
        }
    }

    private void loadIgnoreSpawnerMobs(FileConfiguration config, Plugin plugin){
        if(ConfigHelper.ConfigContainsPath(config, "IgnoreSpawnerMobs")){
            ignoreSpawnerMobs = config.getBoolean("IgnoreSpawnerMobs");
        }
        else{
            RpgMobsOutputHandler.PrintError("Couldn't find value 'IgnoreSpawnerMobs' in config.yml. Using " + RpgMobsOutputHandler.HIGHLIGHT + "IgnoreSpawnerMobs: true" + RpgMobsOutputHandler.ERROR + " as default");
            config.set("IgnoreSpawnerMobs", true);
            ignoreSpawnerMobs = true;
        }
    }

    private void loadEquipmentOverrides(FileConfiguration config, Plugin plugin){
        Properties.VanillaMobEquipmentOverrides = config.getBoolean("VanillaMobEquipmentOverrides");
        Properties.ForceRpgMobsEquipment = config.getBoolean("ForceRpgMobsEquipment");
    }

    public boolean ignoresSpawnerMobs(){
        return ignoreSpawnerMobs;
    }

    public void ignoreEntity(Entity entity){
        ignoredEntities.add(entity.getUniqueId());
    }

    public boolean isIgnoredEntity(Entity entity, boolean remove){
        boolean isIgnored = ignoredEntities.contains(entity.getUniqueId());
        if(remove)
            ignoredEntities.remove(entity.getUniqueId());
        return isIgnored;

    }

    private void checkForBossHandlers(){
        checkForBossPluginHandler();
        checkForMythicMobsSpawnHandler();
    }

    private void checkForBossPluginHandler() {
        try{
            Plugin bossPlugin = Bukkit.getPluginManager().getPlugin("Boss");
            if (bossPlugin != null && bossPlugin.isEnabled()){
                bossHandler = new BossSpawnHandler();
                RpgMobsOutputHandler.PrintInfo("Detected Boss plugin for Boss spawning!");
            }
        }
        catch(Exception e){
            RpgMobsOutputHandler.PrintException("Failed to find Boss plugin", e);
            bossHandler = null;
        }
    }

    private void checkForMythicMobsSpawnHandler() {
        try{
            Plugin mythicMobsPlugin = Bukkit.getPluginManager().getPlugin("MythicMobs");
            if (mythicMobsPlugin != null && mythicMobsPlugin.isEnabled()){
                bossHandler = new MythicMobsSpawnHandler();
                RpgMobsOutputHandler.PrintInfo("Detected MythicMobs plugin for Boss spawning!");
            }
        }
        catch(Exception e){
            RpgMobsOutputHandler.PrintException("Failed to find MythicMobs plugin", e);
            bossHandler = null;
        }
    }

}
