package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.Objects.DataManager;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

public class MessageManager implements DataManager {

    public static String MoneyDrop;
    public static boolean MoneyDropMessage_Enabled = true;
    public static String PlayerMobLevelNotifyIncrease;
    public static String PlayerMobLevelNotifyDecrease;

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        MoneyDrop = ChatColor.translateAlternateColorCodes('&', config.getString("MoneyDrop"));
        MoneyDropMessage_Enabled = config.getBoolean("MoneyDropMessage_Enabled");
        PlayerMobLevelNotifyIncrease = ChatColor.translateAlternateColorCodes('&', config.getString("PlayerMobLevelNotifyIncrease"));
        PlayerMobLevelNotifyDecrease = ChatColor.translateAlternateColorCodes('&', config.getString("PlayerMobLevelNotifyDecrease"));
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

}
