package me.lorinth.rpgmobs.Data;

import de.erethon.dungeonsxl.DungeonsXL;
import me.lorinth.rpgmobs.Listener.DungeonsMobListener;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Disableable;
import me.lorinth.rpgmobs.Objects.ValueRange;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;

public class DungeonsDataManager extends Disableable implements DataManager {

    private static HashMap<String, ValueRange> dungeonLevels;
    private final String path = "DungeonsXL.";

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        if(!ConfigHelper.ConfigContainsPath(config, path + "Enabled")){
            RpgMobsOutputHandler.PrintInfo("DungeonsXL options not found, Generating...");
            setDefaults(config, plugin);
        }

        dungeonLevels = new HashMap<>();
        Plugin dungeonsXL = Bukkit.getPluginManager().getPlugin("DungeonsXL");
        boolean enabled = config.getBoolean(path + "Enabled");

        setDisabled(!enabled || dungeonsXL == null);
        if(!isDisabled()){
            RpgMobsOutputHandler.PrintInfo("DungeonsXL Integration is Enabled!");
            for(String id : config.getConfigurationSection(path + "DungeonLevels").getKeys(false)){
                String value = config.getString(path + "DungeonLevels." + id);
                ValueRange range = new ValueRange(value);
                if(!range.isValid())
                    RpgMobsOutputHandler.PrintError("Error loading value range, " + RpgMobsOutputHandler.HIGHLIGHT + value + RpgMobsOutputHandler.ERROR + ", in config.yml [" + path + "DungeonLevels." + id + "]");
                else
                    dungeonLevels.put(id, range);
            }

            if(dungeonLevels.size() != 0)
                Bukkit.getPluginManager().registerEvents(new DungeonsMobListener((DungeonsXL) dungeonsXL), plugin);
        }else if(enabled){
            RpgMobsOutputHandler.PrintError("DungeonsXL enabled in config, but plugin not found! Disabling...");
        }
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    public static ValueRange getDungeonLevelRange(String id){
        return dungeonLevels.getOrDefault(id, null);
    }

    private void setDefaults(FileConfiguration config, Plugin plugin){
        config.set(path + "Enabled", false);
        config.set(path + "DungeonLevels.dungeon_a", new ValueRange("10-12").toString());
        plugin.saveConfig();
    }
}
