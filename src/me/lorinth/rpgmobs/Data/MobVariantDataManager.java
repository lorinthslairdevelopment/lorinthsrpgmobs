package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Disableable;
import me.lorinth.rpgmobs.Objects.RandomCollection;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Variants.*;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.plugin.Plugin;

import java.util.*;

public class MobVariantDataManager extends Disableable implements DataManager {

    private static HashMap<String, RandomCollection<MobVariant>> entityTypeVariants;
    private static ArrayList<String> disabledEntityTypes;
    private static HashMap<String, MobVariant> mobVariants = new HashMap<>();
    private static TreeMap<Integer, RandomCollection<MobVariant>> mobVariantWeightsPerLevel = new TreeMap<>();
    private static boolean disabled = false;
    private static boolean isWeightsPerLevelEnabled;
    private static int variantChance = 0;
    private static Random random = new Random();

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        entityTypeVariants = new HashMap<>();
        disabledEntityTypes = new ArrayList<>();

        if(!ConfigHelper.ConfigContainsPath(config, "MobVariants.Disabled")){
            RpgMobsOutputHandler.PrintInfo("Mob Variants options not found, Generating...");
            setDefaults(config, plugin);
        }

        if(config.getBoolean("MobVariants.Disabled")) {
            RpgMobsOutputHandler.PrintInfo("Mob Variants Disabled!");
            disabled = true;
            return;
        }

        variantChance = config.getInt("MobVariants.VariantChance");
        disabledEntityTypes.addAll(config.getStringList("MobVariants.DisabledTypes"));
        loadInternalVariants(config);
        loadLevelSpecificWeights(config, plugin);
        RpgMobsOutputHandler.PrintInfo("Mob Variants Enabled!");
    }

    //Variants self load/add when instantiated
    private void loadInternalVariants(FileConfiguration config){
        if(mobVariants.size() == 0){
            new AcidBombVariant();
            new BlindingVariant();
            new BossVariant();
            new BurningVariant();
            new ConfettiVariant();
            new ExplosiveVariant();
            new FastVariant();
            new ForcefulVariant();
            new GlowingVariant();
            new HealingVariant();
            new InvisibleVariant();
            new JokerVariant();
            new MagmaVariant();
            new PoisonousVariant();
            new SlowVariant();
            new StrongVariant();
            new SturdyVariant();
            new ToughVariant();
            new WealthyVariant();
        }
        else{
            for(MobVariant variant : mobVariants.values()){
                variant.load(config, LorinthsRpgMobs.instance);
            }
        }
    }

    private void loadLevelSpecificWeights(FileConfiguration config, Plugin plugin){
        String localPath = "MobVariants.VariantWeightsPerLevel";
        if(!ConfigHelper.ConfigContainsPath(config, localPath + ".Enabled")){
            config.set(localPath + ".Enabled", false);
            for(MobVariant mobVariant : mobVariants.values()){
                config.set(localPath + ".1." + mobVariant.getName(), mobVariant.getWeight());
            }
            plugin.saveConfig();
        }

        mobVariantWeightsPerLevel = new TreeMap<>();
        isWeightsPerLevelEnabled = config.getBoolean(localPath + ".Enabled");
        if(isWeightsPerLevelEnabled){
            for(String levelValue : config.getConfigurationSection("MobVariants.VariantWeightsPerLevel").getKeys(false)){
                if(levelValue.equalsIgnoreCase("Enabled")){
                    continue;
                }

                if(!TryParse.parseInt(levelValue)){
                    RpgMobsOutputHandler.PrintError(levelValue + " is not a valid level [Path: " + "MobVariants.VariantWeightsPerLevel" + "]");
                    continue;
                }
                int level = Integer.parseInt(levelValue);
                RandomCollection<MobVariant> randomVariants = new RandomCollection<>();
                for(String mobVariantName : config.getConfigurationSection("MobVariants.VariantWeightsPerLevel." + levelValue).getKeys(false)){
                    MobVariant mobVariant = mobVariants.get(mobVariantName);
                    if(mobVariant == null){
                        RpgMobsOutputHandler.PrintError(RpgMobsOutputHandler.HIGHLIGHT + mobVariantName + RpgMobsOutputHandler.ERROR + " is not a valid/enabled Mob Variant [Path: " + "MobVariants.VariantWeightsPerLevel." + levelValue + "]");
                        continue;
                    }

                    String weightValue = config.getString("MobVariants.VariantWeightsPerLevel." + levelValue + "." + mobVariantName);
                    if(!TryParse.parseDouble(weightValue)){
                        RpgMobsOutputHandler.PrintError(RpgMobsOutputHandler.HIGHLIGHT + weightValue + RpgMobsOutputHandler.ERROR + " is not a valid decimal number [Path: " + "MobVariants.VariantWeightsPerLevel." + levelValue + "." + mobVariantName + "]");
                        continue;
                    }
                    randomVariants.add(Double.parseDouble(weightValue), mobVariant);
                }
                mobVariantWeightsPerLevel.put(level, randomVariants);
            }
        }
    }

    public static void AddVariant(MobVariant variant){
        if(variant.getWeight() == 0){
            return;
        }
        if(!mobVariants.containsKey(variant.getName())){
            mobVariants.put(variant.getName(), variant);
        }
        for(EntityType type : EntityType.values()){
            if(!variant.isDisabledEntityType(type)){
                RandomCollection<MobVariant> variants = entityTypeVariants.getOrDefault(type.name(), new RandomCollection<>());
                variants.add(variant.getWeight(), variant);
                entityTypeVariants.put(type.name(), variants);
            }
        }
    }

    public static MobVariant GetVariant(String name){
        return mobVariants.get(name);
    }

    public static MobVariant TryApplyVariant(LivingEntity entity){
        if(disabled ||
            disabledEntityTypes.contains(entity.getType().name()) ||
            LorinthsRpgMobs.IsMythicMob(entity))
            return null;

        if(random.nextInt(100) <= variantChance){
            return SelectVariant(entity);
        }
        return null;
    }

    public static MobVariant SelectVariant(LivingEntity entity){
        if(isWeightsPerLevelEnabled){
            return getMobVariantLevelBased(entity);
        }
        else{
            return getMobVariantGlobalBased(entity);
        }
    }

    private static MobVariant getMobVariantLevelBased(LivingEntity entity){
        int tries = 0;
        int level = LorinthsRpgMobs.GetLevelOfEntity(entity);
        Map.Entry<Integer, RandomCollection<MobVariant>> entry = mobVariantWeightsPerLevel.floorEntry(level);
        //If level system doesn't support this level, use old global weight system
        if(entry == null){
            return getMobVariantGlobalBased(entity);
        }

        RandomCollection<MobVariant> randomMobVariants = entry.getValue();
        while(tries < 2){
            MobVariant mobVariant = randomMobVariants.next();
            if(!mobVariant.isDisabledEntityType(entity.getType()) && mobVariant.apply(entity)){
                return mobVariant;
            }
            tries++;
        }

        return null;
    }

    private static MobVariant getMobVariantGlobalBased(LivingEntity entity){
        int tries = 0;
        RandomCollection<MobVariant> randomMobVariants = entityTypeVariants.get(entity.getType().name());
        while(tries < 2){
            MobVariant mobVariant = randomMobVariants.next();
            if(mobVariant.apply(entity)){
                return mobVariant;
            }
            tries++;
        }

        return null;
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    private void setDefaults(FileConfiguration config, Plugin plugin){
        config.set("MobVariants.Disabled", true);
        config.set("MobVariants.VariantChance", 40);
        config.set("MobVariants.DisabledTypes", new ArrayList<String>(){{ add(EntityType.CREEPER.name()); }});
        plugin.saveConfig();
    }
}
