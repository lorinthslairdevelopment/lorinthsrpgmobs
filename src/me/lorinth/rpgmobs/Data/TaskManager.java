package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.Objects.TaskOptions.PlayerMobLevelNotifyOptions;
import me.lorinth.rpgmobs.Objects.DataManager;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

public class TaskManager implements DataManager {

    public static PlayerMobLevelNotifyOptions playerMobLevelNotifyOptions;

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        playerMobLevelNotifyOptions = new PlayerMobLevelNotifyOptions(config, "PlayerMobLevelNotify");
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }
}
