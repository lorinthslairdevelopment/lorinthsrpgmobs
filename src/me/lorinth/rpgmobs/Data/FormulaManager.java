package me.lorinth.rpgmobs.Data;

import me.clip.placeholderapi.PlaceholderAPI;
import me.lorinth.rpgmobs.Enums.LevelHook;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Formula.ModifierFormulas;
import me.lorinth.rpgmobs.Objects.Properties;
import me.lorinth.rpgmobs.Util.LevelHelper;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.Calculator;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

public class FormulaManager implements DataManager {

    private static ModifierFormulas experienceModiferFormulas;
    private static ModifierFormulas moneyModiferFormulas;
    private static boolean placeholderApiFound = false;

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        experienceModiferFormulas = new ModifierFormulas();
        moneyModiferFormulas = new ModifierFormulas();

        placeholderApiFound = Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null;
        if(placeholderApiFound){
            RpgMobsOutputHandler.PrintInfo("PlaceholderAPI Found! Able to use placeholders in formulas.yml");
        }

        String path = "ExperienceModifierFormula.";
        experienceModiferFormulas.enabled = config.getBoolean(path + "Enabled");
        experienceModiferFormulas.MobLevelHigher = config.getString(path + "MobLevelHigher");
        experienceModiferFormulas.MobLevelEqual = config.getString(path + "MobLevelEqual");
        experienceModiferFormulas.MobLevelLower = config.getString(path + "MobLevelLower");

        path = "MoneyModifierFormula.";
        moneyModiferFormulas.enabled = config.getBoolean(path + "Enabled");
        moneyModiferFormulas.MobLevelHigher = config.getString(path + "MobLevelHigher");
        moneyModiferFormulas.MobLevelEqual = config.getString(path + "MobLevelEqual");
        moneyModiferFormulas.MobLevelLower = config.getString(path + "MobLevelLower");
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    public static double getExperienceResult(@NotNull Player killer, Entity target, double experience){
        return getResultFromModifierFormulas(experienceModiferFormulas, killer, target, experience, "{exp}");
    }

    public static double getMoneyResult(@NotNull Player killer, Entity target, double money){
        return getResultFromModifierFormulas(moneyModiferFormulas, killer, target, money, "{money}");
    }

    public static double getResultFromModifierFormulas(ModifierFormulas formulas, Player killer, Entity target, double value, String valueKey){
        if(target instanceof Player){
            return value;
        }
        else if(!formulas.enabled){
            return value;
        }

        int mobLevel = LorinthsRpgMobs.GetLevelOfEntity(target);
        int playerLevel = 1;
        if (LorinthsRpgMobs.getLevelHook() == LevelHook.RpgItems){
            playerLevel = mobLevel;
        }
        else {
            LevelHelper.getPlayerLevel(killer);
        }

        return getResultFromModifierFormulas(formulas, killer, playerLevel, mobLevel, value, valueKey);
    }

    public static double getResultFromModifierFormulas(ModifierFormulas formulas, Player killer, int playerLevel, int mobLevel, double value, String valueKey){
        String modifierFormula = getModifierFormula(formulas, playerLevel, mobLevel).replace("{mobLevel}", ((Double) (double) mobLevel).toString())
                .replace("{playerLevel}", ((Double) (double) playerLevel).toString())
                .replace(valueKey, ((Double) value).toString());

        if(placeholderApiFound){
            modifierFormula = PlaceholderAPI.setPlaceholders(killer, modifierFormula);
        }

        double result = Calculator.eval(Properties.FormulaMethod, modifierFormula);
        return Math.max(0, result);
    }

    public static ModifierFormulas getExperienceModiferFormulas(){
        return experienceModiferFormulas;
    }

    public static ModifierFormulas getMoneyModiferFormulas(){
        return moneyModiferFormulas;
    }

    private static String getModifierFormula(ModifierFormulas formulas, int playerLevel, int mobLevel){
        if(playerLevel < mobLevel)
            return formulas.MobLevelHigher;
        else if(playerLevel == mobLevel)
            return formulas.MobLevelEqual;
        return formulas.MobLevelLower;
    }
}
