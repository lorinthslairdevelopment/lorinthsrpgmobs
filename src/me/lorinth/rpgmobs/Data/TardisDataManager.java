package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.Listener.TardisWeepingAngelsListener;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Disableable;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

public class TardisDataManager extends Disableable implements DataManager {

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        if(!ConfigHelper.ConfigContainsPath(config, "TardisWeepingAngels.Enabled")){
            RpgMobsOutputHandler.PrintInfo("TardisWeepingAngels options not found, Generating...");
            setDefaults(config, plugin);
        }

        if(Bukkit.getServer().getPluginManager().getPlugin("TARDISWeepingAngels") == null)
            this.setDisabled(true);
        else{
            this.setDisabled(!config.getBoolean("TardisWeepingAngels.Enabled"));

            if(!this.isDisabled()){
                Bukkit.getPluginManager().registerEvents(new TardisWeepingAngelsListener(), plugin);
                RpgMobsOutputHandler.PrintInfo("Updating Tardis Monsters");
            }
        }
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    private void setDefaults(FileConfiguration config, Plugin plugin){
        config.set("TardisWeepingAngels.Enabled", false);

        plugin.saveConfig();
    }
}
