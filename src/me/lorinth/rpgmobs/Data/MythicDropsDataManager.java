package me.lorinth.rpgmobs.Data;

import com.tealcube.minecraft.bukkit.mythicdrops.api.MythicDrops;
import com.tealcube.minecraft.bukkit.mythicdrops.items.builders.MythicDropBuilder;
import me.lorinth.rpgmobs.Listener.MythicDropsListener;
import me.lorinth.rpgmobs.Objects.MythicDrops.MythicDropsData;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Objects.CreatureData;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Disableable;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public class MythicDropsDataManager extends Disableable implements DataManager {

    private MythicDropsListener mythicDropsListener;
    private MythicDrops mythicDrops;
    private MythicDropBuilder mythicDropBuilder;
    private DataLoader dataLoader;

    public MythicDropsDataManager(DataLoader dataloader){
        this.dataLoader = dataloader;
    }

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        if(!ConfigHelper.ConfigContainsPath(config, "MythicDrops.Enabled")){
            RpgMobsOutputHandler.PrintInfo("MythicDrops options not found, Generating...");
            setDefaults(config, plugin);
        }

        mythicDrops = (MythicDrops) Bukkit.getServer().getPluginManager().getPlugin("MythicDrops");
        if(mythicDrops == null)
            this.setDisabled(true);
        else
            this.setDisabled(!config.getBoolean("MythicDrops.Enabled"));


        if(this.isDisabled())
            RpgMobsOutputHandler.PrintInfo("MythicDrops Integration is Disabled");
        else {
            RpgMobsOutputHandler.PrintInfo("MythicDrops Integration is Enabled!");
            mythicDropsListener = new MythicDropsListener(this);
            mythicDropBuilder = new MythicDropBuilder(mythicDrops);
            Bukkit.getPluginManager().registerEvents(mythicDropsListener, plugin);
        }
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    private void setDefaults(FileConfiguration config, Plugin plugin){
        config.set("MythicDrops.Enabled", false);
        plugin.saveConfig();
    }

    public ItemStack getDrop(Entity entity, int level){
        CreatureData data = dataLoader.getCreatureDataManager().getData(entity.getType().name());
        MythicDropsData mythicDropsData = data.getMythicDropsData();
        String tierName = mythicDropsData.getNextTierName(level);

        if(tierName != null){
            ItemStack item = mythicDropBuilder.withTier(tierName).build();
            return item;
        }

        return null;
    }
}
