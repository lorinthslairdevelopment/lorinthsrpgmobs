package me.lorinth.rpgmobs.Data;

import io.lumine.xikage.mythicmobs.MythicMobs;
import me.lorinth.rpgmobs.Listener.MythicMobsListener;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Disableable;
import me.lorinth.utils.objects.Version;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;

public class MythicMobsDataManager extends Disableable implements DataManager {

    private ArrayList<String> ignoredWorlds = new ArrayList<>();
    private MythicMobsListener mythicMobsListener;

    public void loadData(FileConfiguration config, Plugin plugin){
        if(!ConfigHelper.ConfigContainsPath(config, "MythicMobs.Enabled")){
            RpgMobsOutputHandler.PrintInfo("MythicMobs options not found, Generating...");
            setDefaults(config, plugin);
        }

        if(Bukkit.getServer().getPluginManager().getPlugin("MythicMobs") == null)
            this.setDisabled(true);
        else
            this.setDisabled(!config.getBoolean("MythicMobs.Enabled"));


        if(this.isDisabled())
            RpgMobsOutputHandler.PrintInfo("MythicMobs Integration is Disabled");
        else {
            RpgMobsOutputHandler.PrintInfo("MythicMobs Integration is Enabled!");
            loadIgnoredWorlds(config);

            mythicMobsListener = new MythicMobsListener(this, new Version(MythicMobs.inst().getVersion()));
            Bukkit.getPluginManager().registerEvents(mythicMobsListener, plugin);
        }
    }

    public void reload(){
        FileConfiguration configuration = LorinthsRpgMobs.instance.getConfig();
        loadData(configuration, LorinthsRpgMobs.instance);
    }

    private void loadIgnoredWorlds(FileConfiguration config){
        if(config.getConfigurationSection("MythicMobs").getKeys(false).contains("IgnoredWorlds"))
            ignoredWorlds.addAll(config.getStringList("MythicMobs.IgnoredWorlds"));
    }

    public boolean isWorldIgnored(World world){
        return ignoredWorlds.contains(world.getName());
    }

    public boolean isMythicMob(Entity entity){
        if(!isDisabled())
            return mythicMobsListener.isMythicMob(entity);
        return false;
    }

    public boolean saveData(FileConfiguration config){
        return false;
    }

    private void setDefaults(FileConfiguration config, Plugin plugin){
        config.set("MythicMobs.Enabled", false);
        config.set("MythicMobs.IgnoredWorlds", new ArrayList<String>(){{ add("world_the_end"); }});
        plugin.saveConfig();
    }
}
