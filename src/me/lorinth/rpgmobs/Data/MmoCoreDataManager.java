package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.Listener.MmoCoreListener;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Disableable;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

public class MmoCoreDataManager extends Disableable implements DataManager {

    private MmoCoreListener listener;

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        if(!ConfigHelper.ConfigContainsPath(config, "MmoCore.Enabled")){
            RpgMobsOutputHandler.PrintInfo("MmoCore options not found, Generating...");
            setDefaults(config, plugin);
        }

        if(Bukkit.getServer().getPluginManager().getPlugin("MMOCore") == null)
            this.setDisabled(true);
        else{
            this.setDisabled(!config.getBoolean("MmoCore.Enabled"));
        }

        if(this.isDisabled())
            RpgMobsOutputHandler.PrintInfo("MMOCore Integration is Disabled");
        else {
            RpgMobsOutputHandler.PrintInfo("MMOCore Integration is Enabled!");
            listener = new MmoCoreListener(config, "MmoCore.");
            Bukkit.getPluginManager().registerEvents(listener, plugin);
        }
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    private void setDefaults(FileConfiguration config, Plugin plugin){
        config.set("MmoCore.Enabled", false);
        config.set("MmoCore.DistanceAlgorithm", "Diamond");
        config.set("MmoCore.ValidDistance", 50);
        config.set("MmoCore.PartyExperienceFormulas.2", "1.5 * {exp} / 2");
        config.set("MmoCore.PartyExperienceFormulas.3", "1.8 * {exp} / 3");
        config.set("MmoCore.PartyExperienceFormulas.4", "2.0 * {exp} / 4");
        config.set("MmoCore.PartyExperienceFormulas.5", "2.2 * {exp} / 5");
        config.set("MmoCore.PartyExperienceFormulas.6", "2.4 * {exp} / 6");

        plugin.saveConfig();
    }

}
