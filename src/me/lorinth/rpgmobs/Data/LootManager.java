package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Loot.CreatureLootData;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Variants.MobVariant;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.ItemStackHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import java.util.*;

public class LootManager implements DataManager {

    private static boolean enabled = true;
    private static boolean replaceVanillaDrops = false;
    private static TreeMap<String, ItemStack> items = new TreeMap<>();
    private static HashMap<EntityType, CreatureLootData> lootData = new HashMap<>();
    private static List<ItemStack> emptyItemList = new ArrayList<>();
    private static List<String> emptyCommandList = new ArrayList<>();
    private static int pageSize = 12;

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        enabled = config.getBoolean("Enabled");
        replaceVanillaDrops = config.getBoolean("ReplaceVanillaDrops");
        if(enabled){
            try{
                loadItemData(config);
                loadLootData(config);
                RpgMobsOutputHandler.PrintInfo("Loot System Enabled! {ReplaceVanillaDrops: " + replaceVanillaDrops + "}");
            }
            catch (Exception e){
                RpgMobsOutputHandler.PrintException("Error loading loot.yml", e);
                enabled = false;
                RpgMobsOutputHandler.PrintError("Loot System Disabled");
            }
        }
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    private void loadItemData(FileConfiguration config){
        if(!ConfigHelper.ConfigContainsPath(config, "Items")){
            return;
        }
        for(String itemId : config.getConfigurationSection("Items").getKeys(false)){
            ItemStack item = ItemStackHelper.makeItemFromConfig(config, "Items", itemId, "loot.yml", RpgMobsOutputHandler.getInstance());
            items.put(itemId, item);
        }
        RpgMobsOutputHandler.PrintInfo("Loaded " + items.keySet().size() + " custom items");
    }

    private void loadLootData(FileConfiguration config){
        if(!ConfigHelper.ConfigContainsPath(config, "Drops")){
            return;
        }
        for(String entityTypeName : config.getConfigurationSection("Drops").getKeys(false)){
            if(!TryParse.parseEnum(EntityType.class, entityTypeName)){
                RpgMobsOutputHandler.PrintError("Invalid Entity Type, " + RpgMobsOutputHandler.HIGHLIGHT + entityTypeName + RpgMobsOutputHandler.ERROR + ", in loot.yml");
                continue;
            }

            EntityType entityType = EntityType.valueOf(entityTypeName);
            lootData.put(entityType, new CreatureLootData(config, "Drops." + entityTypeName));
            RpgMobsOutputHandler.PrintInfo("Loaded loot tables for " + entityType);
        }
    }

    public static boolean shouldReplaceDrops(){
        return enabled && replaceVanillaDrops;
    }

    public static boolean hasLootDefined(EntityType type){
        return lootData.get(type) != null;
    }

    public static ItemStack getItemById(String id){
        ItemStack item = items.get(id);
        if(item != null)
            return item.clone();

        return null;
    }

    public static List<ItemStack> getDropsForCreature(Entity entity){
        if(!enabled)
            return emptyItemList;

        List<ItemStack> loot = new ArrayList<>();
        Integer level = LorinthsRpgMobs.GetLevelOfEntity(entity);

        //Mob Variant Loot
        MobVariant mobVariant = LorinthsRpgMobs.GetMobVariantOfEntity(entity);
        if(mobVariant != null){
            List<ItemStack> mobVariantLoot = mobVariant.getLoot(level);
            if(mobVariantLoot != null){
                loot.addAll(mobVariantLoot);
            }
        }

        //Entity Type Loot
        CreatureLootData creatureLootData = lootData.get(entity.getType());
        if(creatureLootData != null){
            loot.addAll(creatureLootData.getLootByLevel(level));
        }

        return loot;

    }

    public static List<String> getCommandRewardsForCreature(Entity entity){
        if(!enabled)
            return emptyCommandList;

        CreatureLootData creatureLootData = lootData.get(entity.getType());
        if(creatureLootData == null)
            return emptyCommandList;

        return creatureLootData.getCommandRewardsByLevel(LorinthsRpgMobs.GetLevelOfEntity(entity));
    }

    public static Integer getVanillaMultiplierForCreature(Entity entity){
        if(!enabled)
            return 1;

        CreatureLootData creatureLootData = lootData.get(entity.getType());
        if(creatureLootData == null)
            return 1;

        return creatureLootData.getVanillaMultiplier(LorinthsRpgMobs.GetLevelOfEntity(entity));
    }

    public static List<Map.Entry<String, ItemStack>> getLootItemPage(int page){
        ArrayList<Map.Entry<String, ItemStack>> pageItems = new ArrayList<>();
        Set<Map.Entry<String, ItemStack>> entries = items.entrySet();
        int maxSize = entries.size();

        List<Map.Entry<String, ItemStack>> entryList = new ArrayList<>(entries);

        if(page * pageSize > maxSize){
            page = maxSize / pageSize;
        }

        int start = page * pageSize;
        for(int i = start; i < Math.min(maxSize, start + pageSize); i++){
            pageItems.add(entryList.get(i));
        }

        return pageItems;
    }

    public static List<Map.Entry<String, ItemStack>> getSearchItemPage(String searchValue){
        final String searchValueInsensitive = searchValue.toLowerCase();
        final ArrayList<Map.Entry<String, ItemStack>> pageItems = new ArrayList<>();
        Set<Map.Entry<String, ItemStack>> entries = items.entrySet();

        entries.forEach((Map.Entry<String, ItemStack> entry) -> {
            if(entry.getKey().toLowerCase().contains(searchValueInsensitive) ||
                    ChatColor.stripColor(getItemName(entry.getValue())).toLowerCase().contains(searchValueInsensitive)){
                pageItems.add(entry);
            }
        });

        return pageItems.subList(0, Math.min(12, pageItems.size()));
    }

    private static String getItemName(ItemStack itemStack){
        if(itemStack.hasItemMeta()){
            ItemMeta meta = itemStack.getItemMeta();
            return meta.getDisplayName();
        }

        return itemStack.getType().toString();
    }

}
