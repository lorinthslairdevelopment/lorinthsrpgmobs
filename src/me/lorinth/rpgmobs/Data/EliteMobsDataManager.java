package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.Listener.EliteMobsListener;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Disableable;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;

public class EliteMobsDataManager extends Disableable implements DataManager {

    private EliteMobsListener listener;

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        if(!ConfigHelper.ConfigContainsPath(config, "EliteMobs.OverrideEnabled")){
            RpgMobsOutputHandler.PrintInfo("EliteMobs options not found, Generating...");
            setDefaults(config, plugin);
        }
        HashMap<String, String> worldLevelFormulas = new HashMap<>();
        if(Bukkit.getServer().getPluginManager().getPlugin("EliteMobs") == null)
            this.setDisabled(true);
        else{
            this.setEnabled(config.getBoolean("EliteMobs.OverrideEnabled"));
            worldLevelFormulas = new HashMap<>();
            for(String key : config.getConfigurationSection("EliteMobs.Worlds").getKeys(false)){
                worldLevelFormulas.put(key, config.getString("EliteMobs.Worlds." + key));
            }
        }

        if(this.isDisabled())
            RpgMobsOutputHandler.PrintInfo("EliteMobs Level Override is Disabled");
        else {
            RpgMobsOutputHandler.PrintInfo("EliteMobs Level Override is Enabled!");
            listener = new EliteMobsListener(worldLevelFormulas);
            Bukkit.getPluginManager().registerEvents(listener, plugin);
        }
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    private void setDefaults(FileConfiguration config, Plugin plugin){
        config.set("EliteMobs.OverrideEnabled", false);
        config.set("EliteMobs.Worlds.world", "{level} + ({level} / 4) + rand({level} /4)");
        config.set("EliteMobs.Worlds.world_nether", "{level} + ({level} / 4) + rand({level} /4)");
        config.set("EliteMobs.Worlds.world_the_end", "{level} + ({level} / 4) + rand({level} /4)");

        plugin.saveConfig();
    }
}
