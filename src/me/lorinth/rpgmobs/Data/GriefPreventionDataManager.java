package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.handlers.griefProtection.GriefPreventionClaimHandler;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

public class GriefPreventionDataManager implements DataManager {

    private boolean enabled = false;
    private GriefPreventionClaimHandler claimHandler;

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        enabled = config.getBoolean("GriefPrevention.Enabled");

        if(enabled){
            enabled = Bukkit.getPluginManager().getPlugin("GriefPrevention") != null;
            if(!enabled)
                return;
            claimHandler = new GriefPreventionClaimHandler(config.getBoolean("GriefPrevention.DisableSpawnsInAllClaims"));
        }
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        if(!ConfigHelper.ConfigContainsPath(config, "GriefPrevention")){
            config.set("GriefPrevention.Enabled", false);
            config.set("GriefPrevention.DisableSpawnsInAllClaims", true);
            return true;
        }
        return false;
    }

    public boolean canSpawn(Location location, boolean isHostile){
        if(enabled && claimHandler != null){
            return claimHandler.canSpawn(location, isHostile);
        }

        return true;
    }

}
