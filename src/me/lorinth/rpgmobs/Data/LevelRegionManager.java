package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.Objects.LevelRegion;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.handlers.region.GriefPreventionHandler;
import me.lorinth.rpgmobs.handlers.region.IRegionHandler;
import me.lorinth.rpgmobs.handlers.region.RedProtectRegionHandler;
import me.lorinth.rpgmobs.handlers.region.WorldGuardRegionHandler;
import me.lorinth.rpgmobs.Objects.DataManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Manager object of all LevelRegions in memory. Use this class to read/manipulate level regions on the server as well as add/remove them
 */
public class LevelRegionManager implements DataManager {

    private IRegionHandler regionHandler;
    private HashMap<String, HashMap<String, LevelRegion>> allLevelRegions = new HashMap<>();

    public LevelRegionManager(){
        checkForRegionHandlers();
    }

    private void checkForRegionHandlers(){
        checkForWorldGuard();
        checkForRedProtect();
        checkForGriefPrevention();
    }

    private void checkForWorldGuard(){
        if (regionHandler != null){
            return;
        }

        try{
            Plugin worldGuardPlugin = Bukkit.getPluginManager().getPlugin("WorldGuard");
            if (worldGuardPlugin != null && worldGuardPlugin.isEnabled()){
                regionHandler = new WorldGuardRegionHandler(worldGuardPlugin);
                regionHandler.init(true);
                RpgMobsOutputHandler.PrintInfo("Detected WorldGuard for level regions!");
            }
        }
        catch(Exception e){
            RpgMobsOutputHandler.PrintException("Failed to find WorldGuard", e);
            regionHandler = null;
        }
    }

    private void checkForRedProtect(){
        if (regionHandler != null){
            return;
        }

        try{
            Plugin redProtectPlugin = Bukkit.getPluginManager().getPlugin("RedProtect");
            if (redProtectPlugin != null && redProtectPlugin.isEnabled()){
                regionHandler = new RedProtectRegionHandler();
                regionHandler.init(true);
                RpgMobsOutputHandler.PrintInfo("Detected RedProtect for level regions!");
            }
        }
        catch(Exception e){
            RpgMobsOutputHandler.PrintException("Failed to find RedProtect", e);
            regionHandler = null;
        }
    }

    private void checkForGriefPrevention(){
        if (regionHandler != null){
            return;
        }

        try{
            Plugin griefPreventionPlugin = Bukkit.getPluginManager().getPlugin("GriefPrevention");
            if(griefPreventionPlugin != null && griefPreventionPlugin.isEnabled()){
                regionHandler = new GriefPreventionHandler();
                regionHandler.init(true);
                RpgMobsOutputHandler.PrintInfo("Detected GriefPrevention for level regions!");
            }
        }
        catch(Exception e){
            RpgMobsOutputHandler.PrintException("Failed to find GriefPrevention", e);
            regionHandler = null;
        }
    }

    public boolean saveData(FileConfiguration config){
        boolean changed = false;
        for(String worldName : allLevelRegions.keySet()){
            for(LevelRegion region : allLevelRegions.get(worldName).values()){
                changed = region.save(config, "LevelRegions." + worldName + ".");
            }
        }
        return changed;
    }

    public void loadData(FileConfiguration config, Plugin plugin){
        if(config.contains("LevelRegions")){
            for(String world : config.getConfigurationSection("LevelRegions").getKeys(false)){
                allLevelRegions.put(world, new HashMap<>());

                for(String key : config.getConfigurationSection("LevelRegions." + world).getKeys(false)){
                    LevelRegion region = loadRegion(config, key, "LevelRegions." + world);
                    allLevelRegions.get(world).put(key, region);
                    RpgMobsOutputHandler.PrintInfo("Loaded Region, " + region.getName() + " " + (region.isDisabled() ? "(disabled)" : "(Level:" + region.getLevelRange() + ")"));
                }
                RpgMobsOutputHandler.PrintInfo("Loaded World, " + world);
            }
        }
    }

    private LevelRegion loadRegion(FileConfiguration config, String name, String prefix){
        return new LevelRegion(config, name, prefix);
    }

    public void addLevelRegionToWorld(World world, LevelRegion region){
        if(!allLevelRegions.containsKey(world.getName())){
            allLevelRegions.put(world.getName(), new HashMap<>());
        }

        allLevelRegions.get(world.getName()).put(region.getName(), region);
    }

    /**
     * Gets all level region in a given world, with a given name
     * @param world - world to check
     * @return all regions in world, or an empty array
     */
    public ArrayList<LevelRegion> getAllLeveledRegionsInWorld(World world){
        if(regionHandler == null){
            return new ArrayList<>();
        }

        ArrayList<LevelRegion> regionsInWorld = new ArrayList<>();
        if(allLevelRegions.containsKey(world.getName()))
            regionsInWorld.addAll(allLevelRegions.get(world.getName()).values());

        return regionsInWorld;
    }

    /**
     * Gets a level region in a given world, with a given name
     * @param world - world to check
     * @param name - name of region
     * @return LevelRegion
     */
    public LevelRegion getLevelRegionByName(World world, String name){
        if(regionHandler == null){
            return null;
        }

        name = name.toLowerCase();

        if(allLevelRegions.containsKey(world.getName()))
            if(allLevelRegions.get(world.getName()).containsKey(name))
                return allLevelRegions.get(world.getName()).get(name);

        return null;
    }

    /**
     * Gets the highest priority leveled region at a location
     * @param location the location to check
     * @return LevelRegion
     */
    public LevelRegion getHighestPriorityLeveledRegionAtLocation(Location location){
        if(regionHandler == null){
            return null;
        }

        HashMap<String, LevelRegion> regions = allLevelRegions.get(location.getWorld().getName());
        if(regions == null){
            return null;
        }
        return regionHandler.getHighestPriorityLevelRegion(regions, location);
    }

}
