package me.lorinth.rpgmobs.Data;

import com.gamingmesh.jobs.Jobs;
import com.gamingmesh.jobs.container.CurrencyType;
import com.gamingmesh.jobs.container.JobsPlayer;
import me.lorinth.rpgmobs.Events.RpgMobDeathEvent;
import me.lorinth.rpgmobs.Listener.JobsRebornListener;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Disableable;
import me.lorinth.rpgmobs.Objects.Properties;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.Calculator;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;

public class JobsRebornDataManager extends Disableable implements DataManager {

    private boolean hookExp;
    private boolean hookMoney;
    private boolean hookPoints;

    private String defaultFormula = "3 + {level}/5 + rand(3)";
    private HashMap<EntityType, String> entityPointsFormula;

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        String path = "JobsReborn.";
        if(!ConfigHelper.ConfigContainsPath(config, path + "Enabled")){
            RpgMobsOutputHandler.PrintInfo("JobsReborn options not found, Generating...");
            setDefaults(config, plugin);
        }

        boolean enabled = config.getBoolean(path + "Enabled");
        boolean foundJobsPlugin = Bukkit.getPluginManager().getPlugin("Jobs") != null;
        this.setDisabled(!enabled || !foundJobsPlugin);

        if(enabled && !foundJobsPlugin){
            RpgMobsOutputHandler.PrintError("JobsReborn integration was set to enabled, but plugin was not found.");
        }

        if(this.isDisabled()){
            return;
        }

        path += "Options.";

        hookExp = config.getBoolean(path + "HookExp");
        hookMoney = config.getBoolean(path + "HookMoney");
        hookPoints = config.getBoolean(path + "HookPoints");
        loadPointsFormulas(config, path + "Points");
        Bukkit.getPluginManager().registerEvents(new JobsRebornListener(this), plugin);
        RpgMobsOutputHandler.PrintInfo("JobsReborn integration was Enabled!");
    }

    private void loadPointsFormulas(FileConfiguration config, String path){
        entityPointsFormula = new HashMap<>();

        defaultFormula = config.getString(path + ".DEFAULT");
        for(String entityTypeValue : config.getConfigurationSection(path).getKeys(false)){
            if(entityTypeValue.equalsIgnoreCase("DEFAULT")){
                continue;
            }

            if(!TryParse.parseEnum(EntityType.class, entityTypeValue)){
                RpgMobsOutputHandler.PrintError("Invalid entity type specified, "
                    + RpgMobsOutputHandler.HIGHLIGHT + entityTypeValue
                    + RpgMobsOutputHandler.ERROR + ", at " + path);
                continue;
            }

            entityPointsFormula.put(EntityType.valueOf(entityTypeValue), config.getString(path + "." + entityTypeValue));
        }
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    public void handleRpgDeathEvent(RpgMobDeathEvent rpgMobDeathEvent){
        if(this.isDisabled()){
            return;
        }

        Entity entity = rpgMobDeathEvent.getEntity();
        Player player = rpgMobDeathEvent.getKiller();
        if(entity == null || player == null){
            return;
        }

        JobsPlayer jobsPlayer = Jobs.getPlayerManager().getJobsPlayer(player);
        HashMap<CurrencyType, Double> payment = new HashMap<>();
        if(hookExp){
            payment.put(CurrencyType.EXP, (double) rpgMobDeathEvent.getExp());
            rpgMobDeathEvent.setExp(0);
        }
        if(hookMoney){
            double currency = rpgMobDeathEvent.getCurrencyValue();
            payment.put(CurrencyType.MONEY, rpgMobDeathEvent.getCurrencyValue());
            rpgMobDeathEvent.setCurrencyValue(0);
        }
        if(hookPoints){
            double points = getPointsFromEntity(entity.getType(), rpgMobDeathEvent.getLevel());
            payment.put(CurrencyType.POINTS, points);
        }

        Jobs.getEconomy().pay(jobsPlayer, payment);
    }

    private Double getPointsFromEntity(EntityType entityType, Integer level){
        String entityFormula = entityPointsFormula.getOrDefault(entityType, defaultFormula);
        return Calculator.eval(Properties.FormulaMethod, entityFormula.replace("{level}", level.toString()));
    }

    private void setDefaults(FileConfiguration config, Plugin plugin){
        config.set("JobsReborn.Enabled", false);
        config.set("JobsReborn.Options.HookExp", true);
        config.set("JobsReborn.Options.HookMoney", true);
        config.set("JobsReborn.Options.HookPoints", true);
        config.set("JobsReborn.Options.Points.DEFAULT", "3 + {level}/5 + rand(3)");
        config.set("JobsReborn.Options.Points.CREEPER", "5 + {level}/5 + rand(5)");

        plugin.saveConfig();
    }
}
