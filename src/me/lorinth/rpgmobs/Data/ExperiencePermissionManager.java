package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.ExperiencePermission;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;


public class ExperiencePermissionManager implements DataManager {

    private ArrayList<ExperiencePermission> experiencePermissions;
    private float defaultValue = 1.0f;

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        experiencePermissions = new ArrayList<>();
        if(ConfigHelper.ConfigContainsPath(config, "ExperiencePermissions")){
            for(String key : config.getConfigurationSection("ExperiencePermissions").getKeys(false)){
                if(key.equalsIgnoreCase("default"))
                    defaultValue = (float) config.getDouble("ExperiencePermissions." + key);
                else{
                    ExperiencePermission perm = new ExperiencePermission(key, (float) config.getDouble("ExperiencePermissions." + key));
                    experiencePermissions.add(perm);
                    RpgMobsOutputHandler.PrintInfo("Loaded permission, " + RpgMobsOutputHandler.HIGHLIGHT + perm.getPermission() + RpgMobsOutputHandler.INFO + " with exp multiplier " + RpgMobsOutputHandler.HIGHLIGHT + perm.getMultiplier());
                }
            }
        }
        else{
            //define and save
            setDefaults(config, plugin);
            loadData(plugin.getConfig(), plugin);
        }
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    public float getExperienceMultiplier(Player player){
        float multi = defaultValue;
        for(ExperiencePermission perm : experiencePermissions){
            if(perm.hasPermission(player)){
                if(perm.getMultiplier() > multi)
                    multi = perm.getMultiplier();
            }
        }
        return multi;
    }

    private void setDefaults(FileConfiguration config, Plugin plugin){
        config.set("ExperiencePermissions.default", 1.0);
        config.set("ExperiencePermissions.vip", 1.5);

        plugin.saveConfig();
    }

}
