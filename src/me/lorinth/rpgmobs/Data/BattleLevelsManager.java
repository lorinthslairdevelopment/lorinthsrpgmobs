package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Disableable;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.plugin.Plugin;

public class BattleLevelsManager extends Disableable implements DataManager {

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        if(!ConfigHelper.ConfigContainsPath(config, "BattleLevels.Enabled")){
            RpgMobsOutputHandler.PrintInfo("BattleLevels options not found, Generating...");
            setDefaults(config, plugin);
        }

        if(Bukkit.getServer().getPluginManager().getPlugin("BattleLevels") == null)
            this.setDisabled(true);
        else{
            this.setDisabled(!config.getBoolean("BattleLevels.Enabled"));
        }
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    public boolean handleEntityDeathEvent(EntityDeathEvent deathEvent, Player player, int exp, double currency){
        if(this.isDisabled()){
            return false;
        }

        //BattlePlayer battlePlayer = BattleLevelsAPI.findPlayer(player.getUniqueId());
        return true;
    }

    private void setDefaults(FileConfiguration config, Plugin plugin){
        config.set("BattleLevels.Enabled", false);

        plugin.saveConfig();
    }

}
