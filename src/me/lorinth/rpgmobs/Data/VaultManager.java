package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Util.NameHelper;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.ConfigHelper;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;

import static org.bukkit.Bukkit.getServer;

public class VaultManager implements DataManager {

    private static boolean enabled = false;
    public static Economy economy;

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        if(!ConfigHelper.ConfigContainsPath(config, "Vault.Enabled")){
            RpgMobsOutputHandler.PrintInfo("Vault options not found, Generating...");
            setDefaults(config, plugin);
        }

        boolean configEnabled = config.getBoolean("Vault.Enabled");
        enabled = configEnabled && setupEconomy();

        if(enabled){
            RpgMobsOutputHandler.PrintInfo("Vault Integration is Enabled!");
        }
        else if(configEnabled && !enabled){
            RpgMobsOutputHandler.PrintError("Vault Integration was enabled, but unable to find VAULT PLUGIN");
        }
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    private void setDefaults(FileConfiguration config, Plugin plugin){
        config.set("Vault.Enabled", false);
        plugin.saveConfig();
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            RpgMobsOutputHandler.PrintInfo("No Vault Plugin");
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (rsp == null) {
            RpgMobsOutputHandler.PrintInfo("No RegisteredServiceProvider");
            return false;
        }
        economy = rsp.getProvider();
        return true;
    }

    public static boolean isEnabled(){
        return enabled;
    }

    public static void addMoney(Player player, Entity entity, Integer money){
        if(isEnabled()) {
            economy.depositPlayer(player, money);
            if(MessageManager.MoneyDrop != null && MessageManager.MoneyDropMessage_Enabled){
                player.sendMessage(
                        MessageManager.MoneyDrop
                                .replace("{entity}", getName(entity))
                                .replace("{money}", money.toString())
                );
            }
        }
    }

    private static String getName(Entity entity){
        HealthBarManager healthBarManager = LorinthsRpgMobs.GetHealthBarManager();
        if(healthBarManager.isDisabled()){
            return entity.getCustomName() != null ? entity.getCustomName() : entity.getName();
        }

        String storedName = NameHelper.getName(entity);
        if(storedName != null){
            return storedName;
        }

        return NameHelper.ToCamelCase(entity.getType().name());
    }

}
