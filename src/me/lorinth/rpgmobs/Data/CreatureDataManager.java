package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.CreatureData;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Animals;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Manager of all CreatureData objects in memory. Manipulate most aspects of monster data here
 */
public class CreatureDataManager implements DataManager {

    private static ArrayList<String> disabledGroups;
    private HashMap<String, CreatureData> customData = new HashMap<>();
    private HashMap<String, CreatureData> entityData = new HashMap<>();

    public CreatureData getData(String type){
        if (customData.containsKey(type)){
            return customData.get(type);
        }

        return entityData.get(type);
    }

    public boolean isDisabled(LivingEntity entity){
        boolean groupDisabled = true;
        if (entity instanceof Animals) {
            groupDisabled = disabledGroups.contains("Animal");
        }
        else if (entity instanceof Monster) {
            groupDisabled = disabledGroups.contains("Monster");
        }
        else {
            groupDisabled = disabledGroups.contains("Misc");
        }

        if (groupDisabled){
            return true;
        }

        CreatureData data = getData(entity);
        if (data.isDisabled(entity.getWorld().getName())){
            return true;
        }

        return false;
    }

    public CreatureData getData(LivingEntity entity){
        String type = entity.getType().name();
        CreatureData data = getData(type);
        if(data != null){
            return data;
        }
        else{
            CreatureData newData = new CreatureData(entity);
            entityData.put(type, newData);
            return newData;
        }
    }

    public CreatureData getCustomData(LivingEntity entity, String type){
        CreatureData data = getData(type);
        if(data != null){
            return data;
        }
        else{
            CreatureData newData = new CreatureData(entity, type);
            customData.put(entity.getType().toString(), newData);
            return newData;
        }
    }

    public boolean saveData(FileConfiguration config){
        for(Map.Entry<String, CreatureData> data : entityData.entrySet()){
            if (data.getValue().isDirty()){
                File file = new File(LorinthsRpgMobs.instance.getDataFolder(), "mobs/" + data.getKey().toLowerCase() + ".yml");
                YamlConfiguration mobConfig = YamlConfiguration.loadConfiguration(file);
                data.getValue().save(mobConfig);

                try{
                    mobConfig.save(file);
                } catch(Exception ex){
                    RpgMobsOutputHandler.PrintException("Failed to save " + data.getKey().toLowerCase() + ".yml", ex);
                }
            }
        }
        for(Map.Entry<String, CreatureData> data : customData.entrySet()){
            if (data.getValue().isDirty()) {
                File file = new File(LorinthsRpgMobs.instance.getDataFolder(), "mobs/" + data.getKey().toLowerCase() + ".yml");
                YamlConfiguration mobConfig = YamlConfiguration.loadConfiguration(file);
                data.getValue().save(mobConfig);

                try{
                    mobConfig.save(file);
                } catch(Exception ex){
                    RpgMobsOutputHandler.PrintException("Failed to save " + data.getKey().toLowerCase() + ".yml", ex);
                }
            }
        }

        return false;
    }

    public void loadData(FileConfiguration config, Plugin plugin){
        disabledGroups = new ArrayList<>();
        ArrayList<String> types = new ArrayList<String>(){{
            add("Animal");
            add("Monster");
            add("Misc");
            add("Custom");
        }};

        // Check for mobs folder
        File mobsDirectory = new File(LorinthsRpgMobs.instance.getDataFolder(), "mobs");
        if (!mobsDirectory.exists() && !mobsDirectory.mkdirs()){
            RpgMobsOutputHandler.PrintError("Your server isn't cool enough for this operation.");
            return;
        }

        boolean removedMob = false;
        for (String type : types){
            ConfigurationSection configurationSection = config.getConfigurationSection("Entity." + type);
            if (configurationSection == null){
                continue;
            }

            for (String key : configurationSection.getKeys(false)){
                if (key.equalsIgnoreCase("disabled")) {
                    if (config.getBoolean("Entity." + type + ".Disabled")){
                        disabledGroups.add(type);
                    }
                    continue;
                }

                // Rip out to own config.
                File file = new File(LorinthsRpgMobs.instance.getDataFolder(), "mobs/" + key.toLowerCase() + ".yml");
                if (file.exists()) {
                    continue;
                }

                YamlConfiguration mobConfig = YamlConfiguration.loadConfiguration(file);
                for (String propertyPath: config.getConfigurationSection("Entity." + type + "." + key).getKeys(true)){
                    mobConfig.set(propertyPath.replace(Pattern.quote("Entity." + type + "."), ""),
                            config.get("Entity." + type + "." + key + "." + propertyPath));
                }

                try{
                    mobConfig.save(file);
                    config.set("Entity." + type + "." + key, null);
                    removedMob = true;
                }
                catch (Exception ex){
                    RpgMobsOutputHandler.PrintError("Failed to transfer mob from config.yml to separate file");
                }
            }
        }

        if (removedMob){
            try {
                config.save(new File(plugin.getDataFolder(), "config.yml"));
            }
            catch(Exception ex){
                RpgMobsOutputHandler.PrintException("Error updating config.yml", ex);
            }
        }

        for (File file : mobsDirectory.listFiles()){
            String entityName = file.getName().replace(".yml", "").toUpperCase();
            loadEntity(
                    YamlConfiguration.loadConfiguration(file),
                    entityName,
                    !TryParse.parseEnum(EntityType.class, entityName));
        }
    }

    private void loadEntity(FileConfiguration config, String entityType, boolean isCustom){
        try{
            if (isCustom){
                customData.put(entityType, new CreatureData(entityType, config));
            }
            else {
                entityData.put(entityType, new CreatureData(entityType, config));
            }
        }
        catch(Exception error){
            RpgMobsOutputHandler.PrintError("Failed to load entity : " + RpgMobsOutputHandler.HIGHLIGHT + entityType + ".yml");
            error.printStackTrace();
        }
    }
}
