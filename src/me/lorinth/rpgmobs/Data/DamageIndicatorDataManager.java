package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.Listener.DamageIndicatorListener;
import me.lorinth.rpgmobs.Objects.DamageIndicatorConfig;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Disableable;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;

public class DamageIndicatorDataManager extends Disableable implements DataManager {

    private static HashMap<String, DamageIndicatorConfig> indicatorConfigs;

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        indicatorConfigs = new HashMap<>();

        this.setDisabled(!config.getBoolean("Enabled"));
        if (this.isDisabled()){
            return;
        }

        for (String key : config.getConfigurationSection("").getKeys(false)){
            if (key.equalsIgnoreCase("enabled")){
                continue;
            }

            if (!TryParse.parseEnum(EntityDamageEvent.DamageCause.class, key) &&
                !TryParse.parseEnum(EntityRegainHealthEvent.RegainReason.class, key.replace("HEAL_", ""))){
                RpgMobsOutputHandler.PrintError("Invalid DamageCause or RegainReason, " +
                        RpgMobsOutputHandler.HIGHLIGHT + key);
                continue;
            }

            indicatorConfigs.put(key, new DamageIndicatorConfig(config, key));
        }

        RpgMobsOutputHandler.PrintInfo("Loaded, " + indicatorConfigs.size() + " indicator events!");

        Bukkit.getPluginManager().registerEvents(new DamageIndicatorListener(), plugin);
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    public static DamageIndicatorConfig getDamageIndicatorConfig(String key){
        return indicatorConfigs.getOrDefault(key, new DamageIndicatorConfig());
    }
}
