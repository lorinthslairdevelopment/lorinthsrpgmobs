package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.Listener.SkillsProListener;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Disableable;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

public class SkillsProDataManager extends Disableable implements DataManager {

    private DataLoader dataLoader;
    private SkillsProListener listener;

    public SkillsProDataManager(DataLoader dataLoader){
        this.dataLoader = dataLoader;
    }

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        if(!ConfigHelper.ConfigContainsPath(config, "SkillsPro.Enabled")){
            RpgMobsOutputHandler.PrintInfo("SkillsPro options not found, Generating...");
            setDefaults(config, plugin);
        }

        if(Bukkit.getServer().getPluginManager().getPlugin("SkillsPro") == null)
            this.setDisabled(true);
        else{
            this.setDisabled(!config.getBoolean("SkillsPro.Enabled"));
        }

        if(this.isDisabled())
            RpgMobsOutputHandler.PrintInfo("SkillsPro Integration is Disabled");
        else {
            RpgMobsOutputHandler.PrintInfo("SkillsPro Integration is Enabled!");
            listener = new SkillsProListener(dataLoader);
            Bukkit.getPluginManager().registerEvents(listener, plugin);
        }
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    private void setDefaults(FileConfiguration config, Plugin plugin){
        config.set("SkillsPro.Enabled", false);
        plugin.saveConfig();
    }

}
