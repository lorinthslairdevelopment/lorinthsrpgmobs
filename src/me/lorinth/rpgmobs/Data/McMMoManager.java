package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.Listener.McMMoEventListener;
import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Disableable;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.plugin.Plugin;

public class McMMoManager extends Disableable implements DataManager {

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        if(!ConfigHelper.ConfigContainsPath(config, "McMMo.Enabled")){
            RpgMobsOutputHandler.PrintInfo("McMMo options not found, Generating...");
            setDefaults(config, plugin);
        }

        if(Bukkit.getServer().getPluginManager().getPlugin("mcMMO") == null)
            this.setDisabled(true);
        else{
            this.setDisabled(!config.getBoolean("McMMo.Enabled"));

            if(!this.isDisabled()){
                Bukkit.getPluginManager().registerEvents(new McMMoEventListener(), plugin);
                RpgMobsOutputHandler.PrintInfo("Giving monster experience to " + RpgMobsOutputHandler.HIGHLIGHT + " McMMO combat skills");
            }
        }
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    private void setDefaults(FileConfiguration config, Plugin plugin){
        config.set("McMMo.Enabled", false);

        plugin.saveConfig();
    }

}
