package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.*;
import me.lorinth.rpgmobs.Util.MetaDataConstants;
import me.lorinth.rpgmobs.Util.NameHelper;
import me.lorinth.rpgmobs.Variants.MobVariant;
import me.lorinth.rpgmobs.handlers.bossSpawn.IBossHandler;
import org.bukkit.NamespacedKey;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.entity.*;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.Random;

public class CreatureModifierManager {

    private static DataLoader dataLoader;
    private static Random random;

    public CreatureModifierManager(DataLoader dataLoader){
        CreatureModifierManager.dataLoader = dataLoader;
        random = new Random();
    }

    public static void updateEntity(LivingEntity entity, CreatureSpawnEvent.SpawnReason spawnReason){
        //Set Level
        int level = dataLoader.calculateLevel(entity.getLocation(), entity);
        if(level == -1)
            return;

        updateEntityWithLevel(entity, spawnReason, level);
    }

    public static boolean replaceWithBossLevelRegion(LivingEntity entity){
        LevelRegion levelRegion = dataLoader.getLevelRegionManager().getHighestPriorityLeveledRegionAtLocation(entity.getLocation());
        if(levelRegion == null) {
            return false;
        }

        return levelRegion.replaceWithBoss(entity);
    }

    public static void updateEntityWithLevel(LivingEntity entity, CreatureSpawnEvent.SpawnReason spawnReason, int level){
        if(spawnReason != null && spawnReason == CreatureSpawnEvent.SpawnReason.SPAWNER && dataLoader.ignoresSpawnerMobs()) {
            dataLoader.ignoreEntity(entity);
            return;
        }

        CreatureData data = dataLoader.getCreatureDataManager().getData(entity);
        updateEntityWithData(entity, level, data);
    }

    public static void updateEntityWithData(LivingEntity entity, int level, CreatureData data){
        if (dataLoader.getCreatureDataManager().isDisabled(entity)){
            return;
        }

        if(isEliteMob(entity))
            return;

        if(replaceWithBoss(entity, data, level)){
            entity.remove();
            return;
        }
        setLevel(entity, level);
        setHealth(entity, data, level);
        setDamage(entity, data, level);
        setMovementSpeed(entity, data, level);
        setEquipment(entity, data, level);
        setName(entity, data, level);
        setMount(entity, data, level);
        setVariant(entity);
    }

    private static boolean replaceWithBoss(Entity entity, CreatureData data, int level){
        RandomNullCollection<String> bossNameChances = data.getBossChances(level);
        if(bossNameChances == null){
            return false;
        }

        String bossName = bossNameChances.next();
        if (bossName == null){
            return false;
        }

        IBossHandler bossHandler = dataLoader.getBossHandler();
        if(bossHandler == null) {
            return false;
        }

        return dataLoader.getBossHandler().spawnBoss(entity.getLocation(), bossName);
    }

    private static void setLevel(Entity entity, int level){
        if(Properties.IsPersistentDataVersion){
            PersistentDataContainer container = entity.getPersistentDataContainer();
            container.set(new NamespacedKey(LorinthsRpgMobs.instance, "level"), PersistentDataType.INTEGER, level);
        }
        else{
            entity.setMetadata(MetaDataConstants.Level, new FixedMetadataValue(LorinthsRpgMobs.instance, level));
        }
    }

    private static boolean isEliteMob(Entity entity){
        return entity.hasMetadata("EliteMob") || entity.hasMetadata("PassiveEliteMob");
    }

    private static void setHealth(LivingEntity entity, CreatureData data, int level){
        int health = (int)data.getHealthAtLevel(level);

        if (entity instanceof Slime || entity instanceof MagmaCube){
            int size = ((Slime) entity).getSize();

            health = (int) Math.floor(health * Math.pow(size, 2));
        }

        if(Properties.IsAttributeVersion){
            AttributeInstance attribute = entity.getAttribute(Attribute.GENERIC_MAX_HEALTH);
            if(attribute != null) {
                attribute.setBaseValue(health);
                entity.setHealth(health);
            }
            else {
                entity.setMaxHealth(health);
                entity.setHealth(entity.getMaxHealth());
            }
        }
        else{
            entity.setMaxHealth(health);
            entity.setHealth(entity.getMaxHealth());
        }
    }

    private static void setDamage(LivingEntity entity, CreatureData data, int level){
        double damage = (int)data.getDamageAtLevel(level);
        if(Properties.IsAttributeVersion) {
            AttributeInstance attribute = entity.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE);
            if (attribute != null)
                attribute.addModifier(new AttributeModifier("RpgMobs_Damage", damage - attribute.getValue(), AttributeModifier.Operation.ADD_NUMBER));
        }

        if(Properties.IsPersistentDataVersion){
            PersistentDataContainer dataContainer = entity.getPersistentDataContainer();
            dataContainer.set(new NamespacedKey(LorinthsRpgMobs.instance, "damage"), PersistentDataType.DOUBLE, damage);
        }
        else{
            entity.setMetadata(MetaDataConstants.Damage, new FixedMetadataValue(LorinthsRpgMobs.instance, damage));
        }
    }

    private static void setMovementSpeed(LivingEntity entity, CreatureData data, int level){
        if (Properties.IsAttributeVersion){
            AttributeInstance attribute = entity.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED);
            if (attribute == null) {
                return;
            }

            if (data.getMovementSpeedFormula() == null){
                double currentSpeed = Math.round(attribute.getValue() * 100) / 100.0;
                data.setMovementSpeedFormula(currentSpeed + " + ({level} * 0.001)");
            }

            double speed = data.getMovementSpeedAtLevel(level);
            attribute.setBaseValue(speed);
        }
    }

    private static void setEquipment(LivingEntity entity, CreatureData data, int level){
        EquipmentData equipmentData = data.getEquipmentData();
        if(equipmentData != null) {
            equipmentData.equip(entity, level);
        }
    }

    private static void setName(LivingEntity entity, CreatureData data, int level){
        LevelRegion region = LorinthsRpgMobs.GetLevelRegionManager().getHighestPriorityLeveledRegionAtLocation(entity.getLocation());
        NameData regionNameData = region != null ? region.getEntityName(entity.getType()) : null;
        entity.setCustomNameVisible(Properties.NameTagsAlwaysOn);
        String name = data.getNameAtLevel(regionNameData, level);

        if(name != null)
            entity.setCustomName(name);

        entity.setRemoveWhenFarAway(data.shouldRemoveWhenFarAway());
    }

    private static void setMount(LivingEntity entity, CreatureData data, int level){
        RandomNullCollection<EntityType> mountTypeChances = data.getMountType(level);
        if(mountTypeChances == null){
            return;
        }

        EntityType mountType = mountTypeChances.next();
        if(mountType == null){
            return;
        }

        Entity mount = entity.getWorld().spawnEntity(entity.getLocation(), mountType);
        if(Properties.IsAbstractHorseVersion){
            if(mount instanceof AbstractHorse){
                AbstractHorse horse = (AbstractHorse) mount;
                horse.setDomestication(0);
                horse.setTamed(true);
            }
        }
        if(Properties.IsAddPassengerVersion){
            mount.addPassenger(entity);
        }
        else{
            mount.setPassenger(entity);
        }
    }

    private static void setVariant(LivingEntity entity){
        MobVariant variant = MobVariantDataManager.TryApplyVariant(entity);

        //If Variant hasn't been applied, replace all variant tags in format
        if(variant == null){
            String name = entity.getCustomName();
            if(name != null){
                ArrayList<String> removeTags = new ArrayList<String>(){{
                    add("{variant} ");
                    add("{Variant} ");
                    add("{variant}");
                    add("{Variant}");
                }};
                for(String tag : removeTags) {
                    name = name.replace(tag, "");
                }

                entity.setCustomName(name);
            }
        }
        if(entity.getCustomName() != null){
            NameHelper.setName(entity, entity.getCustomName());
        }

    }

}
