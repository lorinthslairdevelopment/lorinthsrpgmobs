package me.lorinth.rpgmobs.Data;

import me.lorinth.rpgmobs.Objects.DataManager;
import me.lorinth.rpgmobs.Objects.Disableable;
import me.lorinth.rpgmobs.Objects.Properties;
import me.lorinth.rpgmobs.Util.LevelHelper;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.TryParse;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.text.DecimalFormat;
import java.util.regex.Pattern;

public class ExperienceDisplayManager extends Disableable implements DataManager {

    private static String path = "ExperienceDisplay";
    private static ChatMessageType ChatMessageType = net.md_5.bungee.api.ChatMessageType.ACTION_BAR;
    private static String MessageFormat = "&aYou gained &e{exp} &aexp (&e{expPercent}&a)!";
    private static ExperienceDisplayManager instance;
    private static DecimalFormat format = new DecimalFormat("###.0");

    public ExperienceDisplayManager(){
        instance = this;
    }

    public static void ShowExperienceToPlayer(Player player, Integer experience){
        if(instance.isDisabled()){
            return;
        }

        double expPercent = LevelHelper.getExperiencePercent(player, experience);
        String message = MessageFormat
                .replaceAll(Pattern.quote("{exp}"), experience.toString())
                .replaceAll(Pattern.quote("{expPercent}"), format.format(expPercent));
        if(Properties.IsMessageTypeVersion){
            player.spigot().sendMessage(ChatMessageType,
                    TextComponent.fromLegacyText(message));
        }
        else{
            player.spigot().sendMessage(
                    TextComponent.fromLegacyText(message));
        }
    }

    @Override
    public void loadData(FileConfiguration config, Plugin plugin) {
        if(!ConfigHelper.ConfigContainsPath(config, path)){
            setDefaults(config, plugin);
        }

        setDisabled(!config.getBoolean(path + ".Enabled"));
        String rawChatMessageType = config.getString(path + ".ChatMessageType");
        if(TryParse.parseEnum(net.md_5.bungee.api.ChatMessageType.class, rawChatMessageType)){
            ChatMessageType = net.md_5.bungee.api.ChatMessageType.valueOf(rawChatMessageType);
        }
        else{
            ChatMessageType = net.md_5.bungee.api.ChatMessageType.ACTION_BAR;
            RpgMobsOutputHandler.PrintError("Failed to parse " + RpgMobsOutputHandler.HIGHLIGHT + rawChatMessageType +
                    RpgMobsOutputHandler.ERROR + " as a ChatMessageType in config.yml");
        }
        MessageFormat = ChatColor.translateAlternateColorCodes('&', config.getString(path + ".Message"));
    }

    @Override
    public boolean saveData(FileConfiguration config) {
        return false;
    }

    private void setDefaults(FileConfiguration config, Plugin plugin){
        config.set(path + ".Enabled", true);
        config.set(path + ".ChatMessageType", net.md_5.bungee.api.ChatMessageType.ACTION_BAR.toString());
        config.set(path + ".Message", "&aYou gained &e{exp} &aexp!");
        plugin.saveConfig();
    }
}
