package me.lorinth.rpgmobs.handlers.griefProtection;

import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.ryanhamshire.GriefPrevention.Claim;
import me.ryanhamshire.GriefPrevention.GriefPrevention;
import org.bukkit.*;

public class GriefPreventionClaimHandler {

    private boolean canSpawnInAllClaims;
    private GriefPrevention griefPrevention;
    private GriefPreventionFlagsHandler griefPreventionFlagsHandler;

    public GriefPreventionClaimHandler(boolean disableSpawnsInClaims){
        this.canSpawnInAllClaims = !disableSpawnsInClaims;
        griefPrevention = (GriefPrevention) Bukkit.getPluginManager().getPlugin("GriefPrevention");
        RpgMobsOutputHandler.PrintInfo("Hooked into GriefPrevention!");

        if(Bukkit.getPluginManager().getPlugin("GriefPreventionFlags") != null){
            griefPreventionFlagsHandler = new GriefPreventionFlagsHandler();
        }
    }

    public boolean canSpawn(Location location, boolean isHostile){
        Claim claim = griefPrevention.dataStore.getClaimAt(location, false, null);
        if(claim == null){
            return true;
        }

        //Use Flags Addon
        if(griefPreventionFlagsHandler != null){
            return griefPreventionFlagsHandler.canSpawn(claim, isHostile) && canSpawnInAllClaims;
        }

        //Default to the boolean
        return canSpawnInAllClaims;
    }

}
