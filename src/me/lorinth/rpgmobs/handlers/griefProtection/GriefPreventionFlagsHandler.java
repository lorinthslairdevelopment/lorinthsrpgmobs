package me.lorinth.rpgmobs.handlers.griefProtection;

import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.ryanhamshire.GPFlags.Flag;
import me.ryanhamshire.GPFlags.FlagManager;
import me.ryanhamshire.GPFlags.GPFlags;
import me.ryanhamshire.GriefPrevention.Claim;
import org.bukkit.Bukkit;

public class GriefPreventionFlagsHandler {

    private GPFlags gpFlags;
    private FlagManager flagManager;

    public GriefPreventionFlagsHandler(){
        gpFlags = (GPFlags) Bukkit.getPluginManager().getPlugin("GriefPreventionFlags");
        if(gpFlags == null){
            RpgMobsOutputHandler.PrintInfo("Loaded hook with Grief Prevention could not find GriefPreventionFlags");
            return;
        }
        flagManager = gpFlags.getFlagManager();
        if(flagManager == null){
            RpgMobsOutputHandler.PrintError("GriefPreventionFlags has no FlagManager...");
        }
    }

    public boolean canSpawn(Claim claim, boolean isHostile){
        if(gpFlags == null || flagManager == null){
            return true;
        }

        Flag flag = flagManager.getFlag(claim, "NoMobSpawns");
        if(flag != null){
            if(flag.getSet())
                return false;
        }

        if(isHostile){
            flag = flagManager.getFlag(claim, "NoMonsterSpawns");
            if(flag != null){
                return !flag.getSet();
            }
        }

        return true;
    }

}
