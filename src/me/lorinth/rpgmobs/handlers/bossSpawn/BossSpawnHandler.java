package me.lorinth.rpgmobs.handlers.bossSpawn;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.mineacademy.boss.api.Boss;
import org.mineacademy.boss.api.BossAPI;
import org.mineacademy.boss.api.BossSpawnReason;

public class BossSpawnHandler implements IBossHandler {

    @Override
    public boolean isBoss(Entity entity) {
        return BossAPI.isBoss(entity);
    }

    public boolean spawnBoss(Location location, String name){
        if(Bukkit.getPluginManager().getPlugin("Boss") == null){
            return false;
        }

        if (name == null){
            return false;
        }

        Boss boss = BossAPI.getBoss(name);
        if(boss == null){
            return false;
        }
        boss.spawn(location, BossSpawnReason.CUSTOM);
        return true;
    }

}
