package me.lorinth.rpgmobs.handlers.bossSpawn;

import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.adapters.AbstractLocation;
import io.lumine.xikage.mythicmobs.adapters.bukkit.BukkitAdapter;
import io.lumine.xikage.mythicmobs.mobs.MythicMob;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;

import java.lang.reflect.Method;

public class MythicMobsSpawnHandler implements IBossHandler {

    private Method spawnMethodDouble;
    private Method spawnMethodInt;

    public boolean isBoss(Entity entity){
        return MythicMobs.inst().getAPIHelper().isMythicMob(entity);
    }

    public boolean spawnBoss(Location location, String name) {
        if (Bukkit.getPluginManager().getPlugin("MythicMobs") == null) {
            return false;
        }

        if (name == null){
            return false;
        }

        MythicMob boss = MythicMobs.inst().getMobManager().getMythicMob(name);
        if (boss == null) {
            return false;
        }
        spawnBoss(boss, BukkitAdapter.adapt(location), 1);
        return true;
    }

    private void spawnBoss(MythicMob boss, AbstractLocation location, int level) {
        boolean found = !methodFound();
        if(!found){
            found = getMethod(boss);
        }

        if(found){
            runMethod(boss, location, level);
        }
        else{
            RpgMobsOutputHandler.PrintError("Tried to spawn a boss with level, but unable to find MythicBoss.spawn() method");
        }
    }

    private boolean getMethod(MythicMob boss){
        try{
            spawnMethodDouble = boss.getClass().getMethod("spawn", AbstractLocation.class, double.class);
            return true;
        } catch(Exception ex){
            try{
                spawnMethodInt = boss.getClass().getMethod("spawn", AbstractLocation.class, int.class);
                return true;
            } catch(Exception ex2) {
                RpgMobsOutputHandler.PrintException("Unable to find method, 'spawn' on 'MythicMobSpawnEvent'", ex2);
            }
        }

        return false;
    }

    private boolean methodFound(){
        return spawnMethodInt != null || spawnMethodDouble != null;
    }

    private boolean runMethod(MythicMob boss, AbstractLocation location, int level){
        try{
            if(spawnMethodDouble != null){
                spawnMethodDouble.invoke(boss, location, (double) level);
            }
            else if(spawnMethodInt != null){
                spawnMethodInt.invoke(boss, location, level);
            }
            return true;
        } catch(Exception ex){
            RpgMobsOutputHandler.PrintException("Unable to find method, 'setMobLevel' on 'MythicMobSpawnEvent' with parameter {level}", ex);
            return false;
        }
    }

}
