package me.lorinth.rpgmobs.handlers.bossSpawn;

import org.bukkit.Location;
import org.bukkit.entity.Entity;

public interface IBossHandler {
    boolean isBoss(Entity entity);
    boolean spawnBoss(Location location, String name);
}
