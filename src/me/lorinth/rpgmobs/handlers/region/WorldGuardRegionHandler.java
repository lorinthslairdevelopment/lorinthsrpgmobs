package me.lorinth.rpgmobs.handlers.region;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import com.sk89q.worldguard.protection.regions.RegionQuery;
import me.lorinth.rpgmobs.Objects.LevelRegion;
import me.lorinth.utils.objects.Version;
import org.bukkit.Location;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;

public class WorldGuardRegionHandler implements IRegionHandler {

    public boolean isValid;
    private com.sk89q.worldguard.bukkit.WorldGuardPlugin WorldGuardPlugin;
    private com.sk89q.worldguard.WorldGuard WG;
    private Version version;

    public WorldGuardRegionHandler(Plugin plugin){
        version = new Version(plugin.getDescription().getVersion());
    }

    @Override
    public void init(boolean isValid) {
        this.isValid = true;

        if(version.getMajorVersion() <= 6){
            WorldGuardPlugin = com.sk89q.worldguard.bukkit.WorldGuardPlugin.inst();
        }
        else{
            WG = com.sk89q.worldguard.WorldGuard.getInstance();
        }
    }

    @Override
    public boolean isValid() {
        return this.isValid;
    }

    @Override
    public LevelRegion getHighestPriorityLevelRegion(HashMap<String, LevelRegion> worldRegions, Location location) {
        if(!isValid)
            return null;

        if(version.getMajorVersion() <= 6){
            return getLevelRegion_Old(worldRegions, location);
        }
        else{
            return getLevelRegion_New(worldRegions, location);
        }
    }

    private LevelRegion getLevelRegion_Old(HashMap<String, LevelRegion> worldRegions, Location location){
        ProtectedRegion highestPriority = null;
        LevelRegion highestLevelRegion = null;
        if(worldRegions == null || worldRegions.size() == 0)
            return null;

        ApplicableRegionSet regionSet = WorldGuardPlugin.getRegionContainer().createQuery().getApplicableRegions(location);
        if(regionSet != null) {
            for(ProtectedRegion region : regionSet.getRegions()){
                if(worldRegions.containsKey(region.getId()) && (highestPriority == null || highestLevelRegion == null || (highestPriority.getPriority() < region.getPriority()) )){
                    highestPriority = region;
                    LevelRegion levelRegion = worldRegions.get(region.getId());
                    if(levelRegion != null && !levelRegion.isDisabled()) {
                        highestPriority = region;
                        highestLevelRegion = levelRegion;
                    }
                }
            }
        }

        return highestLevelRegion;
    }

    private LevelRegion getLevelRegion_New(HashMap<String, LevelRegion> worldRegions, Location location){
        ProtectedRegion highestPriority = null;
        LevelRegion highestLevelRegion = null;
        if(worldRegions == null || worldRegions.size() == 0)
            return null;

        RegionContainer rgContainer = WG.getPlatform().getRegionContainer();
        if(rgContainer != null) {
            RegionQuery query = rgContainer.createQuery();
            ApplicableRegionSet set = query.getApplicableRegions(BukkitAdapter.adapt(location));

            for(ProtectedRegion region : set.getRegions()){
                if(worldRegions.containsKey(region.getId()) && (highestPriority == null || highestLevelRegion == null || (highestPriority.getPriority() < region.getPriority()) )){
                    highestPriority = region;
                    LevelRegion levelRegion = worldRegions.get(region.getId());
                    if(levelRegion != null && !levelRegion.isDisabled()) {
                        highestPriority = region;
                        highestLevelRegion = levelRegion;
                    }
                }
            }
        }

        return highestLevelRegion;
    }

}
