package me.lorinth.rpgmobs.handlers.region;

import br.net.fabiozumbi12.RedProtect.Bukkit.API.RedProtectAPI;
import br.net.fabiozumbi12.RedProtect.Bukkit.RedProtect;
import br.net.fabiozumbi12.RedProtect.Bukkit.Region;
import me.lorinth.rpgmobs.Objects.LevelRegion;
import org.bukkit.Location;

import java.util.HashMap;
import java.util.Map;

public class RedProtectRegionHandler implements IRegionHandler {

    private RedProtectAPI api;
    private boolean isValid;

    @Override
    public void init(boolean isValid) {
        this.isValid = isValid;
        api = RedProtect.get().getAPI();
    }

    @Override
    public boolean isValid() {
        return this.isValid;
    }

    @Override
    public LevelRegion getHighestPriorityLevelRegion(HashMap<String, LevelRegion> worldRegions, Location location) {
        if(worldRegions == null){
            return null;
        }

        Map<Integer, Region> regions = api.getGroupRegions(location);
        if(regions == null){
            return null;
        }

        int highestPriority = 0;
        LevelRegion highestLevelRegion = null;
        for(Map.Entry<Integer, Region> entry : regions.entrySet()){
            if(entry == null){
                continue;
            }

            int priority = entry.getKey();
            Region region = entry.getValue();
            if(region == null){
                continue;
            }
            LevelRegion levelRegion = worldRegions.get(region.getName());
            if(priority > highestPriority && levelRegion != null){
                highestPriority = priority;
                highestLevelRegion = levelRegion;
            }
        }

        return highestLevelRegion;
    }
}
