package me.lorinth.rpgmobs.handlers.region;

import me.lorinth.rpgmobs.Objects.LevelRegion;
import me.ryanhamshire.GriefPrevention.Claim;
import me.ryanhamshire.GriefPrevention.GriefPrevention;
import org.bukkit.Location;

import java.util.HashMap;

public class GriefPreventionHandler implements IRegionHandler {

    private GriefPrevention griefPrevention;
    private boolean isValid;

    @Override
    public void init(boolean isValid) {
        this.isValid = isValid;
        griefPrevention = GriefPrevention.instance;
    }

    @Override
    public boolean isValid() {
        return isValid;
    }

    @Override
    public LevelRegion getHighestPriorityLevelRegion(HashMap<String, LevelRegion> worldRegions, Location location) {
        Claim claim = griefPrevention.dataStore.getClaimAt(location, false, null);
        if(claim == null){
            return null;
        }

        String id = claim.getID().toString();
        return worldRegions.get(id);
    }
}
