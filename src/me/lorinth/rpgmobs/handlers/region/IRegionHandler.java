package me.lorinth.rpgmobs.handlers.region;

import me.lorinth.rpgmobs.Objects.LevelRegion;
import org.bukkit.Location;

import java.util.HashMap;

public interface IRegionHandler {

    void init(boolean isValid);

    boolean isValid();

    /**
     * Gets the highest priority leveled region at a location
     * @param worldRegions the regions in the current world
     * @param location the location to check
     * @return LevelRegion
     */
    LevelRegion getHighestPriorityLevelRegion(HashMap<String, LevelRegion> worldRegions, Location location);

}
