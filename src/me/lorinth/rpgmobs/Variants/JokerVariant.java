package me.lorinth.rpgmobs.Variants;

import me.lorinth.rpgmobs.Events.RpgMobDeathEvent;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.ConfigValue;
import me.lorinth.rpgmobs.Objects.Properties;
import me.lorinth.utils.particles.ParticleHelper;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.*;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Random;

import static me.lorinth.utils.particles.AtRandomLocationsPreset.getParticleAtRandomLocation;

//Mod variant that makes random things, something like Wabbajack.
//Aim is to make that mob completely chaotic
//Supported scenarios:
//1. Player health change
//2. Disarm player
//3. Teleport player
//4. Spawn fused TNT
//5. Disguise as player (only a display name for now)
//6. (Not intentionally, but...) Destroy player weapon
//7. Hydra - Spawn few more enemies when killed
public class JokerVariant extends MobVariant {

    private boolean hardcore;
    private boolean shouldSpawnParticles;
    private String particleName;
    private int particlesCount;
    private double maxDistanceFromSource;
    private double changeHealthChance;
    private double disarmPlayerChance;
    private double disarmedItemDropDistance;
    private double teleportPlayerChance;
    private double teleportPlayerDistance;
    private double surpriseTNTChance;
    private int surpriseTNTTicks;
    private double hydraChance;
    private int numberOfSpawnedEnemies;
    private boolean isDisguiseEnabled;
    private double disguiseChance;
    private String disguiseDeathMessage;
    //private static boolean doYouEvenAimBro;
    private Random random = new Random();

    public JokerVariant() {
        super("Joker", new ArrayList<ConfigValue>(){{
            add(new ConfigValue<>("HARDCORE", false));
            add(new ConfigValue<>("Particles.Enabled", true));
            add(new ConfigValue<>("Particles.Particle", "FLAME"));
            add(new ConfigValue<>("Particles.ParticlesCount", 40));
            add(new ConfigValue<>("Particles.MaxDistanceFromSource", 2.0));
            add(new ConfigValue<>("ChangeHealth.Chance", 10.0));
            add(new ConfigValue<>("Disarm.Chance", 10.0));
            add(new ConfigValue<>("Disarm.Range", 5.0));
            add(new ConfigValue<>("Teleport.Chance", 10.0));
            add(new ConfigValue<>("Teleport.Distance", 20.0));
            add(new ConfigValue<>("Surprise.Chance", 10.0));
            add(new ConfigValue<>("Surprise.Ticks", 20));
            add(new ConfigValue<>("Hydra.Chance", 10.0));
            //add(new ConfigValue<>("Hydra.NumberOfSpawnedEnemies", 2));
            add(new ConfigValue<>("Disguise.Enabled", false));
            add(new ConfigValue<>("Disguise.Chance", 100.0));
            add(new ConfigValue<>("Disguise.DeathMessage", "{player} was slain by {joker}"));
            //add(new ConfigValue<>("DoYouEvenAimBro.Enabled", true));
        }});
    }

    @Override
    protected void loadDetails(FileConfiguration config) {
        ArrayList<ConfigValue> configValues = getConfigValues();
        hardcore = (boolean) configValues.get(0).getValue(config);

        shouldSpawnParticles = (boolean) configValues.get(1).getValue(config);
        particleName = (String) configValues.get(2).getValue(config);
        particlesCount = (int) configValues.get(3).getValue(config);
        maxDistanceFromSource = (double) configValues.get(4).getValue(config);

        changeHealthChance = (double) configValues.get(5).getValue(config);

        disarmPlayerChance = (double) configValues.get(6).getValue(config);
        disarmedItemDropDistance = (double) configValues.get(7).getValue(config);

        teleportPlayerChance = (double) configValues.get(8).getValue(config);
        teleportPlayerDistance = (double) configValues.get(9).getValue(config);

        surpriseTNTChance = (double) configValues.get(10).getValue(config);
        surpriseTNTTicks = (int) configValues.get(11).getValue(config);

        hydraChance = (double) configValues.get(12).getValue(config);
        numberOfSpawnedEnemies = 2;// (int) configValues.get(13).getValue(config);

        isDisguiseEnabled = (boolean) configValues.get(13).getValue(config);
        disguiseChance = (double) configValues.get(14).getValue(config);
        disguiseDeathMessage = (String) configValues.get(15).getValue(config);

        //doYouEvenAimBro = (boolean) configValues.get(16).getValue(config);
    }

    @Override
    boolean augment(Entity entity) {
        if (Math.random() * 100.0 <= disguiseChance && isDisguiseEnabled) {
            Object[] onlinePlayers = Bukkit.getOnlinePlayers().toArray();

            if (onlinePlayers.length == 0) {
                return entity instanceof Monster;
            }

            Player player = (Player) onlinePlayers[random.nextInt(onlinePlayers.length)];
            String playerName = player.getDisplayName();
            this.setDisplayName(playerName);

            entity.setCustomName(playerName);
        }

        return entity instanceof Monster;
    }

    @Override
    void removeAugment(Entity entity) {
        this.setDisplayName(this.getName());
    }

    @Override
    public void onDeathEvent(RpgMobDeathEvent event){
        if (shouldSpawnParticles) {
            Entity joker = event.getEntity();

            getParticleAtRandomLocation(ParticleHelper.toParticle(particleName), joker.getLocation(), particlesCount, maxDistanceFromSource)
                    .runTaskTimer(LorinthsRpgMobs.getPlugin(LorinthsRpgMobs.class), 0, 1);
        }

        if (Math.random() * 100.0 <= changeHealthChance) {
            Player killer = event.getKiller();

            changeKillerHealth(killer);
        }

        if (Math.random() * 100.0 <= disarmPlayerChance) {
            Player killer = event.getKiller();

            disarmPlayer(killer);
        }

        if (hardcore) {
            if (Math.random() * 100.0 <= teleportPlayerChance) {
                Player killer = event.getKiller();

                teleportPlayer(killer);
            }
        }

        if (Math.random() * 100.0 <= surpriseTNTChance) {
            spawnTNT(event.getEntity());
        }

        if (Math.random() * 100.0 <= hydraChance) {
            hydraScenario(event.getEntity());
        }
    }

    //Sometimes it spawns 2 times the numberOfSpawnedEnemies
    private void hydraScenario(Entity entity) {
        Location location = entity.getLocation();

        if (numberOfSpawnedEnemies <= 0) {
            numberOfSpawnedEnemies = (int) (Math.random() * 4) + 2;
        }

        for (int i = 0; i < numberOfSpawnedEnemies; ++i) {
            LivingEntity spawnedEntity = (LivingEntity)entity.getWorld().spawnEntity(location, entity.getType());

            if(Properties.IsAttributeVersion){
                spawnedEntity.setHealth(spawnedEntity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
            }
            else{
                spawnedEntity.setHealth(spawnedEntity.getMaxHealth());
            }
        }
    }

    private void spawnTNT(Entity entity) {
        TNTPrimed tnt = (TNTPrimed) entity.getWorld().spawnEntity(entity.getLocation(), EntityType.PRIMED_TNT);
        tnt.setFuseTicks(surpriseTNTTicks);
    }

    private void teleportPlayer(Player player) {
        player.teleport(calculateNewLocation(player.getLocation(), teleportPlayerDistance));
    }

    private void disarmPlayer(Player killer) {
        ItemStack weapon = killer.getInventory().getItemInMainHand();

        if (weapon == null) return;

        Location itemDropLocation = calculateNewLocation(killer.getEyeLocation(), disarmedItemDropDistance);

        killer.getInventory().setItemInMainHand(null);
        killer.getWorld().dropItem(itemDropLocation, weapon);
    }

    private Location calculateNewLocation(Location location, double maxDifference) {
        double xCord = Math.random() * maxDifference * 2 - maxDifference;
        double yCord = ( maxDifference + location.getY() < 255) ? maxDifference : 255;
        double zCord = Math.random() * maxDifference * 2 - maxDifference;

        return location.add(xCord, yCord, zCord);
    }

    private void changeKillerHealth(Player killer)
    {
        double maxPlayerHealth;

        if (Properties.IsAttributeVersion) {
            maxPlayerHealth = killer.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
        }
        else {
            maxPlayerHealth = killer.getMaxHealth();
        }

        if (hardcore) {
            killer.setHealth((int) (random.nextDouble() * maxPlayerHealth));
        } else {
            killer.setHealth(random.nextDouble() * (maxPlayerHealth - 1.0) + 1.0);
        }
    }

    @Override
    public void onPlayerDeath(PlayerDeathEvent event)
    {
        if (isDisguiseEnabled) {
            String message = disguiseDeathMessage
                    .replace("{player}", event.getEntity().getDisplayName())
                    .replace("{joker}", this.getDisplayName());

            event.setDeathMessage(message);
        }
    }

    //NOT WORKING: JUST NOT CALLED IN ANY SITUATION
    /*@Override
    public void onHit(LivingEntity target, EntityDamageByEntityEvent event){
        if (doYouEvenAimBro) {
            Entity damager = event.getDamager();

            if (!(damager instanceof Player)) return;

            Location location = damager.getLocation();

            float yaw = random.nextFloat() * 360 - 180;
            float pitch = random.nextFloat() * 180 - 90;

            location.setPitch(pitch);
            location.setYaw(yaw);

            damager.teleport(location);
        }
    }*/
}
