package me.lorinth.rpgmobs.Variants;

import me.lorinth.rpgmobs.Events.RpgMobDeathEvent;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.WaterMob;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

//when killed -> spawn lava
public class MagmaVariant extends MobVariant {
    public MagmaVariant() {
        super("Magma");
    }

    @Override
    protected void loadDetails(FileConfiguration config) {

    }

    @Override
    boolean augment(final Entity entity) {
        if(entity instanceof WaterMob)
            return false;
        if(entity instanceof LivingEntity){
            ((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.MAX_VALUE, 1, true));
            Bukkit.getScheduler().runTaskLater(LorinthsRpgMobs.instance, () -> {
                entity.setFireTicks(Integer.MAX_VALUE);
            }, 10);

            return true;
        }
        return false;
    }

    @Override
    void removeAugment(Entity entity){
        entity.setFireTicks(0);
        if(entity instanceof LivingEntity)
            ((LivingEntity) entity).removePotionEffect(PotionEffectType.FIRE_RESISTANCE);
    }

    //Change to whenHit event
    @Override
    public void onDeathEvent(RpgMobDeathEvent event) {
        Location mobLocation = event.getEntity().getLocation();

        Block block = mobLocation.getBlock();

        block.setType(Material.LAVA);
    }
}
