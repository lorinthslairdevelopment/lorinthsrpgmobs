package me.lorinth.rpgmobs.Variants;

import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.ConfigValue;
import me.lorinth.rpgmobs.Objects.Properties;
import me.lorinth.utils.ColorHelper;
import me.lorinth.utils.particles.ParticleHelper;
import me.lorinth.utils.tasks.TemporaryTimer;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Random;

import static me.lorinth.utils.particles.DomePreset.getDome;

//When killed spreads an acid and poison players in near distance
public class AcidBombVariant extends MobVariant {

    private static int duration;
    private static int potionLevel;
    private static double chance;
    private static int distance;
    private static int circlesCount;
    private static int particlesPerCircle;
    private static int maxTimes;
    private static String particle;
    private static String color;
    private static Random random = new Random();

    public AcidBombVariant() {
        super("AcidBomb", new ArrayList<ConfigValue>(){{
            add(new ConfigValue<>("Poison.Duration", 3 * 20));
            add(new ConfigValue<>("Poison.Level", 0));
            add(new ConfigValue<>("Poison.Chance", 80.0));
            add(new ConfigValue<>("Distance", 5));
            add(new ConfigValue<>("Dome.CirclesCount", 8));
            add(new ConfigValue<>("Dome.ParticlesPerCircle", 50));
            add(new ConfigValue<>("Dome.MaxTimes", 2));
            add(new ConfigValue<>("Dome.Particle", "REDSTONE"));
            add(new ConfigValue<>("Dome.Color", "OLIVE"));
        }});
    }

    @Override
    protected void loadDetails(FileConfiguration config) {
        ArrayList<ConfigValue> configValues = getConfigValues();
        duration = (int) configValues.get(0).getValue(config);
        potionLevel = (int) configValues.get(1).getValue(config);
        chance = (double) configValues.get(2).getValue(config);
        distance = (int) configValues.get(3).getValue(config);
        circlesCount = (int) configValues.get(4).getValue(config);
        particlesPerCircle = (int) configValues.get(5).getValue(config);
        maxTimes = (int) configValues.get(6).getValue(config);
        particle = (String) configValues.get(7).getValue(config);
        color = (String) configValues.get(8).getValue(config);
    }

    @Override
    boolean augment(Entity entity) {
        return entity instanceof Monster;
    }

    @Override
    void removeAugment(Entity entity) {

    }

    @Override
    public void onDeath(LivingEntity entity) {
        Location mobLocation = entity.getLocation();

        Color colorConversion = ColorHelper.toColor(color);
        Particle particleConversion = ParticleHelper.toParticle(particle);

        TemporaryTimer particleTimer;

        if (Properties.IsLootableVersion) {
            particleTimer = getDome(particleConversion, mobLocation, distance, circlesCount,
                                                    particlesPerCircle, maxTimes, colorConversion);
        } else {
            particleTimer = getDome(particleConversion, mobLocation, distance, circlesCount,
                    particlesPerCircle, maxTimes);
        }

        particleTimer.runTaskTimer(LorinthsRpgMobs.getPlugin(LorinthsRpgMobs.class), 0, 1);

        mobLocation.getWorld().getPlayers().forEach(player -> {
            if (mobLocation.distance(player.getLocation()) <= distance) {
                if(random.nextDouble() * 100 < chance)
                    player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, duration, potionLevel));
            }
        });
    }
}
