package me.lorinth.rpgmobs.Variants;

import me.lorinth.rpgmobs.Objects.Properties;
import me.lorinth.rpgmobs.Util.MetaDataConstants;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.ConfigValue;
import org.bukkit.NamespacedKey;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import javax.xml.stream.events.Namespace;
import java.util.ArrayList;

public class StrongVariant extends MobVariant {

    private static int rawDamage;
    private static double damageMultiplier;

    public StrongVariant(){
        super("Strong", new ArrayList<ConfigValue>(){{
            add(new ConfigValue<>("Damage", 2));
            add(new ConfigValue<>("DamageMultiplier", 1.0));
        }});
    }

    @Override
    protected void loadDetails(FileConfiguration config){
        ArrayList<ConfigValue> configValues = getConfigValues();
        rawDamage = (int) configValues.get(0).getValue(config);
        damageMultiplier = (double) configValues.get(1).getValue(config);
    }

    @Override
    boolean augment(Entity entity) {
        if(entity instanceof LivingEntity) {
            if(Properties.IsAttributeVersion){
                AttributeInstance instance = ((LivingEntity) entity).getAttribute(Attribute.GENERIC_ATTACK_DAMAGE);
                if (instance != null) {
                    instance.setBaseValue(instance.getValue() * damageMultiplier + rawDamage);
                    return true;
                }
            }

            if(Properties.IsPersistentDataVersion){
                PersistentDataContainer dataContainer = entity.getPersistentDataContainer();
                Double damage = dataContainer.get(new NamespacedKey(LorinthsRpgMobs.instance, "damage"), PersistentDataType.DOUBLE);
                if(damage != null){
                    dataContainer.set(new NamespacedKey(LorinthsRpgMobs.instance, "damage"), PersistentDataType.DOUBLE, damage * damageMultiplier + rawDamage);
                }

            }
            else{
                Double damage = entity.getMetadata(MetaDataConstants.Damage).get(0).asDouble();
                entity.setMetadata(MetaDataConstants.Damage, new FixedMetadataValue(LorinthsRpgMobs.instance,
                        damage * damageMultiplier + rawDamage));
            }

        }
        return false;
    }

    @Override
    void removeAugment(Entity entity){
        if(!(entity instanceof LivingEntity))
            return;

        LivingEntity living = (LivingEntity) entity;
        if(Properties.IsAttributeVersion){
            AttributeInstance instance = living.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE);
            if (instance != null) {
                instance.setBaseValue((instance.getValue() - rawDamage) * (1 / damageMultiplier));
            }
        }
    }
}
