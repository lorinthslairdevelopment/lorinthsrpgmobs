package me.lorinth.rpgmobs.Variants;

import me.lorinth.rpgmobs.Events.RpgMobDeathEvent;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.ConfigValue;
import me.lorinth.rpgmobs.Objects.Properties;
import org.bukkit.NamespacedKey;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;

public class BossVariant extends MobVariant {
    //Limits according to https://minecraft.gamepedia.com/Attribute
    private final double MIN_ARMOR = 0.0;
    private final double MAX_ARMOR = 30.0;
    private final double MIN_KNOCKBACK_RESISTANCE = 0.0;
    private final double MAX_KNOCKBACK_RESISTANCE = 1.0;

    private double healthMultiplier;
    private double currencyValueMultiplier;
    private double experienceMultiplier;
    private double speedMultiplier;
    private double damageMultiplier;
    private double knockbackResistance;
    private double armor;

    public BossVariant() {
        super("Boss", new ArrayList<ConfigValue>(){{
            add(new ConfigValue<>("HealthMultiplier", 10.0));
            add(new ConfigValue<>("CurrencyValueMultiplier", 10.0));
            add(new ConfigValue<>("ExperienceMultiplier", 10.0));
            add(new ConfigValue<>("SpeedMultiplier", 1.0));
            add(new ConfigValue<>("DamageMultiplier", 2.0)); //Require Minecraft 1.9 or greater
            add(new ConfigValue<>("KnockbackResistance", 0.0));
            add(new ConfigValue<>("Armor", 0.0));
        }});
    }

    @Override
    protected void loadDetails(FileConfiguration config) {
        ArrayList<ConfigValue> configValues = getConfigValues();
        healthMultiplier = (double) configValues.get(0).getValue(config);
        currencyValueMultiplier = (double) configValues.get(1).getValue(config);
        experienceMultiplier = (double) configValues.get(2).getValue(config);
        speedMultiplier = (double) configValues.get(3).getValue(config);
        damageMultiplier = (double) configValues.get(4).getValue(config);
        knockbackResistance = applyLimits((double) configValues.get(5).getValue(config), MIN_KNOCKBACK_RESISTANCE, MAX_KNOCKBACK_RESISTANCE);
        armor = applyLimits((double) configValues.get(6).getValue(config), MIN_ARMOR, MAX_ARMOR);
    }

    @Override
    boolean augment(Entity entity) {
        if (entity instanceof Monster) {
            Monster monster = (Monster)entity;
            LivingEntity livingEntity = (LivingEntity)entity;

            if (Properties.IsAttributeVersion) {
                AttributeInstance speed = livingEntity.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED);
                if (speed != null) {
                    speed.setBaseValue(speed.getValue() * speedMultiplier);
                }

                AttributeInstance maxHealth = monster.getAttribute(Attribute.GENERIC_MAX_HEALTH);
                if (maxHealth != null) {
                    maxHealth.setBaseValue(maxHealth.getValue() * healthMultiplier);
                }
                monster.setHealth(maxHealth.getValue());

                AttributeInstance damageAttribute = monster.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE);
                if (damageAttribute != null){
                    double damage = damageAttribute.getValue() * damageMultiplier;
                    damageAttribute.addModifier(new AttributeModifier("RpgMobs_Damage",
                                                                     damage - damageAttribute.getValue(),
                                                                              AttributeModifier.Operation.ADD_NUMBER));
                }

                if(Properties.IsPersistentDataVersion){
                    PersistentDataContainer dataContainer = entity.getPersistentDataContainer();
                    double currentDamage = dataContainer.get(new NamespacedKey(LorinthsRpgMobs.instance, "damage"), PersistentDataType.DOUBLE);
                    double damage = currentDamage * damageMultiplier;
                    dataContainer.set(new NamespacedKey(LorinthsRpgMobs.instance, "damage"), PersistentDataType.DOUBLE, damage);
                }

                AttributeInstance knockbackResist = monster.getAttribute(Attribute.GENERIC_KNOCKBACK_RESISTANCE);
                if (knockbackResist != null) {
                    knockbackResist.setBaseValue(knockbackResistance);
                }

                AttributeInstance armorAttribute = monster.getAttribute(Attribute.GENERIC_ARMOR);
                if (armorAttribute != null) {
                    armorAttribute.setBaseValue(armor);
                }
            }
            else {
                if (speedMultiplier > 1.0) {
                    livingEntity.addPotionEffect(
                            new PotionEffect(PotionEffectType.SPEED,
                                             Integer.MAX_VALUE,
                                             (int) Math.floor(speedMultiplier),
                                             false), true);
                }
                monster.setMaxHealth(monster.getMaxHealth() * healthMultiplier);
                monster.setHealth(monster.getHealth() * healthMultiplier);
            }

            return true;
        }

        return false;
    }

    @Override
    void removeAugment(Entity entity) {
        if(!(entity instanceof Monster))
            return;

        LivingEntity living = (LivingEntity) entity;
        if(Properties.IsAttributeVersion) {
            AttributeInstance speed = living.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED);
            if (speed != null) {
                speed.setBaseValue(speed.getDefaultValue());
            }

            AttributeInstance maxHealth = living.getAttribute(Attribute.GENERIC_MAX_HEALTH);
            if (maxHealth != null) {
                maxHealth.setBaseValue(maxHealth.getDefaultValue());
            }

            AttributeInstance damage = living.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE);
            if (damage != null) {
                damage.setBaseValue(damage.getDefaultValue());
            }

            AttributeInstance knockbackResist = living.getAttribute(Attribute.GENERIC_KNOCKBACK_RESISTANCE);
            if (knockbackResist != null) {
                knockbackResist.setBaseValue(knockbackResist.getDefaultValue());
            }

            AttributeInstance armorAttribute = living.getAttribute(Attribute.GENERIC_ARMOR);
            if (armorAttribute != null) {
                armorAttribute.setBaseValue(armorAttribute.getDefaultValue());
            }
        }
        else
            living.removePotionEffect(PotionEffectType.SPEED);
    }

    @Override
    public void onDeathEvent(RpgMobDeathEvent event) {
        event.setCurrencyValue(event.getCurrencyValue() * currencyValueMultiplier);
        event.setExp((int)Math.ceil(event.getExp() * experienceMultiplier));
    }

    public double getHealthMultiplier() {
        return healthMultiplier;
    }

    public double getCurrencyValueMultiplier() {
        return currencyValueMultiplier;
    }

    public double getExperienceMultiplier() {
        return experienceMultiplier;
    }

    public double getSpeedMultiplier() {
        return speedMultiplier;
    }

    public double getDamageMultiplier() {
        return damageMultiplier;
    }

    private double applyLimits(double value, double min, double max)
    {
        return Math.max(min, Math.min(max, value));
    }
}
