package me.lorinth.rpgmobs.Variants;

import me.lorinth.rpgmobs.Data.MobVariantDataManager;
import me.lorinth.rpgmobs.Events.RpgMobDeathEvent;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.ConfigValue;
import me.lorinth.rpgmobs.Objects.Disableable;
import me.lorinth.rpgmobs.Objects.Loot.CreatureLootData;
import me.lorinth.rpgmobs.Objects.Properties;
import me.lorinth.rpgmobs.Util.MetaDataConstants;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.ChatColor;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

public abstract class MobVariant extends Disableable {

    private String name;
    private String displayName;
    private int weight;
    private ArrayList<String> disabledEntityTypes = new ArrayList<>();
    private ArrayList<ConfigValue> configValues = new ArrayList<>();
    private String prefix = "MobVariants";
    private CreatureLootData lootData;

    public MobVariant(String name){
        this.name = name;
        prefix += "." + name;
        load(LorinthsRpgMobs.instance.getConfig(), LorinthsRpgMobs.instance);
    }

    public MobVariant(String name, ArrayList<ConfigValue> configValues){
        this.name = name;
        prefix += "." + name;
        this.configValues = configValues;
        for(ConfigValue configValue : configValues) {
            configValue.setPath(prefix + "." + configValue.getPath());
        }
        load(LorinthsRpgMobs.instance.getConfig(), LorinthsRpgMobs.instance);
    }

    //Returns if a setting was added to the config (should save file if true)
    public void load(FileConfiguration config, Plugin plugin){
        //Check for config defaults
        if(isMissingConfigSetting(config)) {
            setDefaults(config, plugin, prefix);
        }

        setDisabled(config.getBoolean(prefix + ".Disabled"));
        if(!isDisabled()) {
            if(config.getConfigurationSection(prefix).getKeys(false).contains("DisabledTypes")) {
                disabledEntityTypes.addAll(config.getStringList(prefix + ".DisabledTypes"));
            }
            setDisplayName(config.getString(prefix + ".DisplayName"));
            weight = config.getInt(prefix + ".Weight");
            loadLoot(config);
            loadDetails(config);
            MobVariantDataManager.AddVariant(this);
        }
    }

    public void setDisplayName(String displayName){
        this.displayName = ChatColor.translateAlternateColorCodes('&', displayName);
    }

    public void onHit(LivingEntity target, EntityDamageByEntityEvent event){}

    public void whenHit(LivingEntity attacker, EntityDamageByEntityEvent event){}

    public void onSpawn(LivingEntity entity){}

    public void onDeath(LivingEntity entity){}

    public void onDeathEvent(RpgMobDeathEvent event) {}

    public void onPlayerDeath(PlayerDeathEvent event){}

    public String getName(){
        return this.name;
    }

    public String getDisplayName(){
        return this.displayName;
    }

    public int getWeight(){
        return weight;
    }

    public String getPrefix(){
        return prefix + "." + name;
    }

    protected ArrayList<ConfigValue> getConfigValues(){
        return configValues;
    }

    protected abstract void loadDetails(FileConfiguration config);

    public boolean isDisabledEntityType(EntityType type){
        return disabledEntityTypes.contains(type.name());
    }

    /**
     * Applies the variant to the entity
     * @param entity - the entity to apply the variant to
     */
    public boolean apply(Entity entity){
        if(isDisabled() || LorinthsRpgMobs.IsMythicMob(entity) || disabledEntityTypes.contains(entity.getType().name()))
            return false;

        if(augment(entity)) {
            if(entity != null && getDisplayName() != null && entity.getCustomName() != null) {
                entity.setCustomName(entity.getCustomName().replace("{Variant}", getDisplayName()).replace("{variant}", getDisplayName().toLowerCase()));
            }

            if(Properties.IsPersistentDataVersion) {
                PersistentDataContainer dataContainer = entity.getPersistentDataContainer();
                dataContainer.set(new NamespacedKey(LorinthsRpgMobs.instance, "variant"), PersistentDataType.STRING, getName());
            }
            else{
                entity.setMetadata(MetaDataConstants.Variant, new FixedMetadataValue(LorinthsRpgMobs.instance, this.getName()));
            }

            if(entity instanceof LivingEntity)
                onSpawn((LivingEntity) entity);
            return true;
        }
        return false;
    }

    public void remove(Entity entity){
        removeAugment(entity);
        if(Properties.IsPersistentDataVersion){
            PersistentDataContainer dataContainer = entity.getPersistentDataContainer();
            dataContainer.remove(new NamespacedKey(LorinthsRpgMobs.instance, "variant"));
        } else {
            entity.removeMetadata(MetaDataConstants.Variant, LorinthsRpgMobs.instance);
        }
    }

    /**
     * Use apply(LivingEntity) instead
     * @param entity
     * @return - if the variant took effect
     */
    abstract boolean augment(Entity entity);

    abstract void removeAugment(Entity entity);

    private boolean isMissingConfigSetting(FileConfiguration config){
        if(!ConfigHelper.ConfigContainsPath(config, prefix + ".Disabled")) {
            return true;
        }
        if(configValues.size() == 0){
            return false;
        }

        boolean changed = false;
        for(ConfigValue configValue : configValues){
            if(!ConfigHelper.ConfigContainsPath(config, configValue.getPath())){
                changed = true;
            }
        }
        return changed;
    }

    private void setDefaults(FileConfiguration config, Plugin plugin, String prefix){
        config.set(prefix + ".DisplayName", this.name);
        config.set(prefix + ".Disabled", disabledByDefault());
        config.set(prefix + ".DisabledTypes", new ArrayList<>());
        config.set(prefix + ".Weight", 10);
        for(ConfigValue configValue : configValues){
            configValue.setDefault(config);
        }
        plugin.saveConfig();
    }

    private boolean disabledByDefault(){
        return name.equalsIgnoreCase("Joker") ||
                name.equalsIgnoreCase("Magma") ||
                name.equalsIgnoreCase("Boss");
    }

    private void loadLoot(FileConfiguration config){
        if(ConfigHelper.ConfigContainsPath(config, prefix + ".Loot")){
            lootData = new CreatureLootData(config, prefix + ".Loot");
        }
    }

    public List<ItemStack> getLoot(Integer level){
        if(lootData != null){
            return lootData.getLootByLevel(level);
        }
        return null;
    }

}
