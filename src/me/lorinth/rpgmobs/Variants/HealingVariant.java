package me.lorinth.rpgmobs.Variants;

import me.lorinth.rpgmobs.Events.RpgMobDeathEvent;
import me.lorinth.rpgmobs.Objects.ConfigValue;
import me.lorinth.rpgmobs.Objects.Properties;
import org.bukkit.Particle;
import org.bukkit.attribute.Attribute;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.ArrayList;

//Mob variant that heals player when killed
public class HealingVariant extends MobVariant {
    private double healFor;
    private boolean showHeal;

    public HealingVariant() {
        super("Healing", new ArrayList<ConfigValue>(){{
            add(new ConfigValue("HealFor", 2.0));
            add(new ConfigValue("ShowHeal", true));
        }});
    }

    @Override
    protected void loadDetails(FileConfiguration config) {
        ArrayList<ConfigValue> configValues = getConfigValues();
        healFor = (double) configValues.get(0).getValue(config);
        showHeal = (boolean) configValues.get(1).getValue(config);
    }

    @Override
    boolean augment(Entity entity) {
        return true;
    }

    @Override
    void removeAugment(Entity entity) { }

    @Override
    public void onDeathEvent(RpgMobDeathEvent event){
        Player killer = event.getKiller();
        double actualHealth = killer.getHealth();
        double maxPlayerHealth = 20;

        if(Properties.IsAttributeVersion){
            maxPlayerHealth = killer.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
        }
        else{
            maxPlayerHealth = killer.getMaxHealth();
        }

        if(showHeal){
            killer.getWorld().spawnParticle(Particle.HEART, killer.getLocation().add(0, 1, 0), 1);
        }
        killer.setHealth(Math.min(actualHealth + healFor, maxPlayerHealth));
    }
}
