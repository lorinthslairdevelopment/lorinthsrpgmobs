package me.lorinth.rpgmobs.Variants;

import me.lorinth.rpgmobs.Events.RpgMobDeathEvent;
import me.lorinth.rpgmobs.Objects.ConfigValue;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

import java.util.ArrayList;

public class WealthyVariant extends MobVariant {

    private double currencyValueMutiplier;

    public WealthyVariant(){
        super("Wealthy", new ArrayList<ConfigValue>(){{
            add(new ConfigValue("CurrencyValueMutiplier", 5.0));
        }});
    }

    @Override
    protected void loadDetails(FileConfiguration config) {
        ArrayList<ConfigValue> configValues = getConfigValues();
        currencyValueMutiplier = (double) configValues.get(0).getValue(config);
    }

    @Override
    boolean augment(Entity entity) {
        return entity instanceof LivingEntity;
    }

    @Override
    void removeAugment(Entity entity) {

    }

    @Override
    public void onDeathEvent(RpgMobDeathEvent event) {
        event.setCurrencyValue(event.getCurrencyValue() * currencyValueMutiplier);
    }
}
