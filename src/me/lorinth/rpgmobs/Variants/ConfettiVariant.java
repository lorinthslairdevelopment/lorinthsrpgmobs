package me.lorinth.rpgmobs.Variants;

import me.lorinth.rpgmobs.Objects.ConfigValue;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.ColorHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Firework;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.ArrayList;
import java.util.List;

public class ConfettiVariant extends MobVariant{

    private ArrayList<Color> colors;
    private ArrayList<Color> fadeColors;
    private boolean hasTrail;
    private int power;
    private FireworkEffect.Type type;

    public ConfettiVariant(){
        super("Confetti", new ArrayList<ConfigValue>(){{
            add(new ConfigValue<List<String>>("Colors", new ArrayList<String>(){{ add("RED"); add("BLUE"); add("GREEN"); }}));
            add(new ConfigValue<List<String>>("FadeColors", new ArrayList<String>(){{ add("YELLOW"); add("PURPLE"); add("FUCHSIA"); }}));
            add(new ConfigValue<>("HasTrail", true));
            add(new ConfigValue<>("Power", 0));
            add(new ConfigValue<>("Type", "BURST"));
        }});
    }

    @Override
    protected void loadDetails(FileConfiguration config) {
        colors = new ArrayList<>();
        fadeColors = new ArrayList<>();
        type = FireworkEffect.Type.BURST;

        ArrayList<ConfigValue> configValues = getConfigValues();
        List<String> configColors = (List<String>) configValues.get(0).getValue(config);
        List<String> configFadeColors = (List<String>) configValues.get(1).getValue(config);
        hasTrail = (boolean) configValues.get(2).getValue(config);
        power = (int) configValues.get(3).getValue(config);
        String typeName = (String) configValues.get(4).getValue(config);

        for(String color : configColors){
            Color colorConversion = ColorHelper.toColor(color);
            if(colorConversion == null){
                RpgMobsOutputHandler.PrintError("Invalid color '" + RpgMobsOutputHandler.HIGHLIGHT + color + RpgMobsOutputHandler.ERROR + "' at, " + configValues.get(0).getPath());
                continue;
            }
            this.colors.add(colorConversion);
        }

        for(String fadeColor : configFadeColors){
            Color colorConversion = ColorHelper.toColor(fadeColor);
            if(colorConversion == null){
                RpgMobsOutputHandler.PrintError("Invalid fade color '" + RpgMobsOutputHandler.HIGHLIGHT + fadeColor + RpgMobsOutputHandler.ERROR + "' at, " + configValues.get(1).getPath());
                continue;
            }
            this.fadeColors.add(colorConversion);
        }

        if(!TryParse.parseEnum(FireworkEffect.Type.class, typeName)){
            RpgMobsOutputHandler.PrintError("Invalid firework type '" + RpgMobsOutputHandler.HIGHLIGHT + typeName + RpgMobsOutputHandler.ERROR + "' at, " + configValues.get(4).getPath());
        }
        else{
            type = FireworkEffect.Type.valueOf(typeName);
        }
    }

    @Override
    boolean augment(Entity entity) {
        return true;
    }

    @Override
    void removeAugment(Entity entity) {

    }

    @Override
    public void onDeath(LivingEntity entity){
        Location loc = entity.getLocation();
        Firework firework = loc.getWorld().spawn(loc, Firework.class);
        FireworkMeta meta = firework.getFireworkMeta();
        meta.setPower(power);
        FireworkEffect effect = FireworkEffect.builder()
                .trail(hasTrail)
                .withColor(colors)
                .withFade(fadeColors)
                .with(type)
                .build();
        meta.addEffects(effect);
        firework.setFireworkMeta(meta);
    }

}
