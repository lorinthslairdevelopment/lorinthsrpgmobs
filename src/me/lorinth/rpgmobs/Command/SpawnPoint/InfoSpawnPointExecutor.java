package me.lorinth.rpgmobs.Command.SpawnPoint;

import me.lorinth.rpgmobs.Command.CommandConstants;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandArgument;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.Command.SpawnPointExecutor;
import me.lorinth.rpgmobs.Data.DataLoader;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.SpawnPoint;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by lorinthio on 1/27/2018.
 */
public class InfoSpawnPointExecutor extends CustomCommandExecutor{
    private final SpawnPointExecutor parentExecutor;

    public InfoSpawnPointExecutor(SpawnPointExecutor parent) {
        super("info", "shows detailed info about a spawnpoint", new ArrayList<CustomCommandArgument>(){{
            add(new CustomCommandArgument("name", "name of the spawnpoint you want info about", true));
        }});
        parentExecutor = parent;
    }

    @Override
    public void safeExecute(Player player, String[] args) {
        Location loc = player.getLocation();
        String name = args[0];

        SpawnPoint spawnPoint = LorinthsRpgMobs.GetSpawnPointManager().getSpawnPointInWorldByName(loc.getWorld(), name);

        if(spawnPoint == null) {
            RpgMobsOutputHandler.PrintError(player, "Spawn point not found with the name, " + RpgMobsOutputHandler.HIGHLIGHT + name);
            return;
        }

        ChatColor disabledColor = (spawnPoint.isDisabled() ? ChatColor.RED : ChatColor.GREEN);
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        RpgMobsOutputHandler.PrintCommand(player, "[LorinthsRpgMobs] : " + RpgMobsOutputHandler.HIGHLIGHT + "Spawn Point Info");
        RpgMobsOutputHandler.PrintCommand(player, "Name : " + RpgMobsOutputHandler.HIGHLIGHT + spawnPoint.getName());
        RpgMobsOutputHandler.PrintCommand(player, "Disabled : " + disabledColor + spawnPoint.isDisabled());
        RpgMobsOutputHandler.PrintCommand(player, "Location : " + RpgMobsOutputHandler.HIGHLIGHT + spawnPoint.getCenter().getWorld().getName() + " (" + spawnPoint.getCenter().getBlockX() + "," + spawnPoint.getCenter().getBlockZ() +")");
        RpgMobsOutputHandler.PrintCommand(player, "Start Level : " + RpgMobsOutputHandler.HIGHLIGHT + spawnPoint.getStartingLevel());
        RpgMobsOutputHandler.PrintCommand(player, "Max Level : " + RpgMobsOutputHandler.HIGHLIGHT + (spawnPoint.getMaxLevel() == Integer.MAX_VALUE ? "N/A" : spawnPoint.getMaxLevel()));
        RpgMobsOutputHandler.PrintCommand(player, "Distance : " + RpgMobsOutputHandler.HIGHLIGHT + spawnPoint.getLevelDistance());
        RpgMobsOutputHandler.PrintCommand(player, "Center Buffer : " + RpgMobsOutputHandler.HIGHLIGHT + spawnPoint.getCenterBuffer());
        RpgMobsOutputHandler.PrintCommand(player, "Variance : " + RpgMobsOutputHandler.HIGHLIGHT + spawnPoint.getVariance());
        RpgMobsOutputHandler.PrintCommand(player, "Max Distance : " + RpgMobsOutputHandler.HIGHLIGHT + spawnPoint.getMaxDistance());

        if(spawnPoint.getMaxDistance() != -1){
            boolean isInRange = spawnPoint.getMaxDistance() > spawnPoint.calculateDistance(player.getLocation(), DataLoader.getDistanceAlgorithm());
            String isInRangeMessage = isInRange ? ChatColor.GREEN + "Yes" : ChatColor.RED + "No";
            RpgMobsOutputHandler.PrintCommand(player, "Is in Range : " + isInRangeMessage);
        }

    }

    @Override
    public void sendHelpMessage(Player player){
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " " + parentExecutor.getCommandName();
        RpgMobsOutputHandler.PrintCommand(player, prefix + " " + this.getUserFriendlyCommandText());
        sendCommandArgumentDetails(player);
    }
}
