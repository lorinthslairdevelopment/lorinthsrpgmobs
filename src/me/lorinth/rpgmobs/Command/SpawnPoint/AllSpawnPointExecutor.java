package me.lorinth.rpgmobs.Command.SpawnPoint;

import me.lorinth.rpgmobs.Command.CommandConstants;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.Command.SpawnPointExecutor;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.SpawnPoint;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class AllSpawnPointExecutor extends CustomCommandExecutor {

    private final SpawnPointExecutor parentExecutor;

    public AllSpawnPointExecutor(SpawnPointExecutor parent) {
        super("all", "lists all spawnpoints on the server", null);
        parentExecutor = parent;
    }

    @Override
    public void safeExecute(Player player, String[] args) {
        RpgMobsOutputHandler.PrintCommand(player, ChatColor.UNDERLINE + "All World's RpgMob Spawnpoints");
        for(World world : Bukkit.getWorlds()){
            ArrayList<SpawnPoint> spawnPoints = LorinthsRpgMobs.GetSpawnPointManager().getAllSpawnPointsInWorld(world);
            if(spawnPoints.size() == 0){
                continue;
            }

            RpgMobsOutputHandler.PrintCommand(player, RpgMobsOutputHandler.HIGHLIGHT + world.getName());
            for(SpawnPoint spawnPoint : spawnPoints){
                RpgMobsOutputHandler.PrintCommand(player, CommandConstants.DescriptionDelimeter + spawnPoint.getName());
            }
        }
    }

    @Override
    public void sendHelpMessage(Player player){
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " " + parentExecutor.getCommandName();
        RpgMobsOutputHandler.PrintCommand(player, prefix + " " + this.getUserFriendlyCommandText());
    }

}
