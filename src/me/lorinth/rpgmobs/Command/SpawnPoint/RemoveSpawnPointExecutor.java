package me.lorinth.rpgmobs.Command.SpawnPoint;

import me.lorinth.rpgmobs.Command.CommandConstants;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandArgument;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.Command.SpawnPointExecutor;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.SpawnPoint;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by lorinthio on 1/27/2018.
 */
public class RemoveSpawnPointExecutor extends CustomCommandExecutor {

    private final SpawnPointExecutor parentExecutor;

    public RemoveSpawnPointExecutor(SpawnPointExecutor parent){
        super("remove", "marks a spawn point ", new ArrayList<CustomCommandArgument>(){{
            add(new CustomCommandArgument("name", "name of the spawnpoint you want to delete", true));
        }});
        parentExecutor = parent;
    }

    @Override
    public void safeExecute(Player player, String[] args) {
        Location loc = player.getLocation();
        String name = args[0];

        SpawnPoint spawnPoint = LorinthsRpgMobs.GetSpawnPointManager().getSpawnPointInWorldByName(loc.getWorld(), name);
        if(spawnPoint == null) {
            RpgMobsOutputHandler.PrintError(player, "Spawn point not found with the name, " + RpgMobsOutputHandler.HIGHLIGHT + name);
            return;
        }

        spawnPoint.setDeleted();
        RpgMobsOutputHandler.PrintRawError(player, "Spawn point, " + RpgMobsOutputHandler.HIGHLIGHT + spawnPoint.getName() + RpgMobsOutputHandler.ERROR + ", has been removed");
    }

    @Override
    public void sendHelpMessage(Player player){
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " " + parentExecutor.getCommandName();
        RpgMobsOutputHandler.PrintCommand(player, prefix + " " + this.getUserFriendlyCommandText());
        sendCommandArgumentDetails(player);
    }
}
