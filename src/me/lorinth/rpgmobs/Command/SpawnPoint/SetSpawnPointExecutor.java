package me.lorinth.rpgmobs.Command.SpawnPoint;

import me.lorinth.rpgmobs.Command.CommandConstants;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandArgument;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.Command.SpawnPointExecutor;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.SpawnPoint;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by lorinthio on 1/27/2018.
 */
public class SetSpawnPointExecutor extends CustomCommandExecutor{

    private final SpawnPointExecutor parentExecutor;

    public SetSpawnPointExecutor(SpawnPointExecutor parent){
        super("set", "creates/updates a spawnpoint with the given name", new ArrayList<CustomCommandArgument>(){{
            add(new CustomCommandArgument("name", "name of spawner to create/update", true));
            add(new CustomCommandArgument("distance", "distance to calculate level on", true));
            add(new CustomCommandArgument("level", "starting level for this spawn point", true));
            add(new CustomCommandArgument("maxlevel", "level cap for this spawn point", false));
            add(new CustomCommandArgument("centerBuffer", "center buffer to delay mobs from leveling until further from center", false));
            add(new CustomCommandArgument("variance", "the maximum random variance in level when a mob is spawned using this spawn point", false));
            add(new CustomCommandArgument("maxdistance", "the maximum distance this spawnpoint will affect mobs", false));
        }});
        parentExecutor = parent;
    }

    @Override
    public void safeExecute(Player player, String[] args) {
        Location loc = player.getLocation();
        String name = args[0];
        int distance = Integer.parseInt(args[1]);
        int startLevel = Integer.parseInt(args[2]);

        SpawnPoint spawnPoint = LorinthsRpgMobs.GetSpawnPointManager().getSpawnPointInWorldByName(loc.getWorld(), name);
        //Create
        if(spawnPoint == null){
            LorinthsRpgMobs.GetSpawnPointManager().addSpawnPointInWorld(player.getWorld(), new SpawnPoint(player.getLocation(), name, startLevel, distance));
            spawnPoint = LorinthsRpgMobs.GetSpawnPointManager().getSpawnPointInWorldByName(loc.getWorld(), name);
            if(args.length > 3){
                int maxLevel = Integer.parseInt(args[3]);
                spawnPoint.setMaxLevel(maxLevel);
            }
            if(args.length > 4){
                int centerBuffer = Integer.parseInt(args[4]);
                spawnPoint.setCenterBuffer(centerBuffer);
            }

            if(args.length > 5){
                int variance = Integer.parseInt(args[5]);
                spawnPoint.setVariance(variance);
            }

            if(args.length > 6){
                int maxDistance = Integer.parseInt(args[6]);
                spawnPoint.setMaxDistance(maxDistance);
            }

            RpgMobsOutputHandler.PrintRawInfo(player, "Created spawn point with name, " + RpgMobsOutputHandler.HIGHLIGHT + name);
        }
        //Update
        else{
            spawnPoint.setCenter(loc);
            spawnPoint.setLevelDistance(distance);
            spawnPoint.setStartingLevel(startLevel);

            if(args.length > 3){
                int maxLevel = Integer.parseInt(args[3]);
                spawnPoint.setMaxLevel(maxLevel);
            }
            if(args.length > 4){
                int centerBuffer = Integer.parseInt(args[4]);
                spawnPoint.setCenterBuffer(centerBuffer);
            }

            if(args.length > 5){
                int variance = Integer.parseInt(args[5]);
                spawnPoint.setVariance(variance);
            }

            if(args.length > 6){
                int maxDistance = Integer.parseInt(args[6]);
                spawnPoint.setMaxDistance(maxDistance);
            }

            RpgMobsOutputHandler.PrintRawInfo(player, "Updated spawn point with name, " + RpgMobsOutputHandler.HIGHLIGHT + name);
        }
    }

    @Override
    public void sendHelpMessage(Player player){
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " " + parentExecutor.getCommandName();
        RpgMobsOutputHandler.PrintCommand(player, prefix + " " + this.getUserFriendlyCommandText());
        sendCommandArgumentDetails(player);
    }
}
