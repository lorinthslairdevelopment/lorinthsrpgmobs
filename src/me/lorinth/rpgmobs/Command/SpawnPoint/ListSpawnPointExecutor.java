package me.lorinth.rpgmobs.Command.SpawnPoint;

import me.lorinth.rpgmobs.Command.CommandConstants;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.Command.SpawnPointExecutor;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.SpawnPoint;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by lorinthio on 1/27/2018.
 */
public class ListSpawnPointExecutor extends CustomCommandExecutor{

    private final SpawnPointExecutor parentExecutor;

    public ListSpawnPointExecutor(SpawnPointExecutor parent) {
        super("list", "lists all spawnpoints in your current world", null);
        parentExecutor = parent;
    }

    @Override
    public void safeExecute(Player player, String[] args) {
        ArrayList<SpawnPoint> spawnPoints = LorinthsRpgMobs.GetSpawnPointManager().getAllSpawnPointsInWorld(player.getWorld());
        if(spawnPoints == null || spawnPoints.size() == 0){
            RpgMobsOutputHandler.PrintCommand(player, "No spawn points in current world");
            return;
        }
        RpgMobsOutputHandler.PrintCommand(player, "[LorinthRpgMobs] : " + RpgMobsOutputHandler.HIGHLIGHT + "Spawn Points in world, '" + player.getWorld().getName() + "'");
        for(SpawnPoint spawnPoint : spawnPoints){
            RpgMobsOutputHandler.PrintCommand(player, CommandConstants.DescriptionDelimeter + spawnPoint.getName());
        }
    }

    @Override
    public void sendHelpMessage(Player player){
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " " + parentExecutor.getCommandName();
        RpgMobsOutputHandler.PrintCommand(player, prefix + " " + this.getUserFriendlyCommandText());
    }
}
