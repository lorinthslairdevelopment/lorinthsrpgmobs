package me.lorinth.rpgmobs.Command.Loot;

import me.lorinth.rpgmobs.Command.CommandConstants;
import me.lorinth.rpgmobs.Command.LootExecutor;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandArgument;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.Data.LootManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SearchLootExecutor extends CustomCommandExecutor {

    private LootExecutor parentExecutor;

    public SearchLootExecutor(LootExecutor parentExecutor) {
        super("search", "search for a custom item by search value", new ArrayList<CustomCommandArgument>(){{
            add(new CustomCommandArgument("value", "partial id or name", false));
        }});

        this.parentExecutor = parentExecutor;
    }

    @Override
    public void safeExecute(Player player, String[] args) {
        String searchValueInit = "";
        for(String arg : args){
            searchValueInit += arg + " ";
        }

        final String searchValue = searchValueInit.trim();

        Bukkit.getScheduler().runTaskAsynchronously(LorinthsRpgMobs.instance, () -> {
            List<Map.Entry<String, ItemStack>> itemMatches = LootManager.getSearchItemPage(searchValue);
            RpgMobsOutputHandler.PrintRawInfo(player, "=========== Lorinths Rpg Mobs Custom Items ===========");
            for(Map.Entry<String, ItemStack> entry : itemMatches){
                RpgMobsOutputHandler.PrintInfo(player, entry.getKey() + " : " + getItemName(entry.getValue()));
            }
        });
    }

    private String getItemName(ItemStack itemStack){
        if(itemStack.hasItemMeta()){
            ItemMeta meta = itemStack.getItemMeta();
            return meta.getDisplayName();
        }

        return itemStack.getType().toString();
    }

    @Override
    public void sendHelpMessage(Player player) {
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " " + parentExecutor.getCommandName();
        RpgMobsOutputHandler.PrintCommand(player, prefix + " " + this.getUserFriendlyCommandText());
        sendCommandArgumentDetails(player);
    }
}
