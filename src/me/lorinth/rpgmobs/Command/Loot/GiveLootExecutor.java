package me.lorinth.rpgmobs.Command.Loot;

import me.lorinth.rpgmobs.Command.CommandConstants;
import me.lorinth.rpgmobs.Command.LootExecutor;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandArgument;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.Data.LootManager;
import me.lorinth.utils.TryParse;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class GiveLootExecutor extends CustomCommandExecutor {

    private LootExecutor parentExecutor;

    public GiveLootExecutor(LootExecutor parentExecutor) {
        super("give", "give a custom item to a specified user", new ArrayList<CustomCommandArgument>(){{
            add(new CustomCommandArgument("player", "the player receiving the item", true));
            add(new CustomCommandArgument("itemId", "the custom item you want to give", true));
            add(new CustomCommandArgument("amount", "how many do you want to give", false));
        }});

        this.parentExecutor = parentExecutor;
    }

    @Override
    public void safeExecute(Player player, String[] args) {
        String playerName = args[0];
        String itemId = args[1];

        ItemStack item = LootManager.getItemById(itemId);
        Player targetPlayer = Bukkit.getPlayer(playerName);
        Integer amount = 1;

        if(targetPlayer == null){
            RpgMobsOutputHandler.PrintError(player, "The player with the partial name, " + RpgMobsOutputHandler.HIGHLIGHT + playerName + RpgMobsOutputHandler.ERROR + " does not exist");
            return;
        }
        else if(item == null){
            RpgMobsOutputHandler.PrintError(player, "The custom item with the id, " + RpgMobsOutputHandler.HIGHLIGHT + itemId + RpgMobsOutputHandler.ERROR + " does not exist");
            return;
        }

        if(args.length > 2){
            String amountString = args[2];

            if(!TryParse.parseInt(amountString)){
                RpgMobsOutputHandler.PrintError(player, "Invalid number, " + RpgMobsOutputHandler.HIGHLIGHT + amountString);
                return;
            }
            amount = Integer.parseInt(amountString);
            item.setAmount(amount);
        }


        String itemName = item.getType().toString();
        if(item.hasItemMeta()){
            itemName = item.getItemMeta().getDisplayName();
            if(itemName == null){
                itemName = item.getType().toString();
            }
        }
        targetPlayer.getInventory().addItem(item);
        RpgMobsOutputHandler.PrintInfo(player, targetPlayer.getDisplayName() + " has received " + amount + " of the " + itemName);
    }

    @Override
    public void sendHelpMessage(Player player) {
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " " + parentExecutor.getCommandName();
        RpgMobsOutputHandler.PrintCommand(player, prefix + " " + this.getUserFriendlyCommandText());
        sendCommandArgumentDetails(player);
    }
}
