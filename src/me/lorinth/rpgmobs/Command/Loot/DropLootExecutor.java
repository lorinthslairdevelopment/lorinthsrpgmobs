package me.lorinth.rpgmobs.Command.Loot;

import me.lorinth.rpgmobs.Command.CommandConstants;
import me.lorinth.rpgmobs.Command.LootExecutor;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandArgument;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.Data.LootManager;
import me.lorinth.utils.TryParse;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class DropLootExecutor extends CustomCommandExecutor {

    private LootExecutor parentExecutor;

    public DropLootExecutor(LootExecutor parentExecutor) {
        super("drop", "drop a customer item at a given location", new ArrayList<CustomCommandArgument>(){
            {
                add(new CustomCommandArgument("itemId", "the id of the custom item to drop", true));
                add(new CustomCommandArgument("amount", "the amount of the item to drop", true));
                add(new CustomCommandArgument("x", "the X coord of the location to drop", true));
                add(new CustomCommandArgument("y", "the Y coord of the location to drop", true));
                add(new CustomCommandArgument("z", "the Z coord of the location to drop", true));
            }});

        this.parentExecutor = parentExecutor;
    }

    @Override
    public void safeExecute(Player player, String[] args) {
        String itemId = args[0];
        String amountString = args[1];
        String xString = args[2];
        String yString = args[3];
        String zString = args[4];

        ItemStack item = LootManager.getItemById(itemId);
        if(item == null){
            RpgMobsOutputHandler.PrintError(player, "The custom item with the id, " + RpgMobsOutputHandler.HIGHLIGHT + itemId + RpgMobsOutputHandler.ERROR + " does not exist");
            return;
        }
        else if(!TryParse.parseInt(amountString)){
            RpgMobsOutputHandler.PrintError(player, "Invalid amount, " + RpgMobsOutputHandler.HIGHLIGHT + amountString);
        }
        else if(!TryParse.parseDouble(xString)){
            RpgMobsOutputHandler.PrintError(player, "Invalid x, " + RpgMobsOutputHandler.HIGHLIGHT + xString);
        }
        else if(!TryParse.parseDouble(yString)){
            RpgMobsOutputHandler.PrintError(player, "Invalid y, " + RpgMobsOutputHandler.HIGHLIGHT + yString);
        }
        else if(!TryParse.parseDouble(zString)){
            RpgMobsOutputHandler.PrintError(player, "Invalid z, " + RpgMobsOutputHandler.HIGHLIGHT + zString);
        }

        String itemName = item.getType().toString();
        if(item.hasItemMeta()){
            itemName = item.getItemMeta().getDisplayName();
            if(itemName == null){
                itemName = item.getType().toString();
            }
        }
        item.setAmount(Integer.parseInt(amountString));
        player.getWorld().dropItem(new Location(player.getWorld(), Double.parseDouble(xString), Double.parseDouble(yString), Double.parseDouble(zString)), item);
        RpgMobsOutputHandler.PrintInfo(player, itemName + " was dropped at " + xString + ", " + yString + ", " + zString);
    }

    @Override
    public void sendHelpMessage(Player player) {
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " " + parentExecutor.getCommandName();
        RpgMobsOutputHandler.PrintCommand(player, prefix + " " + this.getUserFriendlyCommandText());
        sendCommandArgumentDetails(player);
    }
}
