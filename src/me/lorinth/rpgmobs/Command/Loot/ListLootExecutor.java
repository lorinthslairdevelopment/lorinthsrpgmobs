package me.lorinth.rpgmobs.Command.Loot;

import me.lorinth.rpgmobs.Command.CommandConstants;
import me.lorinth.rpgmobs.Command.LootExecutor;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandArgument;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.Data.LootManager;
import me.lorinth.utils.TryParse;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Map;

public class ListLootExecutor extends CustomCommandExecutor {

    private LootExecutor parentExecutor;

    public ListLootExecutor(LootExecutor parentExecutor) {
        super("list", "lists all the custom items with id and Display Name", new ArrayList<CustomCommandArgument>(){{
            add(new CustomCommandArgument("page", "the page of loot ids you want to see", false));
        }});

        this.parentExecutor = parentExecutor;
    }

    @Override
    public void safeExecute(Player player, String[] args) {
        int pageNumber = 1;

        if(args.length == 1){
           if(TryParse.parseInt(args[0])){
               pageNumber = Integer.parseInt(args[0]);
           }
           else{
               RpgMobsOutputHandler.PrintError(player, "Can't parse, " + RpgMobsOutputHandler.HIGHLIGHT + args[0] + RpgMobsOutputHandler.ERROR + " to a number");
               return;
           }
        }

        RpgMobsOutputHandler.PrintRawInfo(player, "======= Lorinths Rpg Mobs Custom Items (page: " + pageNumber + ") =======");
        for(Map.Entry<String, ItemStack> entry : LootManager.getLootItemPage(pageNumber-1)){
            RpgMobsOutputHandler.PrintInfo(player, entry.getKey() + " : " + getItemName(entry.getValue()));
        }
    }

    private String getItemName(ItemStack itemStack){
        if(itemStack == null){
            return "";
        }
        if(itemStack.hasItemMeta()){
            ItemMeta meta = itemStack.getItemMeta();
            return meta.getDisplayName();
        }

        return itemStack.getType().toString();
    }

    @Override
    public void sendHelpMessage(Player player) {
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " " + parentExecutor.getCommandName();
        RpgMobsOutputHandler.PrintCommand(player, prefix + " " + this.getUserFriendlyCommandText());
        sendCommandArgumentDetails(player);
    }
}
