package me.lorinth.rpgmobs.Command;

import me.lorinth.rpgmobs.Command.SpawnPoint.*;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class SpawnPointExecutor extends CustomCommandExecutor {

    private final AllSpawnPointExecutor allSpawnPointExecutor = new AllSpawnPointExecutor(this);
    private final InfoSpawnPointExecutor infoSpawnPointExecutor = new InfoSpawnPointExecutor(this);
    private final ListSpawnPointExecutor listSpawnPointExecutor = new ListSpawnPointExecutor(this);
    private final RemoveSpawnPointExecutor removeSpawnPointExecutor = new RemoveSpawnPointExecutor(this);
    private final SetSpawnPointExecutor setSpawnPointExecutor = new SetSpawnPointExecutor(this);

    public SpawnPointExecutor(){
        super("spawnpoint","access to editing spawnpoint data", null);
        RequiredArguments = 1;
    }

    @Override
    public void safeExecute(Player player, String[] args){
        String commandLabel = args[0];
        args = Arrays.copyOfRange(args, 1, args.length);
        if(commandLabel.equalsIgnoreCase(allSpawnPointExecutor.getCommandName())) {
            allSpawnPointExecutor.execute(player, args);
        }
        else if(commandLabel.equalsIgnoreCase(infoSpawnPointExecutor.getCommandName())) {
            infoSpawnPointExecutor.execute(player, args);
        }
        else if(commandLabel.equalsIgnoreCase(listSpawnPointExecutor.getCommandName())) {
            listSpawnPointExecutor.execute(player, args);
        }
        else if(commandLabel.equalsIgnoreCase(removeSpawnPointExecutor.getCommandName())) {
            removeSpawnPointExecutor.execute(player, args);
        }
        else if(commandLabel.equalsIgnoreCase(setSpawnPointExecutor.getCommandName())) {
            setSpawnPointExecutor.execute(player, args);
        }
        else {
            sendHelpMessage(player);
        }
    }

    @Override
    public void sendHelpMessage(Player player){
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " ";
        RpgMobsOutputHandler.PrintCommand(player, "[LorinthsRpgMobs] : " + RpgMobsOutputHandler.HIGHLIGHT + "Spawn Point Command List");
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getUserFriendlyCommandText());
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getCommandName() + " " + allSpawnPointExecutor.getUserFriendlyCommandText());
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getCommandName() + " " + infoSpawnPointExecutor.getUserFriendlyCommandText());
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getCommandName() + " " + listSpawnPointExecutor.getUserFriendlyCommandText());
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getCommandName() + " " + removeSpawnPointExecutor.getUserFriendlyCommandText());
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getCommandName() + " " + setSpawnPointExecutor.getUserFriendlyCommandText());
    }
}
