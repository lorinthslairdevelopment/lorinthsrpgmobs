package me.lorinth.rpgmobs.Command.LevelRegion;

import me.lorinth.rpgmobs.Command.CommandConstants;
import me.lorinth.rpgmobs.Command.LevelRegionExecutor;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandArgument;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.LevelRegion;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by lorinthio on 1/29/2018.
 */
public class InfoLevelRegionExecutor extends CustomCommandExecutor{

    LevelRegionExecutor parentExecutor;

    public InfoLevelRegionExecutor(LevelRegionExecutor parent) {
        super("info", "shows level information for a world guard region", new ArrayList<CustomCommandArgument>(){{
            add(new CustomCommandArgument("regionId", "the Id of the world guard region you want to view info about", true));
        }});
        parentExecutor = parent;
    }

    @Override
    public void safeExecute(Player player, String[] args) {
        String name = args[0].toLowerCase();
        LevelRegion levelRegion = LorinthsRpgMobs.GetLevelRegionManager().getLevelRegionByName(player.getWorld(), name);

        if(levelRegion == null){
            RpgMobsOutputHandler.PrintError(player, "Level Region not found with the name, " + RpgMobsOutputHandler.HIGHLIGHT + name);
            return;
        }

        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        RpgMobsOutputHandler.PrintCommand(player, "[LorinthsRpgMobs] : " + RpgMobsOutputHandler.HIGHLIGHT + "Level Region Info");
        RpgMobsOutputHandler.PrintCommand(player, "Name : " + RpgMobsOutputHandler.HIGHLIGHT + levelRegion.getName());
        RpgMobsOutputHandler.PrintCommand(player, "Level : " + RpgMobsOutputHandler.HIGHLIGHT + levelRegion.getLevelRange());
    }

    @Override
    public void sendHelpMessage(Player player) {
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " " + parentExecutor.getCommandName();
        RpgMobsOutputHandler.PrintCommand(player, prefix + " " + this.getUserFriendlyCommandText());
        sendCommandArgumentDetails(player);
    }
}
