package me.lorinth.rpgmobs.Command.LevelRegion;

import me.lorinth.rpgmobs.Command.CommandConstants;
import me.lorinth.rpgmobs.Command.LevelRegionExecutor;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Data.LevelRegionManager;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.LevelRegion;
import org.bukkit.entity.Player;

/**
 * Created by lorinthio on 1/29/2018.
 */
public class HereLevelRegionExecutor extends CustomCommandExecutor{

    LevelRegionExecutor parentExecutor;

    public HereLevelRegionExecutor(LevelRegionExecutor parent) {
        super("here", "shows the level that would be calculated at your location (and the region if applicable)", null);
        parentExecutor = parent;
    }

    @Override
    public void safeExecute(Player player, String[] args) {
        LevelRegionManager levelRegionManager = LorinthsRpgMobs.GetLevelRegionManager();
        int level = LorinthsRpgMobs.GetLevelAtLocation(player.getLocation());
        LevelRegion levelRegion = levelRegionManager.getHighestPriorityLeveledRegionAtLocation(player.getLocation());

        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        RpgMobsOutputHandler.PrintCommand(player, "[LorinthsRpgMobs] : " + RpgMobsOutputHandler.HIGHLIGHT + "Level info for your location");
        if(levelRegion != null){
            RpgMobsOutputHandler.PrintCommand(player, "Level Region : " + RpgMobsOutputHandler.HIGHLIGHT + levelRegion.getName());
            RpgMobsOutputHandler.PrintCommand(player, "Level : " + RpgMobsOutputHandler.HIGHLIGHT + levelRegion.getLevelRange());
        }
        else {
            RpgMobsOutputHandler.PrintCommand(player, "Level : " + RpgMobsOutputHandler.HIGHLIGHT + level);
        }
    }

    @Override
    public void sendHelpMessage(Player player) {
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " " + parentExecutor.getCommandName();
        RpgMobsOutputHandler.PrintCommand(player, prefix + " " + this.getUserFriendlyCommandText());
    }
}
