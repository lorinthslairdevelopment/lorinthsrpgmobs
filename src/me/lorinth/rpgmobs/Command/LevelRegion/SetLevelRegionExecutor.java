package me.lorinth.rpgmobs.Command.LevelRegion;

import me.lorinth.rpgmobs.Command.CommandConstants;
import me.lorinth.rpgmobs.Command.LevelRegionExecutor;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandArgument;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.LevelRegion;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by lorinthio on 1/29/2018.
 */
public class SetLevelRegionExecutor extends CustomCommandExecutor {

    LevelRegionExecutor parentExecutor;

    public SetLevelRegionExecutor(LevelRegionExecutor parent) {
        super("set", "Creates/Updates a level region with the given level", new ArrayList<CustomCommandArgument>(){{
            add(new CustomCommandArgument("regionId", "the Id of the level region you want to create/update", true));
            add(new CustomCommandArgument("level", "the level of the region", true));
        }});
        parentExecutor = parent;
    }


    @Override
    public void safeExecute(Player player, String[] args) {
        String name = args[0].toLowerCase();
        String level = args[1];

        LevelRegion region = LorinthsRpgMobs.GetLevelRegionManager().getLevelRegionByName(player.getWorld(), name);
        if(region == null){
            RpgMobsOutputHandler.PrintRawInfo(player, "Added level region with name, " + RpgMobsOutputHandler.HIGHLIGHT + name);
            LorinthsRpgMobs.GetLevelRegionManager().addLevelRegionToWorld(player.getWorld(), new LevelRegion(name, level));
        }
        else{
            region.setLevel(level);
            RpgMobsOutputHandler.PrintRawInfo(player, "Updated level region with name, " + RpgMobsOutputHandler.HIGHLIGHT + name);
        }
    }

    @Override
    public void sendHelpMessage(Player player) {
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " " + parentExecutor.getCommandName();
        RpgMobsOutputHandler.PrintCommand(player, prefix + " " + this.getUserFriendlyCommandText());
        sendCommandArgumentDetails(player);
    }
}
