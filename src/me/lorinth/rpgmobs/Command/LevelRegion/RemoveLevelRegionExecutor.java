package me.lorinth.rpgmobs.Command.LevelRegion;

import me.lorinth.rpgmobs.Command.CommandConstants;
import me.lorinth.rpgmobs.Command.LevelRegionExecutor;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandArgument;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.LevelRegion;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by lorinthio on 1/29/2018.
 */
public class RemoveLevelRegionExecutor extends CustomCommandExecutor{

    LevelRegionExecutor parentExecutor;

    public RemoveLevelRegionExecutor(LevelRegionExecutor parent) {
        super("remove", "marks a region for deletion on the next server reload/stop", new ArrayList<CustomCommandArgument>(){{
            add(new CustomCommandArgument("regionId", "the Id of the level region you want to delete", true));
        }});
        parentExecutor = parent;
    }


    @Override
    public void safeExecute(Player player, String[] args) {
        String name = args[0].toLowerCase();

        LevelRegion region = LorinthsRpgMobs.GetLevelRegionManager().getLevelRegionByName(player.getWorld(), name);
        if(region == null){
            RpgMobsOutputHandler.PrintError(player, "Level Region not found with the name, " + RpgMobsOutputHandler.HIGHLIGHT + name);
            return;
        }

        region.setDeleted();
        RpgMobsOutputHandler.PrintRawError(player, "Level Region, " + RpgMobsOutputHandler.HIGHLIGHT + region.getName() + RpgMobsOutputHandler.ERROR + ", has been removed");
    }

    @Override
    public void sendHelpMessage(Player player) {
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " " + parentExecutor.getCommandName();
        RpgMobsOutputHandler.PrintCommand(player, prefix + " " + this.getUserFriendlyCommandText());
        sendCommandArgumentDetails(player);
    }
}
