package me.lorinth.rpgmobs.Command.LevelRegion;

import me.lorinth.rpgmobs.Command.CommandConstants;
import me.lorinth.rpgmobs.Command.LevelRegionExecutor;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandArgument;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.LevelRegion;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.ryanhamshire.GriefPrevention.Claim;
import me.ryanhamshire.GriefPrevention.GriefPrevention;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class GpSetLevelRegionExecutor extends CustomCommandExecutor {

    LevelRegionExecutor parentExecutor;

    public GpSetLevelRegionExecutor(LevelRegionExecutor parent) {
        super("gpset", "Creates/Updates the grief prevention claim you're in with the provided level", new ArrayList<CustomCommandArgument>(){{
            add(new CustomCommandArgument("level", "the level of the region", true));
        }});
        parentExecutor = parent;
    }

    @Override
    public void safeExecute(Player player, String[] args) {
        String level = args[0];

        GriefPrevention griefPrevention = GriefPrevention.instance;
        Claim claim = griefPrevention.dataStore.getClaimAt(player.getLocation(), false, null);
        if(claim == null){
            RpgMobsOutputHandler.PrintError(player, "You are not standing in a GriefPrevention claim");
            return;
        }

        String id = claim.getID().toString();
        LevelRegion region = LorinthsRpgMobs.GetLevelRegionManager().getLevelRegionByName(player.getWorld(), id);
        if(region == null){
            RpgMobsOutputHandler.PrintRawInfo(player, "Added level region with id, " + RpgMobsOutputHandler.HIGHLIGHT + id);
            LorinthsRpgMobs.GetLevelRegionManager().addLevelRegionToWorld(player.getWorld(), new LevelRegion(id, level));
        }
        else{
            region.setLevel(level);
            RpgMobsOutputHandler.PrintRawInfo(player, "Updated level region with id, " + RpgMobsOutputHandler.HIGHLIGHT + id);
        }
    }

    @Override
    public void sendHelpMessage(Player player) {
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " " + parentExecutor.getCommandName();
        RpgMobsOutputHandler.PrintCommand(player, prefix + " " + this.getUserFriendlyCommandText());
        sendCommandArgumentDetails(player);
    }

}
