package me.lorinth.rpgmobs.Command;

import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Updater;
import org.bukkit.entity.Player;

public class UpdateExecutor extends CustomCommandExecutor {


    public UpdateExecutor() {
        super("update", "updates LorinthsRpgMobs if available", null);
    }

    @Override
    public void safeExecute(Player player, String[] args) {
        RpgMobsOutputHandler.PrintCommand(player, "Update starting...");
        LorinthsRpgMobs.ForceUpdate(updater ->
        {
            if(updater.getResult() == Updater.UpdateResult.NO_UPDATE)
                RpgMobsOutputHandler.PrintCommand(player, "[LorinthsRpgMobs] : " + RpgMobsOutputHandler.HIGHLIGHT + "You already have the latest version!");
            else if(updater.getResult() == Updater.UpdateResult.SUCCESS)
                RpgMobsOutputHandler.PrintCommand(player, "[LorinthsRpgMobs] : " + RpgMobsOutputHandler.HIGHLIGHT + "Update finished! Restart server for the update to take effect!");
            else
                RpgMobsOutputHandler.PrintCommand(player, "[LorinthsRpgMobs] : " + RpgMobsOutputHandler.HIGHLIGHT + "Update Result = " + updater.getResult().toString());
        });
    }

    @Override
    public void sendHelpMessage(Player player) {
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " ";
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getUserFriendlyCommandText());
    }
}
