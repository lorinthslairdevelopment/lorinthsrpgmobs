package me.lorinth.rpgmobs.Command;

import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import org.bukkit.entity.Player;

public class ReloadExecutor extends CustomCommandExecutor {

    public ReloadExecutor(){
        super("reload","reloads the plugin", null);
    }

    @Override
    public void safeExecute(Player player, String[] args){
        RpgMobsOutputHandler.PrintCommand(player, "Reloading...");
        LorinthsRpgMobs.Reload();
        RpgMobsOutputHandler.PrintCommand(player, "Reloaded!");
    }

    @Override
    public void sendHelpMessage(Player player){
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " ";
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getUserFriendlyCommandText());
    }
}
