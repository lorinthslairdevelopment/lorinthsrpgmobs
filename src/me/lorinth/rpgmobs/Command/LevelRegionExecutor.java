package me.lorinth.rpgmobs.Command;

import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Command.LevelRegion.SetLevelRegionExecutor;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.Command.LevelRegion.*;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class LevelRegionExecutor extends CustomCommandExecutor {

    HereLevelRegionExecutor hereLevelRegionExecutor = new HereLevelRegionExecutor(this);
    GpSetLevelRegionExecutor gpSetLevelRegionExecutor = new GpSetLevelRegionExecutor(this);
    InfoLevelRegionExecutor infoLevelRegionExecutor = new InfoLevelRegionExecutor(this);
    ListLevelRegionExecutor listLevelRegionExecutor = new ListLevelRegionExecutor(this);
    RemoveLevelRegionExecutor removeLevelRegionExecutor = new RemoveLevelRegionExecutor(this);
    SetLevelRegionExecutor setLevelRegionExecutor = new SetLevelRegionExecutor(this);

    public LevelRegionExecutor(){
        super("region", "access to editing region data", null);
        RequiredArguments = 1;
    }

    @Override
    public void safeExecute(Player player, String[] args){
        String commandLabel = args[0];
        args = Arrays.copyOfRange(args, 1, args.length);
        if(commandLabel.equalsIgnoreCase(hereLevelRegionExecutor.getCommandName())) {
            hereLevelRegionExecutor.execute(player, args);
        }
        else if(commandLabel.equalsIgnoreCase(gpSetLevelRegionExecutor.getCommandName())){
            gpSetLevelRegionExecutor.execute(player, args);
        }
        else if(commandLabel.equalsIgnoreCase(infoLevelRegionExecutor.getCommandName())) {
            infoLevelRegionExecutor.execute(player, args);
        }
        else if(commandLabel.equalsIgnoreCase(listLevelRegionExecutor.getCommandName())) {
            listLevelRegionExecutor.execute(player, args);
        }
        else if(commandLabel.equalsIgnoreCase(removeLevelRegionExecutor.getCommandName())) {
            removeLevelRegionExecutor.execute(player, args);
        }
        else if(commandLabel.equalsIgnoreCase(setLevelRegionExecutor.getCommandName())) {
            setLevelRegionExecutor.execute(player, args);
        }
        else {
            sendHelpMessage(player);
        }
    }

    @Override
    public void sendHelpMessage(Player player){
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " ";
        RpgMobsOutputHandler.PrintCommand(player, "[LorinthsRpgMobs] : " + RpgMobsOutputHandler.HIGHLIGHT + "Level Region Command List");
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getUserFriendlyCommandText());
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getCommandName() + " " + hereLevelRegionExecutor.getUserFriendlyCommandText());
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getCommandName() + " " + gpSetLevelRegionExecutor.getUserFriendlyCommandText());
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getCommandName() + " " + infoLevelRegionExecutor.getUserFriendlyCommandText());
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getCommandName() + " " + listLevelRegionExecutor.getUserFriendlyCommandText());
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getCommandName() + " " + removeLevelRegionExecutor.getUserFriendlyCommandText());
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getCommandName() + " " + setLevelRegionExecutor.getUserFriendlyCommandText());
    }
}
