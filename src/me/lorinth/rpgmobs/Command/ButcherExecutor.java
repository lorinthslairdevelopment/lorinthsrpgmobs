package me.lorinth.rpgmobs.Command;

import me.lorinth.rpgmobs.Command.Objects.CustomCommandArgument;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ButcherExecutor extends CustomCommandExecutor {

    private List<String> MonsterNames = new ArrayList<String>(){{
        add("org.bukkit.entity.phantom");
    }};

    public ButcherExecutor(){
        super("butcher","butchers all loaded mobs",
                new ArrayList<CustomCommandArgument>(){{
                    add(new CustomCommandArgument("help / radius", "enter help to get arguments or enter the radius around you to butcher", false));
        }});
    }

    public void codeExecute(World world, String[] args){
        boolean removeAnimals = false;
        boolean removeGolems = false;
        boolean removeArmorStands = false;
        boolean removeAmbient = false;
        boolean removeVillagers = false;
        boolean removeMobs = true;
        boolean removeZombieVillagers = true;
        boolean removeFlying = false;

        if(args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                if (args[i].equalsIgnoreCase("-a"))
                    removeAnimals = true;
                else if (args[i].equalsIgnoreCase("-g"))
                    removeGolems = true;
                else if (args[i].equalsIgnoreCase("-s"))
                    removeArmorStands = true;
                else if (args[i].equalsIgnoreCase("-b"))
                    removeAmbient = true;
                else if (args[i].equalsIgnoreCase("-v"))
                    removeVillagers = true;
                else if (args[i].equalsIgnoreCase("-m"))
                    removeMobs = false;
                else if (args[i].equalsIgnoreCase("-z"))
                    removeZombieVillagers = false;
                else if (args[i].equalsIgnoreCase("-f"))
                    removeFlying = true;
            }
        }

        RpgMobsOutputHandler.PrintInfo("Butchering in " + RpgMobsOutputHandler.HIGHLIGHT + world.getName() + RpgMobsOutputHandler.INFO + "...");
        String removeMessage = "Removing the following entities";
        if(removeMobs)
            removeMessage += ", Monsters";
        if(removeAnimals)
            removeMessage += ", Animals";
        if(removeGolems)
            removeMessage += ", Iron Golems";
        if(removeArmorStands)
            removeMessage += ", Armor Stands";
        if(removeAmbient)
            removeMessage += ", Ambient";
        if(removeVillagers)
            removeMessage += ", Villagers";
        if(removeZombieVillagers)
            removeMessage += ", Zombie Villagers";
        if(removeFlying)
            removeMessage += ", Flying";
        RpgMobsOutputHandler.PrintInfo(removeMessage);

        for(LivingEntity entity : world.getLivingEntities()){
            if(!(entity instanceof Player)){
                if(entity instanceof  Tameable){
                    Tameable tameable = (Tameable) entity;
                    if(!tameable.isTamed())
                        removeEntity(null, -1, entity, removeAnimals, removeGolems, removeArmorStands, removeAmbient, removeVillagers, removeMobs, removeZombieVillagers, removeFlying);
                }
                else{
                    removeEntity(null, -1, entity, removeAnimals, removeGolems, removeArmorStands, removeAmbient, removeVillagers, removeMobs, removeZombieVillagers, removeFlying);
                }
            }
        }
        RpgMobsOutputHandler.PrintInfo("Done!");
    }

    @Override
    public void safeExecute(Player player, String[] args){
        int radius = 0;
        boolean removeAnimals = false;
        boolean removeGolems = false;
        boolean removeArmorStands = false;
        boolean removeAmbient = false;
        boolean removeVillagers = false;
        boolean removeMobs = true;
        boolean removeZombieVillagers = true;
        boolean removeFlying = false;

        if(args.length > 0 && TryParse.parseInt(args[0])) {
            radius = Integer.parseInt(args[0]);
            args = Arrays.copyOfRange(args, 1, args.length);
        }
        else{
            if(args.length > 0 && args[0].equalsIgnoreCase("help")){
                sendHelpMessage(player);
                return;
            }
        }
        if(args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                if (args[i].equalsIgnoreCase("-a"))
                    removeAnimals = true;
                else if (args[i].equalsIgnoreCase("-g"))
                    removeGolems = true;
                else if (args[i].equalsIgnoreCase("-s"))
                    removeArmorStands = true;
                else if (args[i].equalsIgnoreCase("-b"))
                    removeAmbient = true;
                else if (args[i].equalsIgnoreCase("-v"))
                    removeVillagers = true;
                else if (args[i].equalsIgnoreCase("-m"))
                    removeMobs = false;
                else if (args[i].equalsIgnoreCase("-z"))
                    removeZombieVillagers = false;
                else if (args[i].equalsIgnoreCase("-f"))
                    removeFlying = true;
            }
        }


        RpgMobsOutputHandler.PrintCommand(player, "Butchering...");
        String removeMessage = "Removing the following entities";
        if(removeMobs)
            removeMessage += ", Monsters";
        if(removeAnimals)
            removeMessage += ", Animals";
        if(removeGolems)
            removeMessage += ", Iron Golems";
        if(removeArmorStands)
            removeMessage += ", Armor Stands";
        if(removeAmbient)
            removeMessage += ", Ambient";
        if(removeVillagers)
            removeMessage += ", Villagers";
        if(removeZombieVillagers)
            removeMessage += ", Zombie Villagers";
        if(removeFlying)
            removeMessage += ", Flying";
        RpgMobsOutputHandler.PrintCommand(player, removeMessage);

        Location loc = player.getLocation();
        for(LivingEntity entity : player.getWorld().getLivingEntities()){
            if(!(entity instanceof Player)){
                if(entity instanceof  Tameable){
                    Tameable tameable = (Tameable) entity;
                    if(!tameable.isTamed())
                        removeEntity(loc, radius, entity, removeAnimals, removeGolems, removeArmorStands, removeAmbient, removeVillagers, removeMobs, removeZombieVillagers, removeFlying);
                }
                else{
                    removeEntity(loc, radius, entity, removeAnimals, removeGolems, removeArmorStands, removeAmbient, removeVillagers, removeMobs, removeZombieVillagers, removeFlying);
                }
            }
        }
        RpgMobsOutputHandler.PrintCommand(player, "Done!");
    }

    private void removeEntity(Location location, int radius, Entity entity,
            boolean removeAnimals,
            boolean removeGolems,
            boolean removeArmorStands,
            boolean removeAmbient,
            boolean removeVillagers,
            boolean removeMobs,
            boolean removeZombieVillagers,
            boolean removeFlying) {
        if(!isCloseEnough(location, entity, radius))
            return;
        if (entity instanceof ZombieVillager && removeZombieVillagers) {
            entity.remove();
            return;
        }
        if (entity instanceof ZombieVillager && !removeZombieVillagers) {
            return;
        }
        if (entity instanceof Villager && removeVillagers){
            entity.remove();
            return;
        }
        if(entity instanceof ArmorStand && removeArmorStands){
            entity.remove();
            return;
        }
        if ((entity instanceof IronGolem || entity instanceof Snowman) && removeGolems){
            entity.remove();
            return;
        }
        if(entity instanceof Flying && removeFlying){
            entity.remove();
            return;
        }
        if((entity instanceof Ambient || entity instanceof Squid) && removeAmbient){
            entity.remove();
            return;
        }
        if(entity instanceof Monster && removeMobs){
            entity.remove();
            return;
        }
        if(entity instanceof Creature && !(entity instanceof Monster) && removeAnimals){
            entity.remove();
        }
    }

    private boolean isCloseEnough(Location location, Entity entity, int radius){
        if(location == null || radius <= 0)
            return true;

        return location.distanceSquared(entity.getLocation()) < (radius * radius);
    }

    @Override
    public void sendHelpMessage(Player player){
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " ";
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getUserFriendlyCommandText());
        sendCommandArgumentDetails(player);
        RpgMobsOutputHandler.PrintCommand(player, " -m" + RpgMobsOutputHandler.HIGHLIGHT + CommandConstants.DescriptionDelimeter + "DONT Remove Mobs (hostile)");
        RpgMobsOutputHandler.PrintCommand(player, " -a" + RpgMobsOutputHandler.HIGHLIGHT + CommandConstants.DescriptionDelimeter + "Remove Animals");
        RpgMobsOutputHandler.PrintCommand(player, " -g" + RpgMobsOutputHandler.HIGHLIGHT + CommandConstants.DescriptionDelimeter + "Remove Iron Golems");
        RpgMobsOutputHandler.PrintCommand(player, " -s" + RpgMobsOutputHandler.HIGHLIGHT + CommandConstants.DescriptionDelimeter + "Remove Armor Stands");
        RpgMobsOutputHandler.PrintCommand(player, " -b" + RpgMobsOutputHandler.HIGHLIGHT + CommandConstants.DescriptionDelimeter + "Remove Ambient creatures");
        RpgMobsOutputHandler.PrintCommand(player, " -v" + RpgMobsOutputHandler.HIGHLIGHT + CommandConstants.DescriptionDelimeter + "Remove Villagers");
        RpgMobsOutputHandler.PrintCommand(player, " -z" + RpgMobsOutputHandler.HIGHLIGHT + CommandConstants.DescriptionDelimeter + "DONT Remove Zombie Villagers");
        RpgMobsOutputHandler.PrintCommand(player, " -f" + RpgMobsOutputHandler.HIGHLIGHT + CommandConstants.DescriptionDelimeter + "Remove Flying");
    }
}
