package me.lorinth.rpgmobs.Command;

import me.lorinth.rpgmobs.Command.Loot.ListLootExecutor;
import me.lorinth.rpgmobs.Command.Loot.SearchLootExecutor;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.rpgmobs.Command.Loot.DropLootExecutor;
import me.lorinth.rpgmobs.Command.Loot.GiveLootExecutor;
import me.lorinth.rpgmobs.Command.Objects.CustomCommandExecutor;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class LootExecutor extends CustomCommandExecutor {

    private DropLootExecutor dropLootExecutor = new DropLootExecutor(this);
    private GiveLootExecutor giveLootExecutor = new GiveLootExecutor(this);
    private ListLootExecutor listLootExecutor = new ListLootExecutor(this);
    private SearchLootExecutor searchLootExecutor = new SearchLootExecutor(this);

    public LootExecutor() {
        super("loot", "provides access to loot based commands", null);
        RequiredArguments = 1;
    }

    @Override
    public void safeExecute(Player player, String[] args) {
        String cmd = args[0];
        String[] subArgs = Arrays.copyOfRange(args, 1, args.length);
        if(cmd.equalsIgnoreCase("drop")){
            dropLootExecutor.execute(player, subArgs);
        }
        else if(cmd.equalsIgnoreCase("give")){
            giveLootExecutor.execute(player, subArgs);
        }
        else if (cmd.equalsIgnoreCase("list")){
            listLootExecutor.execute(player, subArgs);
        }
        else if (cmd.equalsIgnoreCase("search")){
            searchLootExecutor.execute(player, subArgs);
        }

    }

    @Override
    public void sendHelpMessage(Player player) {
        RpgMobsOutputHandler.PrintWhiteSpace(player, 2);
        String prefix = "/" + CommandConstants.LorinthsRpgMobsCommand + " ";
        RpgMobsOutputHandler.PrintCommand(player, "[LorinthsRpgMobs] : " + RpgMobsOutputHandler.HIGHLIGHT + "Spawn Point Command List");
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getUserFriendlyCommandText());
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getCommandName() + " " + dropLootExecutor.getUserFriendlyCommandText());
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getCommandName() + " " + giveLootExecutor.getUserFriendlyCommandText());
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getCommandName() + " " + listLootExecutor.getUserFriendlyCommandText());
        RpgMobsOutputHandler.PrintCommand(player, prefix + this.getCommandName() + " " + searchLootExecutor.getUserFriendlyCommandText());

    }
}
