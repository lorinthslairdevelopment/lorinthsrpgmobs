package me.lorinth.rpgmobs.Util;

import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.Properties;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import java.util.List;

public class NameHelper {

    public static String getName(Entity entity){
        if(Properties.IsPersistentDataVersion){
            PersistentDataContainer dataContainer = entity.getPersistentDataContainer();
            String value = dataContainer.get(new NamespacedKey(LorinthsRpgMobs.instance, "name"), PersistentDataType.STRING);
            if(value != null){
                return value;
            }
        }
        else{
            List<MetadataValue> nameMetadata = entity.getMetadata(MetaDataConstants.Name);
            if(nameMetadata.size() > 0){
                return nameMetadata.get(0).asString();
            }
        }

        return null;
    }

    public static void setName(Entity entity, String name){
        if(name == null){
            name = "";
        }

        if(Properties.IsPersistentDataVersion){
            PersistentDataContainer dataContainer = entity.getPersistentDataContainer();
            NamespacedKey namespacedKey = new NamespacedKey(LorinthsRpgMobs.instance, "name");
            if(dataContainer.has(namespacedKey, PersistentDataType.STRING)){
                dataContainer.remove(namespacedKey);
            }
            dataContainer.set(namespacedKey, PersistentDataType.STRING, name);
        }
        else{
            entity.setMetadata(MetaDataConstants.Name, new FixedMetadataValue(LorinthsRpgMobs.instance, name));
        }
    }

    public static String fillNameDetails(LivingEntity entity, String baseName, String format){
        if(baseName == null){
            baseName = ToCamelCase(entity.getType().name());
        }

        Integer current = (int) Math.ceil(entity.getHealth());
        Integer max = (int) entity.getMaxHealth();
        return format
                .replace("{name}", baseName)
                .replace("{current}", current.toString())
                .replace("{max}", max.toString())
                .replace("{percent}", calculatePercentage(entity).toString());
    }

    private static Integer calculatePercentage(LivingEntity entity){
        return (int) (entity.getHealth() * 100 / entity.getMaxHealth());
    }

    public static String ToCamelCase(String s){
        String[] parts = s.split("_");
        String camelCaseString = "";
        for (String part : parts){
            camelCaseString = camelCaseString + toProperCase(part);
        }
        return camelCaseString;
    }

    static String toProperCase(String s) {
        return s.substring(0, 1).toUpperCase() +
                s.substring(1).toLowerCase();
    }

}
