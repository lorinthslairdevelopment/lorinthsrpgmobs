package me.lorinth.rpgmobs.Util;

public class MetaDataConstants {

    public static final String Level = "Lrm.Level";
    public static final String Variant = "Lrm.MobVariant";
    public static final String Damage = "Lrm.Damage";
    public static final String Name = "Lrm.Name";

}
