package me.lorinth.rpgmobs.Util;

import com.herocraftonline.heroes.util.Properties;
import com.sucy.skill.SkillAPI;
import me.lorinth.rpgmobs.Events.RpgItemsGetLevelEvent;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.robin.battlelevels.api.BattleLevelsAPI;
import net.Indyuce.mmocore.api.player.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.skills.api.SkillsAPI;

public class LevelHelper {

    public static int getPlayerLevel(Player player){
        switch(LorinthsRpgMobs.getLevelHook()) {
            case Vanilla:
                return player.getLevel();
            case RpgItems:
                RpgItemsGetLevelEvent event = new RpgItemsGetLevelEvent(player, 1);
                Bukkit.getPluginManager().callEvent(event);
                return event.getLevel();
            case Heroes:
                return com.herocraftonline.heroes.Heroes.getInstance().getCharacterManager().getHero(player).getHeroLevel();
            case BattleStats:
                return BattleLevelsAPI.getLevel(player.getUniqueId());
            case SkillAPI:
                return com.sucy.skill.SkillAPI.getPlayerData(player).getMainClass().getLevel();
            case MMOCore:
                return PlayerData.get(player).getLevel();
            case SkillsPro:
                return SkillsAPI.getSkilledPlayer(player.getUniqueId()).getLevel();

        }
        return 1;
    }

    public static double getExperiencePercent(Player player, int experience) {
        int level = getPlayerLevel(player);
        double totalExp;
        switch(LorinthsRpgMobs.getLevelHook()){
            case Vanilla:
                return getVanillaExperiencePercent(level, experience);
            case RpgItems:
                return getRpgItemsExperiencePercent(level, experience);
            case Heroes:
                totalExp = Properties.getTotalExp(level);
                return ((double) experience / totalExp) * 100.0;
            case BattleStats:
                totalExp = BattleLevelsAPI.getNeededFor(level);
                return ((double) experience / totalExp) * 100.0;
            case SkillAPI:
                totalExp = SkillAPI.getPlayerData(player).getMainClass().getData().getRequiredExp(level);
                return ((double) experience / totalExp) * 100.0;
            case MMOCore:
                totalExp = PlayerData.get(player).getLevelUpExperience();
                return ((double) experience / totalExp) * 100.0;
            case SkillsPro:
                totalExp = SkillsAPI.getSkilledPlayer(player.getUniqueId()).getLevelXP(level);
                return ((double) experience / totalExp) * 100.0;
        }

        return 1.0;
    }

    private static double getVanillaExperiencePercent(int level, int experience){
        int experienceNeeded = 0;
        if (level <= 15){
            experienceNeeded = 2 * level + 7;
        }
        else if (level <= 30){
            experienceNeeded = 5 * level - 38;
        }
        else {
            experienceNeeded = 9 * level - 158;
        }

        return ((double) experience / experienceNeeded) * 100.0;
    }

    private static double getRpgItemsExperiencePercent(int level, int experience){
        //int experienceNeeded = ExperienceManager.getExpToNextLevel(level);

        return 0.0;
    }

}
