package me.lorinth.rpgmobs.Util;

import me.lorinth.rpgmobs.Objects.DistanceAlgorithm;
import org.bukkit.Location;

public class DistanceHelper {

    /**
     * Calculate the distance to a spawnpoint from a location
     * @param loc The location we want to check
     * @param algorithm The algorithm to calculate distance
     * @return
     */
    public static int calculateDistance(Location a, Location loc, DistanceAlgorithm algorithm){
        loc.setY(0); // We only care about x/y

        if(algorithm == DistanceAlgorithm.Circle){
            return (int) (a.distance(loc));
        }
        else if(algorithm == DistanceAlgorithm.Diamond){
            return (int) (getSimpleDistance(a, loc));
        }
        else if(algorithm == DistanceAlgorithm.Square){
            return (int) (getSquareDistance(a, loc));
        }

        return 1;
    }

    /**
     *
     * @param a first location to compare distance
     * @param b second location to compare distance
     * @return the difference in x + z (results in diamond-esque shapes)
     */
    private static double getSimpleDistance(Location a, Location b){
        int x = Math.abs(a.getBlockX() - b.getBlockX());
        int z = Math.abs(a.getBlockZ() - b.getBlockZ());

        return x+z;
    }

    private static  double getSquareDistance(Location a, Location b){
        int x = Math.abs(a.getBlockX() - b.getBlockX());
        int z = Math.abs(a.getBlockZ() - b.getBlockZ());

        return Math.max(x,z);
    }

}
