package me.lorinth.rpgmobs.Objects;

import me.lorinth.rpgmobs.Objects.MythicDrops.MythicDropsData;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.Calculator;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.ChatColor;
import org.bukkit.attribute.Attribute;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Contains formulas, world data and name data for a creature type
 */
public class CreatureData extends DirtyObject {

    private String type;
    private EquipmentData equipmentData = new EquipmentData();
    private MythicDropsData mythicDropsData = new MythicDropsData();
    private TreeMap<Integer, RandomNullCollection<EntityType>> mounts = new TreeMap<>();
    private TreeMap<Integer, RandomNullCollection<String>> bosses = new TreeMap<>();
    private boolean entityDisabled = false;
    private boolean allowDespawn = false;

    //Formulas
    private String damageFormula;
    private String expFormula;
    private String healthFormula;
    private String movementSpeedFormula;
    private String currencyChanceFormula;
    private String currencyValueFormula;

    private ArrayList<NameData> nameData = new ArrayList<>();
    private ArrayList<String> entityDisabledWorlds = new ArrayList<String>();

    /**
     * Creates creature data based on saved config data
     * @param type - Type of entity toString()
     * @param config - config file
     */
    public CreatureData(String type, FileConfiguration config) {
        this.type = type;
        load(config);
    }

    /**
     * Creates new creature data based on a creature entity
     * @param entity - creature to base data on
     */
    public CreatureData(LivingEntity entity){
        this(entity, entity.getType().name());
    }

    public CreatureData(LivingEntity entity, String type){
        this.type = type;
        initBasedOnNewEntity(entity);
    }

    private void initBasedOnNewEntity(LivingEntity entity){
        if(Properties.IsAttributeVersion){
            healthFormula = (entity.getAttribute(Attribute.GENERIC_MAX_HEALTH) != null ? (int) entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue() : 20) + " + ({level} / 3) + ({level} / 5) + rand(5)";
            damageFormula = (entity.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE) != null ? (int) entity.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).getValue() : 2) + " + rand(3) + ({level} / 12)";

            double currentSpeed = Math.round(entity.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).getValue() * 100) / 100.0;
            movementSpeedFormula = currentSpeed + " + ({level} * 0.003)";
        }
        else{
            healthFormula = entity.getMaxHealth() + " + ({level} / 3) + ({level} / 5) + rand(5)";
            damageFormula = "1 + rand(3) + ({level} / 12)";
            movementSpeedFormula = null;
        }
        expFormula = "rand(5) + 1 + rand({level} / 5)";
        currencyChanceFormula = "5.0 + ({level} / 5)";
        currencyValueFormula = "3.0 + ({level} / 3) + rand(3)";

        String friendlyName = getUserFriendlyName(type);
        nameData.add(new NameData(1, friendlyName, false));
        nameData.add(new NameData(40, "Epic " + friendlyName, false));

        allowDespawn = entity instanceof Monster;

        this.setNew();
    }

    private String getUserFriendlyName(String name){
        String friendlyTypeName = "";
        String[] split = name.split("_");

        for(String word : split){
            friendlyTypeName += word.substring(0,1).toUpperCase() + word.substring(1).toLowerCase() + " ";
        }
        return friendlyTypeName.trim();
    }

    public boolean shouldRemoveWhenFarAway(){
        return allowDespawn;
    }

    /**
     * Saves all data associated with this object
     * @param config - config file to save to
     * @param prefix - the path prefix we will use
     */
    protected void saveData(FileConfiguration config, String prefix){
        config.set("Disabled", entityDisabled);
        config.set("AllowDespawn", allowDespawn);
        saveDisabledWorlds(config);
        saveFormulas(config);
        saveNames(config);
    }

    /**
     * Saves the disabledWorlds
     * @param config - config file to save to
     */
    private void saveDisabledWorlds(FileConfiguration config){
        config.set("DisabledWorlds", entityDisabledWorlds);
    }

    /**
     * Saves the formulas associated with this object
     * @param config - config file to save to
     */
    private void saveFormulas(FileConfiguration config){
        //Backwards compatibility updates
        config.set("Health", null);
        config.set("Damage", null);
        config.set("Experience", null);
        config.set("Exp", null);

        String prefix = "Formulas.";
        config.set(prefix + "Health", healthFormula);
        config.set(prefix + "Damage", damageFormula);
        config.set(prefix + "Experience", expFormula);
        config.set(prefix + "CurrencyChance", currencyChanceFormula);
        config.set(prefix + "CurrencyValue", currencyValueFormula);
        config.set(prefix + "MovementSpeed", movementSpeedFormula);
    }

    /**
     * Saves the names associated with this object
     * @param config - config file to save to
     */
    private void saveNames(FileConfiguration config){
        for(NameData data : nameData){
            data.save(config);
        }
    }

    /**
     * Loads the data from config file
     * @param config - config file to load from
     */
    private void load(FileConfiguration config){
        entityDisabled = config.getBoolean("Disabled");
        if(entityDisabled)
            return; //We don't care about loading disabled entities

        if(ConfigHelper.ConfigContainsPath(config, "AllowDespawn")){
            allowDespawn = config.getBoolean("AllowDespawn");
        } else{
            allowDespawn = true;
        }
        loadDisabledWorlds(config);
        loadFormulas(config);
        loadNames(config);
        loadEquipmentData(config);
        loadMythicDropsData(config);
        loadBossData(config);
        loadMountData(config);
    }

    /**
     * Loads the formulas associated with this object from config file
     * @param config - config file to load from.
     */
    private void loadFormulas(FileConfiguration config){
        //Backwards Compatibility
        String prefix = "Formulas.";
        if(ConfigHelper.ConfigContainsPath(config, prefix + "Health")){
            healthFormula = config.getString(prefix + "Health");
            damageFormula = config.getString(prefix + "Damage");
            if (ConfigHelper.ConfigContainsPath(config, prefix + "MovementSpeed")){
                movementSpeedFormula = config.getString(prefix + "MovementSpeed");
            }

            if(ConfigHelper.ConfigContainsPath(config, prefix + "Exp")){
                expFormula = config.getString(prefix + "Exp");
            }
            else {
                expFormula = config.getString(prefix + "Experience");
            }

            if(ConfigHelper.ConfigContainsPath(config, prefix + "CurrencyChance")) {
                currencyChanceFormula = config.getString(prefix + "CurrencyChance");
            }
            else{
                currencyChanceFormula = "10.0 + ({level} / 5)";
                this.setDirty();
            }

            if(ConfigHelper.ConfigContainsPath(config, prefix + "CurrencyValue")) {
                currencyValueFormula = config.getString(prefix + "CurrencyValue");
            }
            else{
                currencyValueFormula = "3.0 + ({level} / 3) + rand(3)";
                this.setDirty();
            }
        }
        else{
            healthFormula = config.getString("Health");
            damageFormula = config.getString("Damage");
            movementSpeedFormula = config.getString("MovementSpeed");

            if(ConfigHelper.ConfigContainsPath(config, "Exp"))
                expFormula = config.getString("Exp");
            else
                expFormula = config.getString("Experience");

            if(ConfigHelper.ConfigContainsPath(config, "CurrencyChance"))
                currencyChanceFormula = config.getString("CurrencyChance");
            else{
                currencyChanceFormula = "10.0 + ({level} / 5)";
                this.setDirty();
            }

            if(ConfigHelper.ConfigContainsPath(config, "CurrencyValue"))
                currencyValueFormula = config.getString("CurrencyValue");
            else {
                currencyValueFormula = "5.0 + ({level} / 3) + rand(3)";
                this.setDirty();
            }
        }
    }

    /**
     * Loads the names associated with this object from config file
     * @param config - config file to load from
     */
    private void loadNames(FileConfiguration config){
        if(ConfigHelper.ConfigContainsPath(config, "Names")){
            for(String key : config.getConfigurationSection("Names").getKeys(false)){
                try{
                    int level = Integer.parseInt(key);
                    String name = "";
                    boolean overrideFormat = false;

                    //Backwards support for Names
                    if(config.get("Names." + key) != null) {
                        name = ChatColor.translateAlternateColorCodes('&', config.getString("Names." + key));
                    }
                    if(config.get("Names." + key + ".Name") != null) {
                        name = ChatColor.translateAlternateColorCodes('&', config.getString("Names." + key + ".Name"));
                    }

                    //Check for OverrideFormat
                    if(config.contains("Names." + key + ".OverrideFormat"))
                        overrideFormat = config.getBoolean("Names." + key + ".OverrideFormat");

                    nameData.add(new NameData(level, name, overrideFormat));
                }
                catch(Exception exception){
                    RpgMobsOutputHandler.PrintException("Unable to part key, '" + key + "' as a level/int.", exception);
                }
            }
        }
        else{
            RpgMobsOutputHandler.PrintInfo("No names for entity, " + type);
        }
    }

    private void loadEquipmentData(FileConfiguration config){
        equipmentData.loadData(config);
    }

    private void loadMythicDropsData(FileConfiguration config){
        mythicDropsData.loadData(config);
    }

    /**
     * Loads the disabled worlds associated with this object from config file
     * @param config - config file to load from
     */
    private void loadDisabledWorlds(FileConfiguration config){
        if(config.contains("DisabledWorlds"))
            entityDisabledWorlds.addAll(config.getStringList("DisabledWorlds"));
    }

    private void loadBossData(FileConfiguration config) {
        bosses = new TreeMap<>();
        String prefix = "Bosses";
        if(ConfigHelper.ConfigContainsPath(config, prefix)){
            for(String levelString : config.getConfigurationSection(prefix).getKeys(false)){
                if(!TryParse.parseInt(levelString)){
                    RpgMobsOutputHandler.PrintError("Cannot parse value, " + RpgMobsOutputHandler.HIGHLIGHT + levelString +
                            RpgMobsOutputHandler.ERROR + " as integer, in path " + RpgMobsOutputHandler.HIGHLIGHT + prefix);
                    continue;
                }

                int level = Integer.parseInt(levelString);
                RandomNullCollection<String> bossChances = new RandomNullCollection<>();

                for(String bossName : config.getConfigurationSection(prefix + "." + levelString).getKeys(false)){
                    Double value = config.getDouble(prefix + "." + levelString + "." + bossName);
                    bossChances.add(value, bossName);
                }

                bosses.put(level, bossChances);
            }
        }
    }

    private void loadMountData(FileConfiguration config) {
        mounts = new TreeMap<>();
        String prefix = "Mounts";
        if(ConfigHelper.ConfigContainsPath(config, prefix)){
            for(String levelString : config.getConfigurationSection(prefix).getKeys(false)){
                if(!TryParse.parseInt(levelString)){
                    RpgMobsOutputHandler.PrintError("Cannot parse value, " + RpgMobsOutputHandler.HIGHLIGHT + levelString +
                            RpgMobsOutputHandler.ERROR + " as integer, in path " + RpgMobsOutputHandler.HIGHLIGHT + prefix);
                    continue;
                }

                int level = Integer.parseInt(levelString);
                RandomNullCollection<EntityType> mountChances = new RandomNullCollection<>();

                for(String mountType : config.getConfigurationSection(prefix + "." + levelString).getKeys(false)){
                    Double value = config.getDouble(prefix + "." + levelString + "." + mountType);
                    if(!TryParse.parseEnum(EntityType.class, mountType)){
                        RpgMobsOutputHandler.PrintError("Cannot parse value, " + RpgMobsOutputHandler.HIGHLIGHT + mountType +
                                RpgMobsOutputHandler.ERROR + " as entity type, in path " + RpgMobsOutputHandler.HIGHLIGHT + prefix + "." + levelString);
                        continue;
                    }
                    mountChances.add(value, EntityType.valueOf(mountType));
                }

                mounts.put(level, mountChances);
            }
        }
    }

    /**
     * Get the health formula assigned to this object
     * @return health formula
     */
    public String getHealthFormula(){
        return healthFormula;
    }

    /**
     * Set the health formula assigned to this object
     * @param formula - the new formula used for calculating health
     */
    public void setHealthFormula(String formula){
        if(formula == null){
            return;
        }

        healthFormula = formula;
        setDirty();
    }

    /**
     * Get the damage formula assigned to this object
     * @return damage formula
     */
    public String getDamagerFormula(){
        return damageFormula;
    }

    /**
     * Set the damage formula assigned to this object
     * @param formula - the new formula used for calculating damage
     */
    public void setDamagerFormula(String formula){
        if(formula == null){
            return;
        }

        damageFormula = formula;
        setDirty();
    }

    public String getMovementSpeedFormula(){
        return movementSpeedFormula;
    }

    public void setMovementSpeedFormula(String formula){
        if(formula == null){
            return;
        }

        movementSpeedFormula = formula;
        setDirty();
    }

    /**
     * Get the exp formula assigned to this object
     * @return exp formula
     */
    public String getExpFormula(){
        return expFormula;
    }

    public MythicDropsData getMythicDropsData(){ return mythicDropsData; }

    /**
     * Set the exp formula assigned to this object
     * @param formula - the new formula used for calculating exp
     */
    public void setExpFormula(String formula){
        if(formula == null){
            return;
        }

        expFormula = formula;
        setDirty();
    }

    /**
     * Gets the name for a creature at a given level
     * @param level - level to check
     * @param regionNameData - regionNameData if applicable
     * @return - name that will be applied
     */
    public String getNameAtLevel(NameData regionNameData, int level){
        if(regionNameData != null)
            return regionNameData.getName(level);

        NameData highest = null;
        for(NameData data : nameData){
            if((highest == null && data.getLevel() <= level ) || (highest != null && data.getLevel() <= level && data.getLevel() >= highest.getLevel())){
                highest = data;
            }
        }

        if(highest != null)
            return highest.getName(level);
        return null;
    }

    public EquipmentData getEquipmentData(){
        return equipmentData;
    }

    public RandomNullCollection<String> getBossChances(int level){
        if(bosses == null){
            return null;
        }
        Map.Entry<Integer, RandomNullCollection<String>> entry = bosses.floorEntry(level);
        if(entry != null){
            return entry.getValue();
        }
        return null;
    }

    public RandomNullCollection<EntityType> getMountType(int level){
        if(mounts == null){
            return null;
        }
        Map.Entry<Integer, RandomNullCollection<EntityType>> entry = mounts.floorEntry(level);
        if(entry != null){
            return entry.getValue();
        }
        return null;
    }

    /**
     * Get the health this data would give at the given level
     * @param level - level used in the health formula
     * @return - health the entity should have
     */
    public double getHealthAtLevel(Integer level){
        try{
            return (int) Calculator.eval(Properties.FormulaMethod, preParseFormula(healthFormula, level));
        }
        catch(Exception exception){
            RpgMobsOutputHandler.PrintRawError("Got Health Error for, " + type);
            RpgMobsOutputHandler.PrintException("Level : " + level + ", Formula : " + healthFormula, exception);
        }
        return 1;
    }

    /**
     * Get the damage this data would give at the given level
     * @param level - level used in the damage formula
     * @return - damage the entity should do
     */
    public double getDamageAtLevel(Integer level){
        try{
            return (int) Calculator.eval(Properties.FormulaMethod, preParseFormula(damageFormula, level));
        }
        catch(Exception exception){
            RpgMobsOutputHandler.PrintRawError("Got Damage Formula Error for, " + type);
            RpgMobsOutputHandler.PrintException("Level : " + level + ", Formula : " + damageFormula, exception);
        }
        return 1;
    }

    /**
     * Gets the movement speed this data would give at the given level
     * @param level - level used in the movespeed formula
     * @return - movement speed the entity should have
     */
    public double getMovementSpeedAtLevel(Integer level){
        try{
            return Calculator.eval(Properties.FormulaMethod, preParseFormula(movementSpeedFormula, level));
        }
        catch(Exception exception){
            RpgMobsOutputHandler.PrintRawError("Got Movement Speed Formula Error for, " + type);
            RpgMobsOutputHandler.PrintException("Level : " + level + ", Formula : " + movementSpeedFormula, exception);
        }
        return 1;
    }

    /**
     * Get the exp this data would give at the given level
     * @param level - level used in the exp formula
     * @return - exp the entity should give
     */
    public int getExperienceAtLevel(Integer level){
        try{
            return (int) Calculator.eval(Properties.FormulaMethod, preParseFormula(expFormula, level));
        }
        catch(Exception exception){
            RpgMobsOutputHandler.PrintRawError("Got Experience Error for, " + type);
            RpgMobsOutputHandler.PrintException("Level : " + level + ", Formula : " + expFormula, exception);
        }
        return 1;
    }

    public double getCurrencyChanceAtLevel(Integer level){
        try{
            return (int) Calculator.eval(Properties.FormulaMethod, preParseFormula(currencyChanceFormula, level));
        }
        catch(Exception exception){
            RpgMobsOutputHandler.PrintRawError("Got Currency Chance Formula Error for, " + type);
            RpgMobsOutputHandler.PrintException("Level : " + level + ", Formula : " + currencyChanceFormula, exception);
        }
        return 1;
    }

    public double getCurrencyValueAtLevel(Integer level){
        try{
            return (int) Calculator.eval(Properties.FormulaMethod, preParseFormula(currencyValueFormula, level));
        }
        catch(Exception exception){
            RpgMobsOutputHandler.PrintRawError("Got Currency Value Formula Error for, " + type);
            RpgMobsOutputHandler.PrintException("Level : " + level + ", Formula : " + currencyValueFormula, exception);
        }
        return 1;
    }

    /**
     * Converts the string formula to use provided values such as {level}
     * @param formulaBefore - base formula
     * @param level - level to replace {level}
     * @return the formula we can evaluate
     */
    private String preParseFormula(String formulaBefore, Integer level){
        return formulaBefore
                .replace("{level}", level.toString());
    }

    /**
     * Are leveling features disabled for this entity type
     * @return - isDisabled
     */
    public boolean isDisabled(String worldName){
        return entityDisabled || entityDisabledWorlds.contains(worldName);
    }
}
