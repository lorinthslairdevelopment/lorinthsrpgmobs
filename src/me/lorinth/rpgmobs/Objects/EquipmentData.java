package me.lorinth.rpgmobs.Objects;

import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class EquipmentData{

    private HashMap<Integer, EquipmentDetail> WeaponLevels = new HashMap<>();
    private HashMap<Integer, EquipmentDetail> OffHandLevels = new HashMap<>();
    private HashMap<Integer, EquipmentDetail> HelmetLevels = new HashMap<>();
    private HashMap<Integer, EquipmentDetail> ChestLevels = new HashMap<>();
    private HashMap<Integer, EquipmentDetail> LegLevels = new HashMap<>();
    private HashMap<Integer, EquipmentDetail> BootLevels = new HashMap<>();

    public void loadData(FileConfiguration config){
        String prefix = "Equipment";

        loadLevels(config, prefix + ".Weapon", WeaponLevels);
        loadLevels(config, prefix + ".Offhand", OffHandLevels);
        loadLevels(config, prefix + ".Helmet", HelmetLevels);
        loadLevels(config, prefix + ".Chestplate", ChestLevels);
        loadLevels(config, prefix + ".Leggings", LegLevels);
        loadLevels(config, prefix + ".Boots", BootLevels);
    }

    private void loadLevels(FileConfiguration config, String prefix, HashMap<Integer, EquipmentDetail> data){
        if(config.contains(prefix)){
            for(String key : config.getConfigurationSection(prefix).getKeys(false)){
                try{
                    Integer level = Integer.parseInt(key);
                    data.put(level, new EquipmentDetail(config, prefix + "." + key));
                }
                catch(Exception exception){
                    RpgMobsOutputHandler.PrintException("Failed to load equipment level, " + key, exception);
                }
            }
        }
    }

    public void equip(LivingEntity creature, int level){
        EntityEquipment equipment = creature.getEquipment();
        if(equipment == null){
            return;
        }

        creature.setCanPickupItems(false);
        if(Properties.IsAttributeVersion){
            //Main Hand
            if(Properties.ForceRpgMobsEquipment){
                EquipmentResult mainHand = getHighest(level, WeaponLevels);
                if(mainHand != null) {
                    equipment.setItemInMainHand(mainHand.getItem());
                    equipment.setItemInMainHandDropChance((float) mainHand.getDropChance() / 100.0f);
                }
            }
            else if (!(doesVanillaItemOverride(equipment.getItemInMainHand()))){
                EquipmentResult mainHand = getHighest(level, WeaponLevels);
                if(mainHand != null && !mainHand.isEmpty) {
                    equipment.setItemInMainHand(mainHand.getItem());
                    equipment.setItemInMainHandDropChance((float) mainHand.getDropChance() / 100.0f);
                }
            }

            //Off Hand

            if(Properties.ForceRpgMobsEquipment){
                EquipmentResult offHand = getHighest(level, OffHandLevels);
                if(offHand != null) {
                    equipment.setItemInOffHand(offHand.getItem());
                    equipment.setItemInOffHandDropChance((float) offHand.getDropChance() / 100.0f);
                }
            }
            else if (!(doesVanillaItemOverride(equipment.getItemInOffHand()))){
                EquipmentResult offHand = getHighest(level, OffHandLevels);
                if(offHand != null && !offHand.isEmpty) {
                    equipment.setItemInOffHand(offHand.getItem());
                    equipment.setItemInOffHandDropChance((float) offHand.getDropChance() / 100.0f);
                }
            }
        }
        else{
            if(Properties.ForceRpgMobsEquipment){
                EquipmentResult mainHand = getHighest(level, WeaponLevels);
                if(mainHand != null) {
                    equipment.setItemInHand(mainHand.getItem());
                    equipment.setItemInHandDropChance((float) mainHand.getDropChance() / 100.0f);
                }
            }
            else if(!(doesVanillaItemOverride(equipment.getItemInHand()))){
                EquipmentResult mainHand = getHighest(level, WeaponLevels);
                if(mainHand != null && !mainHand.isEmpty) {
                    equipment.setItemInHand(mainHand.getItem());
                    equipment.setItemInHandDropChance((float) mainHand.getDropChance() / 100.0f);
                }
            }
        }


        //Helmet
        EquipmentResult helmet = getHighest(level, HelmetLevels);
        if(Properties.ForceRpgMobsEquipment){
            if(helmet != null) {
                equipment.setHelmet(helmet.getItem());
                equipment.setHelmetDropChance((float) helmet.getDropChance() / 100.0f);
            }
        }
        else if (!(doesVanillaItemOverride(equipment.getHelmet()))){
        	if(helmet != null && !helmet.isEmpty) {
                equipment.setHelmet(helmet.getItem());
                equipment.setHelmetDropChance((float) helmet.getDropChance() / 100.0f);
            }
        }

        //Chest
        EquipmentResult chest = getHighest(level, ChestLevels);
        if(Properties.ForceRpgMobsEquipment){
            if(chest != null) {
                equipment.setChestplate(chest.getItem());
                equipment.setChestplateDropChance((float) chest.getDropChance() / 100.0f);
            }
        }
        else if (!(doesVanillaItemOverride(equipment.getChestplate()))){
            if(chest != null && !chest.isEmpty) {
                equipment.setChestplate(chest.getItem());
                equipment.setChestplateDropChance((float) chest.getDropChance() / 100.0f);
            }

        }

        //Legs
        EquipmentResult legs = getHighest(level, LegLevels);
        if(Properties.ForceRpgMobsEquipment){
            if(legs != null) {
                equipment.setLeggings(legs.getItem());
                equipment.setLeggingsDropChance((float) legs.getDropChance() / 100.0f);
            }
        }
        else if (!(doesVanillaItemOverride(equipment.getLeggings()))){
            if(legs != null && !legs.isEmpty) {
                equipment.setLeggings(legs.getItem());
                equipment.setLeggingsDropChance((float) legs.getDropChance() / 100.0f);
            }
        }

        //Boots
        EquipmentResult boots = getHighest(level, BootLevels);
        if(Properties.ForceRpgMobsEquipment){
            if(boots != null) {
                equipment.setBoots(boots.getItem());
                equipment.setBootsDropChance((float) boots.getDropChance() / 100.0f);
            }
        }
        else if (!(doesVanillaItemOverride(equipment.getBoots()))){
            if(boots != null && !boots.isEmpty) {
                equipment.setBoots(boots.getItem());
                equipment.setBootsDropChance((float) boots.getDropChance() / 100.0f);
            }
        }
    }

    private boolean doesVanillaItemOverride(ItemStack item){
        return Properties.VanillaMobEquipmentOverrides && item != null && item.getType() != Material.AIR;
    }

    private EquipmentResult getHighest(int level, HashMap<Integer, EquipmentDetail> equipLevels){
        int highest = 0;
        for(Integer key : equipLevels.keySet()){
            if(key >= highest && key <= level)
                highest = key;
        }

        if(highest > 0){
            return equipLevels.get(highest).getItem();
        }
        return null;
    }
}
