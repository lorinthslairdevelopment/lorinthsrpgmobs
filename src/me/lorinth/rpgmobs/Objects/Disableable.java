package me.lorinth.rpgmobs.Objects;

public class Disableable {

    private boolean isDisabled;

    public void setEnabled(boolean enabled){
        isDisabled = !enabled;
    }

    public void setDisabled(boolean disabled){
        isDisabled = disabled;
    }

    public boolean isEnabled(){
        return !isDisabled;
    }

    public boolean  isDisabled(){
        return isDisabled;
    }

}
