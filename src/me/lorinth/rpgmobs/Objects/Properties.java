package me.lorinth.rpgmobs.Objects;

import me.lorinth.utils.FormulaMethod;

public class Properties {

    public static boolean NameTagsAlwaysOn;
    public static String NameFormat;
    public static boolean VanillaMobEquipmentOverrides;
    public static boolean ForceRpgMobsEquipment;
    public static boolean IsMessageTypeVersion; //10
    public static boolean IsAttributeVersion; //9
    public static boolean IsPersistentDataVersion; //14
    public static boolean IsLootableVersion; //13
    public static boolean IsAbstractHorseVersion; //11
    public static boolean IsAddPassengerVersion; //12
    public static FormulaMethod FormulaMethod;
    public static int SubVersion;

}
