package me.lorinth.rpgmobs.Objects;

import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Util.NameHelper;
import org.bukkit.entity.Entity;

public class MobInstance {

    private int level;
    private String name;

    public MobInstance(Entity entity){
        level = LorinthsRpgMobs.GetLevelOfEntity(entity);
        name = NameHelper.getName(entity);
    }

    public int getLevel(){
        return level;
    }

    public String getName(){
        return name;
    }

}
