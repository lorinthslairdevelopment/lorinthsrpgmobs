package me.lorinth.rpgmobs.Objects.Loot;

import me.lorinth.rpgmobs.Objects.MythicMobs.DropHelper;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;

public class DropSection {
  private HashMap<ItemStack, Double> drops = new HashMap<ItemStack, Double>();
  private HashMap<String, Double> mythicMobsTable = new HashMap<>();
  private Random random = new Random();

  public void addItem(ItemStack item, double chance) {
    if(chance <= 0.0d){
      return;
    }

    drops.put(item, chance);
  }

  public void addMythicMobsTable(String tableName, double chance){
    if(chance <= 0.0d){
      return;
    }

    mythicMobsTable.put(tableName, chance);
  }

  public Collection<ItemStack> getDrops() {
    ArrayList<ItemStack> drops = new ArrayList<>();
    double roll = random.nextDouble() * 100.0;
    for (Entry<ItemStack, Double> entry : this.drops.entrySet()) {
      roll -= entry.getValue();
      if (roll <= 0) {
        drops.add(entry.getKey());
        return drops;
      }
    }
    for (Entry<String, Double> entry : mythicMobsTable.entrySet()){
      roll -= entry.getValue();
      if (roll <= 0)
      {
        drops.addAll(DropHelper.getLootFromMythicMobsTable(entry.getKey()));
        return drops;
      }
    }
    return drops;
  }
}
