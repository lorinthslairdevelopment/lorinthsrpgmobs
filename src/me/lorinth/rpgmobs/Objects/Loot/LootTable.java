package me.lorinth.rpgmobs.Objects.Loot;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class LootTable {

  private HashMap<String, DropSection> dropSections = new HashMap<>();
  private String name;

  public LootTable(String name) {
    this.name = name;
  }

  public void addSection(String name, ItemStack item, double chance) {
    if(chance <= 0.0d){
      return;
    }

    if (dropSections.get(name) == null) {
      DropSection dropSection = new DropSection();
      dropSections.put(name, dropSection);
    }
    dropSections.get(name).addItem(item, chance);
  }

  public void addSection(String name, String tableName, double chance) {
    if(chance <= 0.0d){
      return;
    }

    if (dropSections.get(name) == null) {
      DropSection dropSection = new DropSection();
      dropSections.put(name, dropSection);
    }
    dropSections.get(name).addMythicMobsTable(tableName, chance);
  }

  public List<ItemStack> generateLoot(){
    ArrayList<ItemStack> items = new ArrayList<>();
    for(DropSection dropSection : dropSections.values()){
      Collection<ItemStack> drops = dropSection.getDrops();
      for(ItemStack drop : drops){
        if (drop != null && drop.getType() != Material.AIR){
          items.add(drop);
        }
      }
    }
    return items;
  }
}
