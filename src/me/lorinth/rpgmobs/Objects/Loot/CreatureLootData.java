package me.lorinth.rpgmobs.Objects.Loot;

import me.lorinth.rpgmobs.Data.LootManager;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class CreatureLootData {

    private TreeMap<Integer, LootTable> levelLootData = new TreeMap<>();
    private TreeMap<Integer, HashMap<String, Double>> commandRewards = new TreeMap<>();
    private List<ItemStack> emptyItemList = new ArrayList<>();
    private List<String> emptyCommandList = new ArrayList<>();
    private TreeMap<Integer, Integer> vanillaMultipliers = new TreeMap<>();
    private Random random = new Random();

    public CreatureLootData(FileConfiguration config, String path){
        for(String levelValue : config.getConfigurationSection(path).getKeys(false)){
            if(!TryParse.parseInt(levelValue)){
                RpgMobsOutputHandler.PrintError("Invalid level, " + RpgMobsOutputHandler.HIGHLIGHT + levelValue + RpgMobsOutputHandler.ERROR + ", in loot.yml  at " + path);
                continue;
            }

            Integer level = Integer.parseInt(levelValue);
            if(ConfigHelper.ConfigContainsPath(config, path + "." + levelValue + ".VanillaMultiplier")){
                vanillaMultipliers.put(level, config.getInt(path + "." + levelValue + ".VanillaMultiplier"));
            }
            if(ConfigHelper.ConfigContainsPath(config, path + "." + levelValue + ".Commands")){
                loadCommandRewards(config, path + "." + levelValue + ".Commands", level);
            }

            loadLootTables(config, path, level);
        }
    }

    private void loadCommandRewards(FileConfiguration config, String path, Integer level) {
        HashMap<String, Double> commandRewardChances = new HashMap<>();
        for(String command : config.getConfigurationSection(path).getKeys(false)){
            commandRewardChances.put(command, config.getDouble(path + "." + command));
        }
        commandRewards.put(level, commandRewardChances);
    }

    private void loadLootTables(FileConfiguration config, String path, Integer level){
        LootTable table = new LootTable(path + "." + level.toString());
        for(String category : config.getConfigurationSection(path + "." + level.toString()).getKeys(false)){
            if(category.equalsIgnoreCase("VanillaMultiplier") ||
                    category.equalsIgnoreCase("Commands")){
                continue;
            }

            for(String itemId : config.getConfigurationSection(path + "." + level.toString() + "." + category).getKeys(false)){
                double dropChance = config.getDouble(path + "." + level.toString() + "." + category + "." + itemId);
                if(dropChance <= 0.0d){
                    continue;
                }

                //vanilla items
                if(TryParse.parseEnum(Material.class, itemId)){
                    table.addSection(category, new ItemStack(Material.valueOf(itemId)), dropChance);
                }
                //custom loot items via loot.yml
                else{
                    ItemStack itemStack = LootManager.getItemById(itemId);
                    if(itemStack != null) {
                        table.addSection(category, itemStack, dropChance);
                    }
                    else{
                        table.addSection(category, itemId, dropChance);
                    }
                }
            }
        }
        levelLootData.put(level, table);
    }

    public List<ItemStack> getLootByLevel(Integer level){
        if(level == null || levelLootData.size() == 0)
            return emptyItemList;

        Map.Entry<Integer, LootTable> lootTableEntry = levelLootData.floorEntry(level);
        if(lootTableEntry == null)
            return emptyItemList;
        return levelLootData.floorEntry(level).getValue().generateLoot();
    }

    public List<String> getCommandRewardsByLevel(Integer level){
        if(level == null || levelLootData.size() == 0)
            return emptyCommandList;

        Map.Entry<Integer, HashMap<String, Double>> possibleCommandRewards = commandRewards.floorEntry(level);
        if(possibleCommandRewards == null)
            return emptyCommandList;

        HashMap<String, Double> commandRewardChances = possibleCommandRewards.getValue();
        List<String> commands = new ArrayList<>();
        for(Map.Entry<String, Double> commandReward : commandRewardChances.entrySet()){
            if(random.nextDouble() * 100.0 < commandReward.getValue()){
                commands.add(commandReward.getKey());
            }
        }

        return commands;
    }

    public Integer getVanillaMultiplier(Integer level){
        if(level == null || vanillaMultipliers.size() == 0){
            return 1;
        }

        Map.Entry<Integer, Integer> entry = vanillaMultipliers.floorEntry(level);
        if(entry == null){
            return 1;
        }

        return entry.getValue();
    }

}
