package me.lorinth.rpgmobs.Objects;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class DamageIndicatorConfig {

    private static DecimalFormat decimalFormat = new DecimalFormat("#.#");
    private boolean useHologram = false;
    private List<String> hologramLines = new ArrayList<>();
    private double hologramRadius = 10.0;
    private double height = 1.0;

    public DamageIndicatorConfig(){}

    public DamageIndicatorConfig(FileConfiguration config, String prefix){
        useHologram = config.getBoolean(prefix + ".UseHologram");
        hologramLines = config.getStringList(prefix + ".HologramLines");
        hologramRadius = config.getDouble(prefix + ".HologramRadius");
        height = config.getDouble(prefix + ".Height");
    }

    public boolean isUseHologram() {
        return useHologram;
    }

    public List<String> getHologramLines() {
        return hologramLines;
    }

    public double getHologramRadius() {
        return hologramRadius;
    }

    public double getHeight() {
        return height;
    }

    public List<String> getLines(Double value){
        List<String> output = new ArrayList<>();

        for (String line : hologramLines){
            output.add(ChatColor.translateAlternateColorCodes('&',
                    line.replaceAll(Pattern.quote("{value}"), decimalFormat.format(value))));
        }

        return output;
    }
}
