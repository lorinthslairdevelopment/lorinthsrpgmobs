package me.lorinth.rpgmobs.Objects.Placeholder;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import me.lorinth.rpgmobs.Data.DataLoader;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.LevelRegion;
import me.lorinth.rpgmobs.Objects.SpawnPoint;
import org.bukkit.entity.Player;

public class RpgMobsPlaceholder extends PlaceholderExpansion {
    @Override
    public String getIdentifier() {
        return "rpgmobs";
    }

    @Override
    public String getAuthor() {
        return "Lorinthio";
    }

    @Override
    public String getVersion() {
        return "1.0.0";
    }

    @Override
    public String onPlaceholderRequest(Player p, String identifier) {
        if (identifier.equalsIgnoreCase("level")){
            SpawnPoint spawnPoint = LorinthsRpgMobs.GetSpawnPointManager().getSpawnPointForLocation(p.getLocation());
            LevelRegion levelRegion =
                    LorinthsRpgMobs.GetLevelRegionManager().getHighestPriorityLeveledRegionAtLocation(p.getLocation());
            if (spawnPoint == null && levelRegion == null){
                return "";
            }

            if (levelRegion != null){
                if (levelRegion.getMinLevel() == levelRegion.getMaxLevel()){
                    return levelRegion.getMinLevel() + "";
                }
                else {
                    return levelRegion.getLevelRange();
                }
            }
            else {
                int level = spawnPoint.calculateRawLevel(p.getLocation(), DataLoader.getDistanceAlgorithm());
                if (spawnPoint.getVariance() > 0){
                    return level + "-" + (level + spawnPoint.getVariance());
                }
                else {
                    return level + "";
                }
            }


        }

        return "";
    }
}
