package me.lorinth.rpgmobs.Objects;

import com.sucy.enchant.api.CustomEnchantment;
import me.lorinth.rpgmobs.Data.LootManager;
import me.lorinth.rpgmobs.Util.RpgMobsOutputHandler;
import me.lorinth.utils.EnchantmentHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import su.nightexpress.goldenenchants.manager.EnchantManager;
import su.nightexpress.goldenenchants.manager.enchants.GoldenEnchant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class EquipmentDetail {

    ArrayList<ItemStack> ItemStacks = new ArrayList<ItemStack>();
    ArrayList<Double> Chances = new ArrayList<>();
    ArrayList<Double> DropChances = new ArrayList<>();
    private Random random = new Random();
    private EquipmentResult empty = new EquipmentResult(new ItemStack(Material.AIR), 0, true);

    public EquipmentDetail(FileConfiguration config, String path){
        String line = config.getString(path);
        String[] items = line.split(",");
        for(String item : items){
            item = item.trim();
            String[] details = item.split(" ");
            Double chance = 100d;
            Double dropChance = 0d;
            ItemStack itemStack = null;
            Map<String, Integer> enchants = new HashMap<>();

            for(String detail : details){
                detail = detail.replace("%", "").trim();
                if(detail.contains(":")){
                    String trimmed = detail.replace("drop:", "").replace("%", "").trim();
                    if(detail.contains("drop:") && TryParse.parseDouble(trimmed))
                        dropChance = Double.parseDouble(trimmed);
                    else{
                        String[] args = detail.split(":");
                        if(args.length == 2){
                            if(TryParse.parseInt(args[1])){
                                enchants.put(args[0], Integer.parseInt(args[1]));
                            }
                        }
                    }
                }
                else if(TryParse.parseDouble(detail)) {
                    chance = Double.parseDouble(detail);
                }
                else if(TryParse.parseEnum(Material.class, detail)){
                    itemStack = new ItemStack(Material.valueOf(detail), 1);
                }
                else{
                    ItemStack customItem = LootManager.getItemById(detail);
                    if(customItem != null){
                        itemStack = customItem;
                    }
                }
            }

            if(itemStack != null){
                for(Map.Entry<String, Integer> entry : enchants.entrySet()){
                    Object enchant = EnchantmentHelper.getEnchantmentByName(entry.getKey());
                    if(enchant != null){
                        if(EnchantmentHelper.GoldenEnchantsFound && enchant instanceof GoldenEnchant){
                            ((EnchantManager) EnchantmentHelper.GoldenEnchantsManager).addEnchant(itemStack, (GoldenEnchant) enchant, entry.getValue());
                        }
                        else if(EnchantmentHelper.EnchantmentApiFound && enchant instanceof CustomEnchantment){
                            if(((CustomEnchantment) enchant).canEnchantOnto(itemStack)) {
                                ((CustomEnchantment) enchant).addToItem(itemStack, entry.getValue());
                            }
                        }
                        else if(enchant instanceof Enchantment){
                            {
                                if (((Enchantment) enchant).canEnchantItem(itemStack)) {
                                    itemStack.addEnchantment((Enchantment) enchant, entry.getValue());
                                }
                            }
                        }
                    }
                    else
                        RpgMobsOutputHandler.PrintError("Can't apply enchant, " + entry.getKey() + " to material, " + itemStack.getType().name());
                }

                ItemStacks.add(itemStack);
                Chances.add(chance);
                DropChances.add(dropChance);
            }
            else{
                RpgMobsOutputHandler.PrintError("Failed to load item data, " + item);
            }
        }
    }

    public EquipmentResult getItem(){
        double rand = random.nextDouble() * 100.0;
        double current = 0;
        for(int i=0; i<Chances.size(); i++){
            Double Chance = Chances.get(i);
            current += Chance;
            if(rand < current) {
                return new EquipmentResult(ItemStacks.get(i).clone(), DropChances.get(i));
            }
        }

        return empty;
    }

}
