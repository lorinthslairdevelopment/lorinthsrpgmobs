package me.lorinth.rpgmobs.Objects.TaskOptions;

import org.bukkit.configuration.file.FileConfiguration;

public class PlayerMobLevelNotifyOptions {

    public boolean Enabled = false;
    public long Interval = 20;
    public int LevelDifferenceTrigger = 2;

    public PlayerMobLevelNotifyOptions(FileConfiguration config, String path){
        Enabled = config.getBoolean(path + ".Enabled");
        Interval = config.getLong(path + ".Interval");
        LevelDifferenceTrigger = config.getInt(path + ".LevelDifferenceTrigger");
    }

}
