package me.lorinth.rpgmobs.Objects.MythicMobs;

import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.adapters.bukkit.BukkitItemStack;
import io.lumine.xikage.mythicmobs.drops.*;
import io.lumine.xikage.mythicmobs.drops.droppables.ItemDrop;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

public class DropHelper {

    public static Collection<ItemStack> getLootFromMythicMobsTable(String tableName){
        ArrayList<ItemStack> itemsDropped = new ArrayList<>();
        if(Bukkit.getPluginManager().getPlugin("MythicMobs") == null)
            return itemsDropped;

        DropManager dropManager = MythicMobs.inst().getDropManager();
        Optional<DropTable> dropTable = dropManager.getDropTable(tableName);
        if(dropTable.isPresent()){
            DropMetadata dropMetadata = new DropMetadata(null, null);
            LootBag lootBag = dropTable.get().generate(dropMetadata);
            Collection<Drop> drops = lootBag.getDrops();

            for(Drop drop : drops){
                if(drop instanceof ItemDrop){
                    ItemDrop itemDrop = (ItemDrop) drop;
                    BukkitItemStack bukkitItemStack = (BukkitItemStack) itemDrop.getDrop(dropMetadata);
                    if(bukkitItemStack != null)
                        itemsDropped.add(bukkitItemStack.build());
                }
            }
        }

        return itemsDropped;
    }

}
