package me.lorinth.rpgmobs.Tasks;

public class ExecutedRunnable implements Runnable {

    private boolean didExecute = false;
    private boolean cancelled = false;
    private Runnable runnable;

    public ExecutedRunnable(Runnable runnable){
        this.runnable = runnable;
    }

    public boolean hasExecuted(){
        return didExecute;
    }

    public void cancel(){
        cancelled = true;
    }

    @Override
    public void run() {
        if(cancelled){
            return;
        }

        this.runnable.run();
        didExecute = true;
    }
}
