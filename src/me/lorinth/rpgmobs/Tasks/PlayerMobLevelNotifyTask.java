package me.lorinth.rpgmobs.Tasks;

import me.lorinth.rpgmobs.Data.DataLoader;
import me.lorinth.rpgmobs.Data.MessageManager;
import me.lorinth.rpgmobs.Data.TaskManager;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgmobs.Objects.LevelRegion;
import me.lorinth.rpgmobs.Objects.SpawnPoint;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.regex.Pattern;

public class PlayerMobLevelNotifyTask extends BukkitRunnable {

    Player player;
    LevelRegion lastRegion;
    int lastLevel = 1;
    int levelTrigger;

    public PlayerMobLevelNotifyTask(Player player){
        this.player = player;
        this.levelTrigger = TaskManager.playerMobLevelNotifyOptions.LevelDifferenceTrigger;
        loop();
        runTaskTimer(LorinthsRpgMobs.instance, TaskManager.playerMobLevelNotifyOptions.Interval, TaskManager.playerMobLevelNotifyOptions.Interval);
    }

    @Override
    public void run() {
        if(!player.isOnline()) {
            cancel();
            return;
        }

        loop();
    }

    private void loop(){
        Location location = player.getLocation();
        LevelRegion region = DataLoader.getRegion(location);
        if(region != null && region == lastRegion)
            return;
        else
            lastRegion = region;

        if(lastRegion != null){
            checkNotify(lastRegion.getMinLevel(), lastRegion.getLevelRange());
        }
        else{
            SpawnPoint spawnPoint = DataLoader.getSpawnPoint(location);
            if(spawnPoint == null)
                return;
            
            int level = spawnPoint.calculateRawLevel(location, DataLoader.getDistanceAlgorithm());
            int maxLevel = level + spawnPoint.getVariance();
            checkNotify(level, getNotifyLevelValue(level, maxLevel));
        }
    }

    private void checkNotify(Integer minLevel, String levelValue){
        if(Math.abs(minLevel - lastLevel) < levelTrigger){
            return;
        }

        if(minLevel > lastLevel)
            player.sendMessage(MessageManager.PlayerMobLevelNotifyIncrease.replaceAll(Pattern.quote("{level}"), levelValue));
        else
            player.sendMessage(MessageManager.PlayerMobLevelNotifyDecrease.replaceAll(Pattern.quote("{level}"), levelValue));

        lastLevel = minLevel;
    }

    private String getNotifyLevelValue(Integer level, Integer maxLevel){
        if(level == maxLevel)
            return level.toString();
        else
            return level.toString() + "-" + maxLevel.toString();
    }
}
